﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SBDomain.Model.DoctorModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBProject.Controllers
{
    public class DoctorsController : ApiController
    {
        [AcceptVerbs("GET")]
        public DataSourceResult GetDoctors([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (Model1 doctorDB = new Model1())
            {
                var items = doctorDB.VW_Dr.ToList();
                //var items = doctorDB.Doctors.Select(e => new { id = e.Id, name = e.Name, fName = e.FName, sectionName = e.Section.Name, expert = e.Expert, address = e.Address, bDate = e.BirthDate, phoneNum = e.PhoneNumber, nId = e.NationalId, sectionId = e.Section.Id }).ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("GET")]
        public DataSourceResult GetSections([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (Model1 doctorDB = new Model1())
            {
                var items = doctorDB.Sections.Select(e => new { secName = e.Name, secId = e.Id }).ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("POST")]
        public HttpResponseMessage PostDoctors(VW_Dr newDr)
        {
            using (Model1 doctorDB = new Model1())
            {
                //Console.WriteLine(newDr);
                doctorDB.Doctors.Add(new Doctor
                {
                    Name = newDr.name,
                    FName = newDr.fName,
                    PhoneNumber = newDr.phoneNum,
                    Address = newDr.address,
                    NationalId = newDr.nId,
                    Expert = newDr.expert,
                    SectionId = newDr.sectionId,
                    BirthDate = newDr.bDate 
                });
                doctorDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [AcceptVerbs("PUT")]
        public HttpResponseMessage PutDoctor(VW_Dr editDr)
        {
            using (Model1 DoctorDB = new Model1())
            {
                var oldDr = DoctorDB.Doctors.First(doctor => doctor.Id == editDr.Id);
                oldDr.Name = editDr.name;
                oldDr.FName = editDr.fName;
                oldDr.PhoneNumber = editDr.phoneNum;
                oldDr.Address = editDr.address;
                oldDr.NationalId = editDr.nId;
                oldDr.Expert = editDr.expert;
                oldDr.SectionId = editDr.sectionId;
                oldDr.BirthDate = editDr.bDate;
                DoctorDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [AcceptVerbs("DELETE")]
        public HttpResponseMessage DeleteDoctor(byte drId)
        {
            using (Model1 DoctorDB = new Model1())
            {
                var oldShirt = DoctorDB.Doctors.First(doctor => doctor.Id == drId);
                DoctorDB.Doctors.Remove(oldShirt);
                DoctorDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }
    }


}
