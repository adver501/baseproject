﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SBDomain.Model.BookModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBProject.Controllers
{
    public class BooksController : ApiController
    {
        [AcceptVerbs("GET")]
        public DataSourceResult GetBooks([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (BookDB bookDB = new BookDB())
            {
                var items = bookDB.VW_Book.ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("GET")]
        public DataSourceResult GetBooksList([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (BookDB bookDB = new BookDB())
            {
                var items = bookDB.Books.Select(e => new { e.Id, e.Name }).ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("GET")]
        public DataSourceResult GetAuthors([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (BookDB bookDB = new BookDB())
            {
                var items = bookDB.Authors.Select(e => new { Id = e.Id, FullName = e.FirstName + " " + e.LastName }).ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("GET")]
        public DataSourceResult GetGenres([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (BookDB bookDB = new BookDB())
            {
                var items = bookDB.Genres.Select(e => new { e.Id, e.Name }).ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("GET")]
        public DataSourceResult GetSubGenres([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (BookDB bookDB = new BookDB())
            {
                var items = bookDB.SubGenres.Select(e => new { e.Id, e.Name, e.GenreId }).ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("POST")]
        public HttpResponseMessage PostBook(VW_Book newBook)
        {
            using (BookDB bookDB = new BookDB())
            {
                bookDB.Book_Author.Add(new Book_Author
                {
                    BookId = newBook.BookId,
                    AuthorId = newBook.AuthorId,
                    SubGenreId = newBook.SubGenreId
                });
                bookDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [AcceptVerbs("PUT")]
        public HttpResponseMessage PutBook(VW_Book editedBook)
        {
            using (BookDB bookDB = new BookDB())
            {
                var oldBook = bookDB.Book_Author.First(book => book.Id == editedBook.Id);
                oldBook.BookId = editedBook.BookId;
                oldBook.AuthorId = editedBook.AuthorId;
                oldBook.SubGenreId = editedBook.SubGenreId;
                bookDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [AcceptVerbs("DELETE")]
        public HttpResponseMessage DeleteBook(byte bookId)
        {
            using (BookDB bookDB = new BookDB())
            {
                var oldBook = bookDB.Book_Author.First(book => book.Id == bookId);
                bookDB.Book_Author.Remove(oldBook);
                bookDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

    }
}
