﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBDomain.Model.StudentModel;
using Kendo.Mvc.Extensions;

namespace SBProject.Controllers
{
    public class StudentsController : ApiController
    {
        [AcceptVerbs("GET")]
        public DataSourceResult GetStudents([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (StudentDB studentDB = new StudentDB())
                return studentDB.VW_Student.ToList().ToDataSourceResult(request);
        }

        [AcceptVerbs("GET")]
        public DataSourceResult GetMajors([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (StudentDB studentDB = new StudentDB())
                return studentDB.Majors.Select(e => new { e.Id, e.Name }).ToList().ToDataSourceResult(request);
        }

        [AcceptVerbs("GET")]
        public DataSourceResult GetUniversities([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (StudentDB studentDB = new StudentDB())
                return studentDB.Universities.Select(e => new { e.Id, e.Name }).ToList().ToDataSourceResult(request);
        }

        [AcceptVerbs("PUT")]
        public HttpResponseMessage PutStudent(VW_Student student)
        {
            using (StudentDB studentDB = new StudentDB())
            {
                var oldStudent = studentDB.Students.First(selectedStudent => selectedStudent.Id == student.Id);

                oldStudent.FirstName = student.FirstName;
                oldStudent.LastName = student.LastName;
                oldStudent.StudentId = student.StudentId;
                oldStudent.Gender = student.Gender;
                oldStudent.MajorId = student.MajorId;
                oldStudent.UniversityId = student.UniversityId;
                oldStudent.Grade = student.Grade;

                studentDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [AcceptVerbs("POST")]
        public HttpResponseMessage PostStudent(VW_Student student)
        {
            using (StudentDB shirtDB = new StudentDB())
            {
                shirtDB.Students.Add(new Student
                {
                    FirstName = student.FirstName,
                    LastName = student.LastName,
                    StudentId = student.StudentId,
                    Gender = student.Gender,
                    MajorId = student.MajorId,
                    UniversityId = student.UniversityId,
                    Grade = student.Grade
                });
                shirtDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [AcceptVerbs("DELETE")]
        public HttpResponseMessage DeleteStudent(int Id)
        {
            using (StudentDB studentDB = new StudentDB())
            {
                var student = studentDB.Students.First(selectedStudent => selectedStudent.Id == Id);
                studentDB.Students.Remove(student);
                studentDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }
    }
}