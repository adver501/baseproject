﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBDomain.Model.Sample;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace SBProject.Controllers
{
    public class SampleController : ApiController
    {
        public DataSourceResult GetSamples([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (SampleModel sampleModel = new SampleModel())
            {
                var items = sampleModel.Subs.Select(e => new { id = e.Id, price = e.Price, name = "Ali" }).ToList();
                return items.ToDataSourceResult(request);
            }
        }
    }
}
