﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBDomain.Model.SofaModel;
using Kendo.Mvc.Extensions;
namespace SBProject.Controllers
{
    public class SofasController : ApiController
    {
        public DataSourceResult GetSofas([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (SofaDB sofaDB = new SofaDB())
            {
                //var items = sofaDB.Sofas.Select(e => new { Id = e.Id, Price = e.Price, ColorName = e.Color.Name , CategoryName = e.Category.Name , MaterialName = e.Material.Name}).ToList();
                 var items = sofaDB.VW_Sofa.ToList();
                //Console.WriteLine(items);

                return items.ToDataSourceResult(request);
            }
        }

        public DataSourceResult GetColors([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (SofaDB sofaDB = new SofaDB())
            {
                var items = sofaDB.Colors.Select(e => new {e.Id,  e.Name}).ToList();
                //var items = sofaDB.VW_Sofa.ToList();
                //Console.WriteLine(items);

                return items.ToDataSourceResult(request);
            }
        }

        public DataSourceResult GetCategories([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (SofaDB sofaDB = new SofaDB())
            {
                var items = sofaDB.Categories.Select(e => new { e.Id, e.Name }).ToList();
                //var items = sofaDB.VW_Sofa.ToList();
                //Console.WriteLine(items);

                return items.ToDataSourceResult(request);
            }
        }

        public DataSourceResult GetMaterials([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (SofaDB sofaDB = new SofaDB())
            {
                var items = sofaDB.Materials.Select(e => new { e.Id, e.Name }).ToList();
               // var items = sofaDB.VW_Sofa.ToList();
                //Console.WriteLine(items);

                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("POST")]
        public HttpResponseMessage PostSofas(VW_Sofa newSofa)
        {
            using (SofaDB sofaDB = new SofaDB())
            {
                //var tempCategory = from c in sofaDB.Categories
                //                   where c.Name == newSofa.CategoryName
                //                   select c.Id;

                //var tempColor = from c in sofaDB.Colors
                //                   where c.Name == newSofa.ColorName
                //                   select c.Id;
                //var tempMaterial = from m in sofaDB.Materials
                //                   where m.Name == newSofa.MaterialName
                //                   select m.Id;

                sofaDB.Sofas.Add(new Sofa
                {
                    Price = newSofa.Price,
                    CategoryId = newSofa.CategoryId,
                    ColorId = newSofa.ColorId,
                    MaterialId = newSofa.MaterialId
                });
                sofaDB.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [AcceptVerbs("PUT")]
        public HttpResponseMessage PutSofas(VW_Sofa editedSofa)
        {
            using (SofaDB sofaDB = new SofaDB())
            {
                var oldSofa = sofaDB.Sofas.First(sofa => sofa.Id == editedSofa.Id);
                oldSofa.Price = editedSofa.Price;
                oldSofa.CategoryId = editedSofa.CategoryId;
                oldSofa.ColorId = editedSofa.ColorId;
                oldSofa.MaterialId = editedSofa.MaterialId;
                sofaDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [AcceptVerbs("DELETE")]
        public HttpResponseMessage DeleteSofas(byte sofaId)
        {
            using (SofaDB sofaDB = new SofaDB())
            {
                var oldSofa = sofaDB.Sofas.First(sofa => sofa.Id == sofaId);
                sofaDB.Sofas.Remove(oldSofa);
                sofaDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }
    }
}
