﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBDomain.Model.ShirtModel;
using Kendo.Mvc.Extensions;

namespace SBProject.Controllers
{
    public class ShirtsController : ApiController
    {
        [AcceptVerbs("GET")]
        public DataSourceResult GetShirts([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (ShirtDB shirtDB = new ShirtDB())
            {
                var items = shirtDB.VW_Shirt.ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("GET")]
        public DataSourceResult GetColors([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (ShirtDB shirtDB = new ShirtDB())
            {
                var items = shirtDB.Colors.Select(e => new { e.Id, e.Name }).ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("GET")]
        public DataSourceResult GetCategories([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (ShirtDB shirtDB = new ShirtDB())
            {
                var items = shirtDB.Categories.Select(e => new { e.Id, e.Name }).ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("POST")]
        public HttpResponseMessage PostShirt(VW_Shirt newShirt)
        {
            using (ShirtDB shirtDB = new ShirtDB())
            {
                shirtDB.Shirts.Add(new Shirt
                {
                    Price = newShirt.Price,
                    CategoryId = newShirt.CategoryId,
                    ColorId = newShirt.ColorId
                });
                shirtDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [AcceptVerbs("PUT")]
        public HttpResponseMessage PutShirt(VW_Shirt editedShirt)
        {
            using (ShirtDB shirtDB = new ShirtDB())
            {
                var oldShirt = shirtDB.Shirts.First(shirt => shirt.Id == editedShirt.Id);
                oldShirt.Price = editedShirt.Price;
                oldShirt.CategoryId = editedShirt.CategoryId;
                oldShirt.ColorId = editedShirt.ColorId;
                shirtDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [AcceptVerbs("DELETE")]
        public HttpResponseMessage DeleteShirt(byte shirtId)
        {
            using (ShirtDB shirtDB = new ShirtDB())
            {
                var oldShirt = shirtDB.Shirts.First(shirt => shirt.Id == shirtId);
                shirtDB.Shirts.Remove(oldShirt);
                shirtDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

    }
}
