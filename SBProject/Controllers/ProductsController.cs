﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBDomain.Model.ProductModel;
using Kendo.Mvc.Extensions;

namespace SBProject.Controllers
{
    public class ProductsController : ApiController
    {
        [AcceptVerbs("GET")]
        public DataSourceResult GetProducts([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (ProductDB productDB = new ProductDB())
            {
                var items = productDB.VW_Product.ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("GET")]
        public DataSourceResult GetColors([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (ProductDB productDB = new ProductDB())
            {
                var items = productDB.Colors.Select(e => new { e.Id, e.Name }).ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("GET")]
        public DataSourceResult GetCategories([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (ProductDB productDB = new ProductDB())
            {
                var items = productDB.Categories.Select(e => new { CategoryId = e.Id, e.Name }).ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("GET")]
        public DataSourceResult GetSubCategories([System.Web.Http.ModelBinding.ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            using (ProductDB productDB = new ProductDB())
            {
                List<SubCategoryClass> items = productDB.SubCategories.Select(e => new SubCategoryClass { CategoryId = e.CategoryId, Id = e.Id, Name = e.Name }).ToList();
                return items.ToDataSourceResult(request);
            }
        }

        [AcceptVerbs("POST")]
        public HttpResponseMessage PostProduct(VW_Product newProduct)
        {
            using (ProductDB productDB = new ProductDB())
            {
                productDB.Products.Add(new Product
                {
                    Name = newProduct.Name,
                    Price = newProduct.Price,
                    Count = newProduct.Count,
                    ColorId = newProduct.ColorId,
                    SubCategoryId = newProduct.SubCategoryId,
                    Date = newProduct.Date,
                });
                productDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [AcceptVerbs("PUT")]
        public HttpResponseMessage PutProduct(VW_Product editedProduct)
        {
            using (ProductDB productDB = new ProductDB())
            {
                var oldProduct = productDB.Products.First(product => product.Id == editedProduct.Id);
                oldProduct.Name = editedProduct.Name;
                oldProduct.Price = editedProduct.Price;
                oldProduct.SubCategoryId = editedProduct.SubCategoryId;
                oldProduct.ColorId = editedProduct.ColorId;
                oldProduct.Count = editedProduct.Count;
                oldProduct.Date = editedProduct.Date;
                productDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [AcceptVerbs("DELETE")]
        public HttpResponseMessage DeleteProduct(byte productId)
        {
            using (ProductDB productDB = new ProductDB())
            {
                var oldProduct = productDB.Products.First(p => p.Id == productId);
                productDB.Products.Remove(oldProduct);
                productDB.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }
        public class SubCategoryClass
        {
            public int CategoryId { get; set; }
            public string Name { get; set; }
            public int Id { get; set; }
        }
    }
}
