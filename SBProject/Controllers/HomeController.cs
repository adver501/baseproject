﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBProject.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Shirts()
        {
            return View();
        }

        public ActionResult Products()
        {
            return View();
        }

        public ActionResult Students()
        {
            return View();
        }

        public ActionResult Books()
        {
            return View();
        }
        public ActionResult Sofas()
        {
            return View();
        }
        public ActionResult Doctors()
        {
            return View();
        }
    }
}