﻿(function ($) {
    $.fn.bookManagement = function (options) {
        var settings = $.extend({
            GetBooksAPI: '/api/Books/GetBooks',
            PostBookAPI: '/api/Books/PostBook',
            PutBookAPI: '/api/Books/PutBook',
            DeleteBookAPI: '/api/Books/DeleteBook',
            GetListBooksAPI: '/api/Books/GetBooksList',
            GetAuthorsAPI: '/api/Books/GetAuthors',
            GetGenresAPI: '/api/Books/GetGenres',
            GetSubGenresAPI: '/api/Books/GetSubGenres',
            gridHeight: 400,
        }, options)

        var area = this;
        var grid = null;
        var viewModel = null;
        var myWindow = null;

        buildInterface();

        function buildInterface() {
            kendo.culture("fa-IR");
            viewModel = kendo.observable({
                additem: function () {
                    var m = viewModel.get("dataSource");
                    var t = m.add(new m.reader.model());
                    viewModel.set("selected", t);
                    myWindow.center();
                    myWindow.open();
                },
                edititem: function () {
                    if (viewModel.selected) {
                        myWindow.center();
                        myWindow.open();
                    }
                    else {
                        let notificationWidget = $("#notification").kendoNotification({ autoHideAfter: 5000 }).data("kendoNotification");
                        notificationWidget.show("لظفا یکی از کتاب ها را انتخاب کنید", "error");
                    }
                },
                deleteitem: function () {
                    if (viewModel.get("selected")) {
                        kendo.confirm("آیا میخواهید این آیتم را حذف کنید؟")
                            .done(function () {
                                $.ajax({
                                    url: settings.DeleteBookAPI + "?bookId=" + viewModel.get("selected").Id,
                                    type: "DELETE",
                                    success: function () {
                                        let notificationWidget = $("#notification").kendoNotification({ autoHideAfter: 5000 }).data("kendoNotification");
                                        notificationWidget.show("آیتم مورد نظر حذف شد", "success");
                                        grid.dataSource.read();
                                        viewModel.set("selected", null);
                                    }
                                });
                            })
                            .fail(function () {
                                viewModel.set("selected", null);
                            });
                    } else {
                        let notificationWidget = $("#notification").kendoNotification({ autoHideAfter: 5000 }).data("kendoNotification");
                        notificationWidget.show("لظفا یکی از کتاب ها را انتخاب کنید", "error");
                    }
                },
                selected: null,
                dataSource: getBookDatasource(),
                bookDataSource: getBookListDataSource(),
                authorDataSource: getAuthorDataSource(),
                genresDataSource: getGenresDataSource(),
                subGenresDataSource: getSubGenresDataSource(),
                sync: function () {
                    grid.dataSource.sync();
                },
                cancel: function (e) {
                    this.dataSource.cancelChanges();
                    viewModel.set("selected", null);
                    myWindow.close();
                },
            });

            grid = $('[data-userman-role="grid"]').kendoGrid({
                columns: [
                    { field: "Name", width: 200, title: 'نام', sortable: true, filterable: true },
                    { field: "FirstName", width: 200, title: 'نویسنده', sortable: true, filterable: true, template: "#=data.FirstName# #=data.LastName#" },
                    { field: "ISBN", width: 200, title: 'شابک', sortable: true, filterable: true },
                    { field: "GenreName", width: 200, title: 'ژانر', sortable: true, filterable: true },
                    { field: "SubGenreName", width: 200, title: 'دسته', sortable: true, filterable: true },
                ],
                height: settings.gridHeight,
                selectable: "Single, Row",
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5,
                },
                dataSource: viewModel.dataSource,
                sortable: true, filterable: true,
                columnMenu: true,
                change: function (arg) {
                    var grid = arg.sender;
                    var items = grid.select();
                    var selectedItem = grid.dataItem(items);
                    viewModel.set("selected", selectedItem);
                },
            }).data('kendoGrid');

            kendo.bind(area, viewModel);

            myWindow = $("#aeWindow").kendoWindow({
                width: "300px",
                title: "ایجاد کتاب",
                visible: false,
                close: function () {
                    viewModel.get("dataSource").cancelChanges();
                    viewModel.set("selected", null);
                }
            }).data("kendoWindow");
            $("#genre").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "Id",
                dataSource: getGenresDataSource()
            });

            $("#subGenre").kendoDropDownList({
                valuePrimitive: true,
                cascadeFrom: "genre",
                cascadeFromField: "GenreId",
                cascadeFromParentField: "Id",
                dataTextField: "Name",
                dataValueField: "Id",
                dataSource: getSubGenresDataSource()
            });
        }

        function getBookDatasource() {
            return new kendo.data.DataSource({
                transport: {
                    prefix: "",
                    read: {
                        url: settings.GetBooksAPI,
                        type: "GET"
                    },
                    create: {
                        url: settings.PostBookAPI,
                        type: "POST",
                        complete: function (jqXHR, textStatus) {
                            if (jqXHR.status == 200) {
                                let notificationWidget = $("#notification").kendoNotification({ autoHideAfter: 5000 }).data("kendoNotification");
                                notificationWidget.show("آیتم با موفقیت ایجاد شد", "success");
                                grid.dataSource.read();
                                myWindow.close();
                            } else {
                                console.log(jqXHR);
                                let notificationWidget = $("#notification").kendoNotification({ autoHideAfter: 5000 }).data("kendoNotification");
                                notificationWidget.show("مشکلی در ایجاد پیش آمد", "error");
                            }
                        },
                        error: function (x, y, z) {
                            console.log(x + "\n" + y + "\n" + z);
                        }
                    },
                    update: {
                        url: settings.PutBookAPI,
                        type: "PUT",
                        complete: function (jqXHR, textStatus) {
                            let notificationWidget = $("#notification").kendoNotification({ autoHideAfter: 5000 }).data("kendoNotification");
                            notificationWidget.show("تغییرات با موفقیت ذخیره شد", "success");
                            grid.dataSource.read();
                            myWindow.close();
                        },
                        error: function (x, y, z) {
                            console.log(x + '\n' + y + '\n' + z);
                            let notificationWidget = $("#notification").kendoNotification({ autoHideAfter: 5000 }).data("kendoNotification");
                            notificationWidget.show("مشکلی در ویرایش پیش آمد", "error");
                        }
                    },
                    idField: "ID"
                },
                pageable: true,
                pageSize: 10,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                type: (function () { if (kendo.data.transports['webapi']) { return 'webapi'; } else { throw new Error('The kendo.aspnetmvc.min.js script is not included.'); } })(),
                schema: {
                    data: "Data",
                    total: "Total",
                    model: {
                        id: "Id",
                        fields: {
                            "Name": { type: "string" },
                            "FirstName": { type: "string" },
                            "LastName": { type: "string" },
                            "ISBN": { type: "string" },
                            "GenreName": { type: "string" },
                            "SubGenreName": { type: "string" },
                        }
                    }
                }
            });
        }

        function getBookListDataSource() {
            return new kendo.data.DataSource({
                type: 'webapi',
                transport: {
                    prefix: "",
                    type: "GET",
                    read: {
                        url: settings.GetListBooksAPI,
                        dataType: "json"
                        , complete: function (jj, qq) {
                        }
                    },
                    idField: "Id"
                },
                pageSize: 20,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    model: {
                        id: "Id",
                        fields: {
                            "Id": { "type": "number" },
                            "Name": { "type": "string" },
                        }
                    }
                }
            });
        }

        function getAuthorDataSource() {
            return new kendo.data.DataSource({
                type: 'webapi',
                transport: {
                    prefix: "",
                    type: "GET",
                    read: {
                        url: settings.GetAuthorsAPI,
                        dataType: "json"
                        , complete: function (jj, qq) {
                        }
                    },
                    idField: "Id"
                },
                pageSize: 20,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    model: {
                        id: "Id",
                        fields: {
                            "Id": { "type": "number" },
                            "FullName": { "type": "string" },
                        }
                    }
                }
            });
        }

        function getGenresDataSource() {
            return new kendo.data.DataSource({
                type: 'webapi',
                transport: {
                    prefix: "",
                    type: "GET",
                    read: {
                        url: settings.GetGenresAPI,
                        dataType: "json"
                        , complete: function (jj, qq) {
                        }
                    },
                    idField: "Id"
                },
                pageSize: 20,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    model: {
                        id: "Id",
                        fields: {
                            "Id": { "type": "number" },
                            "Name": { "type": "string" },
                        }
                    }
                }
            });
        }

        function getSubGenresDataSource() {
            return new kendo.data.DataSource({
                type: 'webapi',
                transport: {
                    prefix: "",
                    type: "GET",
                    read: {
                        url: settings.GetSubGenresAPI,
                        dataType: "json"
                        , complete: function (jj, qq) {
                        }
                    },
                    idField: "Id"
                },
                pageSize: 20,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    model: {
                        id: "Id",
                        fields: {
                            "Id": { "type": "number" },
                            "Name": { "type": "string" },
                            "GenreId": { "type": "number" },
                        }
                    }
                }
            });
        }
    }
}(jQuery));
