﻿(function ($) {
    $.fn.userManagement = function (options) {
        var settings = $.extend({
            GetStudentsAPI: '/api/Students/GetStudents',
            PostStudentAPI: '/api/Students/PostStudent',
            PutStudentAPI: '/api/Students/PutStudent',
            DeleteStudentAPI: '/api/Students/DeleteStudent',
            GetMajorsAPI: '/api/Students/GetMajors',
            GetUniversitiesAPI: '/api/Students/GetUniversities',
            gridHeight: 400,
        }, options);

        var area = this;
        var grid = null;
        var viewModel = null;
        var myWindow = null;

        buildInterface();

        function buildInterface() {
            kendo.culture("fa-IR");
            viewModel = kendo.observable({
                additem: function () {
                    var m = viewModel.get("dataSource");
                    var t = m.add(new m.reader.model());
                    viewModel.set("selected", t);
                    myWindow.center();
                    myWindow.open();
                },
                edititem: function () {

                    if (viewModel.selected) {
                        myWindow.center();
                        myWindow.open();
                    } else {
                        alert('^_^');
                    }
                },
                deleteitem: function () {
                    if (viewModel.get("selected")) {
                        $.ajax({
                            url: settings.DeleteStudentAPI + "?Id=" + viewModel.get("selected").Id,
                            type: "DELETE",
                            success: function (result) {
                                alert("آیتم مورد نظر حذف شد")
                                grid.dataSource.read();
                                viewModel.set("selected", null);
                            }
                        });
                    } else
                        alert("یک دانشجو انتخاب کنید");
                },
                sync: function () {
                    grid.dataSource.sync();
                },
                cancel: function (e) {
                    this.get("dataSource").cancelChanges();
                    this.set("selected", null);
                    myWindow.close();
                },

                dataSource: getStudentDatasource(),
                selected: null,
                MajorDataSource: getMajorDataSource(),
                UniversityDataSource: getUniversityDataSource(),
            });

            grid = $('#grid').kendoGrid({
                columns: [
                    { field: "FirstName", title: 'نام', sortable: true, filterable: true },
                    { field: "LastName", title: 'نام خانوادگی', sortable: true, filterable: true },
                    { field: "Gender", title: 'جنسیت', sortable: true, filterable: true },
                    { field: "StudentId", title: 'شماره دانشجویی', sortable: true, filterable: true },
                    { field: "Major", title: 'رشته', sortable: true, filterable: true },
                    { field: "University", title: 'دانشگاه', sortable: true, filterable: true },
                    { field: "Grade", title: 'معدل', sortable: true, filterable: true },
                    { field: "ModifiedDate", title: 'تاریخ آخرین تغییر', sortable: true, filterable: true}
                ],
                height: settings.gridHeight,
                selectable: "Single, Row",
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5,
                },
                dataSource: viewModel.dataSource,
                sortable: true, filterable: true,
                columnMenu: true,
                change: function (arg) {
                    var grid = arg.sender;
                    var items = grid.select();
                    var selectedItem = grid.dataItem(items);
                    viewModel.set("selected", selectedItem);
                    console.log(selectedItem);
                },
            }).data('kendoGrid');

            kendo.bind(area, viewModel);

            myWindow = $('#aeWindow').kendoWindow({
                width: "400px",
                title: "ایجاد دانشجو",
                visible: false,
                close: function () {
                    viewModel.get("dataSource").cancelChanges();
                    viewModel.set("selected", null);
                }
            }).data("kendoWindow").center().open();
        }

        function getStudentDatasource() {
            return new kendo.data.DataSource({
                transport: {
                    prefix: "",
                    read: {
                        url: settings.GetStudentsAPI,
                        type: "GET"
                    },
                    create: {
                        url: settings.PostStudentAPI,
                        type: "POST",
                        complete: function (jqXHR, textStatus) {
                            if (jqXHR.status == 200) {
                                grid.dataSource.read();
                                alert('آیتم با موفقیت ایجاد شد');
                                myWindow.close();
                            } else {
                                console.log(jqXHR);
                            }
                        },
                        error: function (x, y, z) {
                            alert(x + "\n" + y + "\n" + z);
                        }
                    },
                    update: {
                        url: settings.PutStudentAPI,
                        type: "PUT",
                        complete: function (jqXHR, textStatus) {
                            grid.dataSource.read();
                            grid.refresh();
                            winuser.close();
                            alert('تغییرات با موفقیت ذخیره شد');
                            myWindow.close();
                        },
                        error: function (x, y, z) {
                            alert(x + '\n' + y + '\n' + z);
                        }
                    },
                    idField: "ID"
                },
                pageable: true,
                pageSize: 10,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                type: (function () { if (kendo.data.transports['webapi']) { return 'webapi'; } else { throw new Error('The kendo.aspnetmvc.min.js script is not included.'); } })(),
                schema: {
                    data: "Data",
                    total: "Total",
                    model: {
                        id: "Id",
                        fields: {
                            "FirstName": { type: "string" },
                            "LastName": { type: "string" },
                            "Gender": { type: "string" },
                            "StudentId": { type: "number" },
                            "Major": { type: "string" },
                            "MajorId": { type: "number" },
                            "University": { type: "string" },
                            "UniversityId": { type: "number" },
                            "Grade": { type: "number" },
                            "ModifiedDate": { type: "date", format: "{0:yyyy/MM/dd HH:mm:ss}", template: "#= kendo.toString(kendo.parseDate(data.Date),'MM-dd-yyyy HH:mm:ss') #" }                            
                        }
                    }
                }
            });
        }

        function getMajorDataSource() {
            return new kendo.data.DataSource({
                type: 'webapi',
                transport: {
                    prefix: "",
                    type: "GET",
                    read: {
                        url: settings.GetMajorsAPI,
                        dataType: "json"
                        , complete: function (jj, qq) {
                        }
                    },
                    idField: "Id"
                },
                pageSize: 20,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    model: {
                        id: "Id",
                        fields: {
                            "Id": { "type": "number" },
                            "Name": { "type": "string" },
                        }
                    }
                }
            });
        }

        function getUniversityDataSource() {
            return new kendo.data.DataSource({
                type: 'webapi',
                transport: {
                    prefix: "",
                    type: "GET",
                    read: {
                        url: settings.GetUniversitiesAPI,
                        dataType: "json"
                        , complete: function (jj, qq) {
                        }
                    },
                    idField: "Id"
                },
                pageSize: 20,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    model: {
                        id: "Id",
                        fields: {
                            "Id": { "type": "number" },
                            "Name": { "type": "string" },
                        }
                    }
                }
            });
        }
    }
}(jQuery));