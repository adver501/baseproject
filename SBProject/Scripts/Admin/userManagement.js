﻿(function ($) {
    $.fn.userManagement = function (options) {
        var settings = $.extend({
            getShirtsAPI: '/api/Shirts/GetShirts',
            postShirtAPI: '/api/Shirts/PostShirt',
            putShirtAPI: '/api/Shirts/PutShirt',
            deleteShirtAPI: '/api/Shirts/DeleteShirt',
            getColorsAPI: '/api/Shirts/GetColors',
            getCategoriesAPI: '/api/Shirts/GetCategories',
            gridHeight: 400,
        }, options);

        var area = this;
        var grid = null;
        var viewModel = null;
        var myWindow = null;

        buildInterface();

        function buildInterface() {
            kendo.culture("fa-IR");

            viewModel = kendo.observable({
                additem: function () {
                    var m = viewModel.get("dataSource");
                    var t = m.add(new m.reader.model());
                    viewModel.set("selected", t);
                    myWindow.center();
                    myWindow.open();
                },
                edititem: function () {
                    if (viewModel.selected) {
                        myWindow.center();
                        myWindow.open();
                    }
                    else
                        alert("لطفا یکی از پوشاک‌ها را انتخاب کنید");
                },
                deleteitem: function () {
                    if (viewModel.get("selected")) {
                        kendo.confirm("آیا از حذف کردن اطمینان دارید؟")
                            .then(function () {
                                $.ajax({
                                    url: settings.deleteShirtAPI + "?shirtId=" + viewModel.get("selected").Id,
                                    type: "DELETE",
                                    success: function () {
                                        alert("آیتم مورد نظر حذف شد");
                                        grid.dataSource.read();
                                        viewModel.set("selected", null);
                                    }
                                });
                            })
                            .fail(function () {
                                viewModel.set("selected", null);
                            });
                    }
                    else
                        alert("لطفا یکی از موارد را انتخاب کنید");
                },
                selected: null,
                dataSource: getUserDatasource(),
                categoryDataSource: getCategoryDataSource(),
                colorDataSource: getColorDataSource(),
                sync: function () {
                    grid.dataSource.sync();
                },
                cancel: function (e) {
                    this.dataSource.cancelChanges();
                    viewModel.set("selected", null);
                    myWindow.close();
                },
            });

            grid = $('[data-userman-role="grid"]').kendoGrid({
                columns: [
                    { field: "Price", width: 200, title: 'قیمت', sortable: true, filterable: true, template: "#=data.Price# ریال" },
                    { field: "ColorName", width: 200, title: 'رنگ', sortable: true, filterable: true },
                    { field: "CategoryName", width: 200, title: 'نوع', sortable: true, filterable: true },
                ],
                height: settings.gridHeight,
                selectable: "Single, Row",
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5,
                },
                dataSource: viewModel.dataSource,
                sortable: true, filterable: true,
                columnMenu: true,
                change: function (arg) {
                    var grid = arg.sender;
                    var items = grid.select();
                    var selectedItem = grid.dataItem(items);
                    viewModel.set("selected", selectedItem);
                    console.log(viewModel.selected);
                },
            }).data('kendoGrid');

            kendo.bind(area, viewModel);

            myWindow = $("#aeWindow").kendoWindow({
                width: "300px",
                title: "ایجاد پوشاک",
                visible: false,
                close: function () {
                    viewModel.get("dataSource").cancelChanges();
                    viewModel.set("selected", null);
                }
            }).data("kendoWindow");
        }

        function getUserDatasource() {
            return new kendo.data.DataSource({
                transport: {
                    prefix: "",
                    read: {
                        url: settings.getShirtsAPI,
                        type: "GET"
                    },
                    create: {
                        url: settings.postShirtAPI,
                        type: "POST",
                        complete: function (jqXHR, textStatus) {
                            if (jqXHR.status == 200) {
                                alert('آیتم با موفقیت ایجاد شد');
                                grid.dataSource.read();
                                myWindow.close();
                            } else {
                                console.log(jqXHR);
                            }
                        },
                        error: function (x, y, z) {
                            console.log(x + "\n" + y + "\n" + z);
                        }
                    },
                    update: {
                        url: settings.putShirtAPI,
                        type: "PUT",
                        complete: function (jqXHR, textStatus) {
                            alert('تغییرات با موفقیت ذخیره شد');
                            grid.dataSource.read();
                            myWindow.close();
                        },
                        error: function (x, y, z) {
                            alert(x + '\n' + y + '\n' + z);
                        }
                    },
                    idField: "ID"
                },
                pageable: true,
                pageSize: 10,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                type: (function () { if (kendo.data.transports['webapi']) { return 'webapi'; } else { throw new Error('The kendo.aspnetmvc.min.js script is not included.'); } })(),
                schema: {
                    data: "Data",
                    total: "Total",
                    model: {
                        id: "Id",
                        fields: {
                            "Price": { type: "number" },
                            "ColorName": { type: "string" },
                            "CategoryName": { type: "string" },
                            "CategoryId": { type: "number" },
                            "ColorId": { type: "number" },
                        }
                    }
                }
            });
        }

        function getCategoryDataSource() {
            return new kendo.data.DataSource({
                type: 'webapi',
                transport: {
                    prefix: "",
                    type: "GET",
                    read: {
                        url: settings.getCategoriesAPI,
                        dataType: "json"
                        , complete: function (jj, qq) {
                        }
                    },
                    idField: "Id"
                },
                pageSize: 20,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    model: {
                        id: "Id",
                        fields: {
                            "Id": { "type": "number" },
                            "Name": { "type": "string" },
                        }
                    }
                }
            });
        }

        function getColorDataSource() {
            return new kendo.data.DataSource({
                type: 'webapi',
                transport: {
                    prefix: "",
                    type: "GET",
                    read: {
                        url: settings.getColorsAPI,
                        dataType: "json"
                        , complete: function (jj, qq) {
                        }
                    },
                    idField: "Id"
                },
                pageSize: 20,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    model: {
                        id: "Id",
                        fields: {
                            "Id": { "type": "number" },
                            "Name": { "type": "string" },
                        }
                    }
                }
            });
        }
    }
}(jQuery));