﻿(function ($) {
    $.fn.doctorManagement = function (options) {
        var settings = $.extend({
            postDoctorsAPI: '/api/Doctors/PostDoctors',
            putDoctorAPI: '/api/Doctors/PutDoctor',
            getDoctorsAPI: '/api/Doctors/GetDoctors',
            deleteDoctorAPI: '/api/Doctors/DeleteDoctor',
            getSectionsAPI: '/api/Doctors/GetSections',
            gridHeight: 400,
        }, options);

        var area = this;
        var grid = null;
        var viewModel = null;
        var myWindow = null;

        buildInterface();

        function buildInterface() {
            kendo.culture("fa-IR");

            viewModel = kendo.observable({
                additem: function () {
                    var m = viewModel.get("dataSource");
                    var t = m.add(new m.reader.model());
                    viewModel.set("selected", t);
                    myWindow.center();
                    myWindow.open();
                },
                edititem: function () {
                    if (viewModel.selected) {
                        myWindow.center();
                        myWindow.open();
                    }
                    else
                        alert("لطفا یکی از پوشاک‌ها را انتخاب کنید");
                },
                deleteitem: function () {
                    if (viewModel.get("selected")) {
                        kendo.confirm("آیا از حذف کردن اطمینان دارید؟")
                            .then(function () {
                                $.ajax({
                                    url: settings.deleteDoctorAPI + "?drId=" + viewModel.get("selected").Id,
                                    type: "DELETE",
                                    success: function () {
                                        alert("آیتم مورد نظر حذف شد");
                                        grid.dataSource.read();
                                        viewModel.set("selected", null);
                                    }
                                });
                            })
                            .fail(function () {
                                viewModel.set("selected", null);
                            });
                    }
                    else
                        alert("لطفا یکی از موارد را انتخاب کنید");
                },
                selected: null,
                dataSource: getUserDatasource(),
                sectionDataSource: getSectionDataSource(),
                //colorDataSource: getColorDataSource(),
                sync: function () {
                    grid.dataSource.sync();
                },
                cancel: function (e) {
                    this.dataSource.cancelChanges();
                    viewModel.set("selected", null);
                    myWindow.close();
                },
            });

            grid = $('[data-userman-role="grid"]').kendoGrid({
                columns: [
                    { field: "name", width: 200, title: 'نام ', sortable: true, filterable: true },
                    { field: "fName", width: 200, title: 'نام خانوادگی ', sortable: true, filterable: true },
                    { field: "bDate", width: 200, title: 'تاریخ تولد', sortable: true, filterable: true, },
                    { field: "secName", width: 200, title: 'نام بخش', sortable: true, filterable: true },
                    { field: "expert", width: 200, title: 'تخصص', sortable: true, filterable: true },
                    { field: "phoneNum", width: 200, title: 'تلفن همراه', sortable: true, filterable: true },





                    //{ field: "name", width: 200, title: 'نام ', sortable: true, filterable: true },
                    //{ field: "fName", width: 200, title: 'نام خانوادگی ', sortable: true, filterable: true },
                    //{ field: "id", width: 200, title: 'آیدی', sortable: true, filterable: true, },
                    //{ field: "sectionName", width: 200, title: 'نام بخش', sortable: true, filterable: true },


                    // template: "#= data.name #=data.fName"
                ],
                height: settings.gridHeight,
                selectable: "Single, Row",
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5,
                },
                dataSource: viewModel.dataSource,
                sortable: true, filterable: true,
                columnMenu: true,
                change: function (arg) {
                    var grid = arg.sender;
                    var items = grid.select();
                    var selectedItem = grid.dataItem(items);
                    viewModel.set("selected", selectedItem);
                    console.log(viewModel.selected);
                },
            }).data('kendoGrid');

            kendo.bind(area, viewModel);

            myWindow = $("#aeWin").kendoWindow({
                width: "300px",
                title: "اضافه کردن دکتر",
                visible: false,
                close: function () {
                    viewModel.get("dataSource").cancelChanges();
                    viewModel.set("selected", null);
                }
            }).data("kendoWindow");
        }

        function getUserDatasource() {
            return new kendo.data.DataSource({
                transport: {
                    prefix: "",
                    read: {
                        url: settings.getDoctorsAPI,
                        type: "GET"
                    },
                    create: {
                        url: settings.postDoctorsAPI,
                        type: "POST",
                        complete: function (jqXHR, textStatus) {
                            if (jqXHR.status == 200) {
                                alert('آیتم با موفقیت ایجاد شد');
                                grid.dataSource.read();
                                myWindow.close();
                            } else {
                                console.log(jqXHR);
                            }
                        },
                        error: function (x, y, z) {
                            console.log(x + "\n" + y + "\n" + z);
                        }
                    },
                    update: {
                        url: settings.putDoctorAPI,
                        type: "PUT",
                        complete: function (jqXHR, textStatus) {
                            alert('تغییرات با موفقیت ذخیره شد');
                            grid.dataSource.read();
                            myWindow.close();
                        },
                        error: function (x, y, z) {
                            alert(x + '\n' + y + '\n' + z);
                        }
                    },
                    idField: "ID"
                },
                pageable: true,
                pageSize: 10,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                type: (function () { if (kendo.data.transports['webapi']) { return 'webapi'; } else { throw new Error('The kendo.aspnetmvc.min.js script is not included.'); } })(),
                schema: {
                    data: "Data",
                    total: "Total",
                    model: {
                        id: "Id",
                        fields: {

                            //"id": { type: "number" },
                            "name": { type: "string" },
                            "fName": { type: "string" },
                            //"sectionName": { type: "string" },
                            "expert": { type: "string" },
                            "address": { type: "string" },
                            //"bDate": { type: "datetime" },
                            "phoneNum": { type: "number" },
                            "nId": { type: "number" },
                            //"sectionId": { type: "number" },
                            //"secId": { type: "number" },
                            //"secName": { type: "string" },

                        }
                    }
                }
            });
        }

        function getSectionDataSource() {
            return new kendo.data.DataSource({
                type: 'webapi',
                transport: {
                    prefix: "",
                    type: "GET",
                    read: {
                        url: settings.getSectionsAPI,
                        dataType: "json"
                        , complete: function (jj, qq) {
                        }
                    },
                    idField: "Id"
                },
                pageSize: 20,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    model: {
                        id: "Id",
                        fields: {
                            "secId": { type: "number" },
                            "secName": { type: "string" },
                        }
                    }
                }
            });
        }

        function getColorDataSource() {
            return new kendo.data.DataSource({
                type: 'webapi',
                transport: {
                    prefix: "",
                    type: "GET",
                    read: {
                        url: settings.getColorsAPI,
                        dataType: "json"
                        , complete: function (jj, qq) {
                        }
                    },
                    idField: "Id"
                },
                pageSize: 20,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    model: {
                        id: "Id",
                        fields: {
                            "Id": { "type": "number" },
                            "Name": { "type": "string" },
                            "SectionId": { "type": "number" },
                            "id": { type: "number" },
                            "name": { type: "string" },
                            "fName": { type: "string" },
                            "sectionId": { type: "number" },
                        }
                    }
                }
            });
        }
    }
}(jQuery));