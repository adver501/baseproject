﻿(function ($) {
    $.fn.productManagement = function (options) {
        var settings = $.extend({
            GetProductAPI: '/api/Products/GetProducts',
            PostProductAPI: '/api/Products/PostProduct',
            GetColorsAPI: '/api/Products/GetColors',
            GetCategoriesAPI: '/api/Products/GetCategories',
            GetSubCategoriesAPI: '/api/Products/GetSubCategories',
            PutProductAPI: '/api/Products/PutProduct',
            DeleteProductAPI: '/api/Products/DeleteProduct',
            gridHeight: 400,
        }, options);

        var area = this;
        var grid = null;
        var viewModel = null;
        var myWindow = null;

        buildInterface();

        function buildInterface() {
            kendo.culture("fa-IR");
            viewModel = kendo.observable({
                additem: function () {
                    var m = viewModel.get("dataSource");
                    var t = m.add(new m.reader.model());
                    viewModel.set("selected", t);
                    myWindow.center();
                    myWindow.open();
                },
                edititem: function () {
                    if (viewModel.selected) {
                        myWindow.center();
                        myWindow.open();
                    }
                    else {
                        alert("لطفا یکی از کالاهای زیر را برای ویرایش انتخاب کنید");
                    }
                },
                deleteitem: function () {
                    if (viewModel.get("selected")) {
                        kendo.confirm("آیا از حذف کردن خود اطمینان دارید؟")
                            .then(function () {
                                $.ajax({
                                    url: settings.DeleteProductAPI + "?productId=" + viewModel.get("selected").Id,
                                    type: "DELETE",
                                    success: function () {
                                        alert("کالا موردنظر با موفقیت حذف شد");
                                        grid.dataSource.read();
                                        viewModel.set("selected", null);
                                    }
                                });
                            })
                            .fail(function () {
                                alert("amir");
                                viewModel.set("selected", null);
                            });
                    }
                    else {
                        alert("لطفا یکی از کالاها را به منظور حذف انتخاب کنید")
                    }
                },
                selected: null,
                dataSource: getUserDatasource(),
                categoryDataSource: getCategoryDataSource(),
                subCategoryDataSource: getSubCategoryDataSource(),
                colorDataSource: getColorDataSource(),
                sync: function () {
                    grid.dataSource.sync();
                },
                cancel: function (e) {
                    this.dataSource.cancelChanges();
                    viewModel.set("selected", null);
                    myWindow.close();
                },
            });

            grid = $('[data-userman-role="grid"]').kendoGrid({
                columns: [
                    { field: "Name", width: 200, title: 'نام کالا', sortable: true, filterable: true },
                    { field: "Price", width: 200, title: 'قیمت', sortable: true, filterable: true, template: "#=data.Price# ریال" },
                    { field: "ColorName", width: 200, title: 'رنگ', sortable: true, filterable: true },
                    { field: "CategoryName", width: 200, title: 'دسته', sortable: true, filterable: true },
                    { field: "SubCategoryName", width: 200, title: 'زیر دسته', srotable: true, filterable: true },
                    { field: "Count", width: 200, title: 'تعداد موجود در انبار', sortable: true, filterable: true },
                    { field: "Date", width: 200, title: 'تاریخ و زمان ایجاد کالا', sortable: true, filterable: true, template: "#= getDateTimeStr(1,data.Date)#" },
                ],
                height: settings.gridHeight,
                selectable: "Single, Row",
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5,
                },
                dataSource: viewModel.dataSource,
                sortable: true, filterable: true,
                columnMenu: true,
                change: function (arg) {
                    var grid = arg.sender;
                    var items = grid.select();
                    var selectedItem = grid.dataItem(items);
                    viewModel.set("selected", selectedItem);
                    console.log(viewModel.selected);
                },
            }).data('kendoGrid');

            kendo.bind(area, viewModel);

            myWindow = $("#aeWindow").kendoWindow({
                width: "300px",
                title: "ایجاد کالا",
                visible: false,
                close: function () {
                    viewModel.get("dataSource").cancelChanges();
                    viewModel.set("selected", null);
                }
            }).data("kendoWindow");
            $("#categoryInput").kendoComboBox({
                optionLabel: "انتخاب دسته...",
                dataTextField: "Name",
                dataValueField: "CategoryId",
                dataSource: getCategoryDataSource(),
            }).data("kendoComboBox");

            $("#subCatInput").kendoComboBox({
                autoBind: false,
                cascadeFrom: "categoryInput",
                optionLabel: "انتخاب زیردسته...",
                dataTextField: "Name",
                dataValueField: "Id",
                dataSource: getSubCategoryDataSource(),
            }).data("kendoComboBox");
        }

        function getUserDatasource() {
            return new kendo.data.DataSource({
                transport: {
                    prefix: "",
                    read: {
                        url: settings.GetProductAPI,
                        type: "GET"
                    },
                    create: {
                        url: settings.PostProductAPI,
                        type: "POST",
                        complete: function (jqXHR, textStatus) {
                            if (jqXHR.status == 200) {
                                console.log(jqXHR);
                                alert('آیتم با موفقیت ایجاد شد');
                                grid.dataSource.read();
                                myWindow.close();
                            } else {
                                console.log(jqXHR);
                            }
                        },
                        error: function (x, y, z) {
                            console.log(x + "\n" + y + "\n" + z);
                        }
                    },
                    update: {
                        url: settings.PutProductAPI,
                        type: "PUT",
                        complete: function (jqXHR, textStatus) {
                            grid.dataSource.read();
                            myWindow.close();
                            alert('تغییرات با موفقیت ذخیره شد');
                        },
                        error: function (x, y, z) {
                            alert(x + '\n' + y + '\n' + z);
                        }
                    },
                    idField: "ID"
                },
                pageable: true,
                pageSize: 10,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                type: (function () { if (kendo.data.transports['webapi']) { return 'webapi'; } else { throw new Error('The kendo.aspnetmvc.min.js script is not included.'); } })(),
                schema: {
                    data: "Data",
                    total: "Total",
                    model: {
                        id: "Id",
                        fields: {
                            "Name": { type: "string" },
                            "Price": { type: "number" },
                            "ColorName": { type: "string" },
                            "CategoryName": { type: "string" },
                            "SubCategoryName": { type: "string" },
                            "Count": { type: "number" },
                            "Date": { type: "date" },
                            "CategoryId": {type: "number"},
                        }
                    }
                }
            });
        }
        function getCategoryDataSource() {
            return new kendo.data.DataSource({
                type: 'webapi',
                transport: {
                    prefix: "",
                    type: "GET",
                    read: {
                        url: settings.GetCategoriesAPI,
                        dataType: "json"
                        , complete: function (jj, qq) {
                        }
                    },
                    idField: "Id"
                },
                pageable: true,
                pageSize: 100,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    model: {
                        id: "Id",
                        fields: {
                            "CategoryId": { "type": "number" },
                            "Name": { "type": "string" },
                        }
                    }
                }
            });
        }
        function getSubCategoryDataSource() {
            return new kendo.data.DataSource({
                type: 'webapi',
                transport: {
                    prefix: "",
                    type: "odata",
                    read: {
                        url: settings.GetSubCategoriesAPI,
                        dataType: "json"
                        , complete: function (jj, qq) {
                        }
                    },
                    idField: "Id"
                },
                pageable: true,
                pageSize: 100,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    model: {
                        id: "Id",
                        parentId:"CategoryId",
                        fields: {
                            "Id": { "type": "number" },
                            "Name": { "type": "string" },
                            "CategoryId": { "type": "number", nullable: true },
                        }
                    }
                }
            });
        }
        function getColorDataSource() {
            return new kendo.data.DataSource({
                type: 'webapi',
                transport: {
                    prefix: "",
                    type: "GET",
                    read: {
                        url: settings.GetColorsAPI,
                        dataType: "json"
                        , complete: function (jj, qq) {
                        }
                    },
                    idField: "Id"
                },
                pageable: true,
                pageSize: 100,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    model: {
                        id: "Id",
                        fields: {
                            "Id": { "type": "number" },
                            "Name": { "type": "string" },
                        }
                    }
                }
            });
        }
    }

}(jQuery));