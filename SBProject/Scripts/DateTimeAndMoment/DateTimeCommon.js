﻿function getDateStr(dateTypeID, value) {
   
    if (!dateTypeID){
        return "";
    }
    if (value === null) {
        return "";
    }

    if( dateTypeID==1) {
        return moment(value).format('jYYYY/jMM/jDD')
    } else {
        return moment(value).format('YYYY/MM/DD')
    }

}


function getDateTimeStr(dateTypeID, value) {

    if (!dateTypeID) {
        return "";
    }
    if (value === null) {
        return "";
    }

    if (dateTypeID == 1) {
        return moment(value).format('jYYYY/jMM/jDD HH:mm')
    } else {
        return moment(value).format('YYYY/MM/DD HH:mm')
    }

}

function getDateTimeStrWithSeconds(dateTypeID, value) {

    if (!dateTypeID) {
        return "";
    }
    if (value === null) {
        return "";
    }

    if (dateTypeID == 1) {
        return moment(value).format('jYYYY/jMM/jDD HH:mm:ss')
    } else {
        return moment(value).format('YYYY/MM/DD HH:mm:ss')
    }

}

function getDateStrFromUTC(dateTypeID, value) {

    if (!dateTypeID) {
        return "";
    }
    if (value === null) {
        return "";
    }
    var localTime = moment.utc(value).toDate();
    if (dateTypeID == 1) {
        return moment(localTime).local().format('jYYYY/jMM/jDD')
    } else {
        return moment(localTime).local().format('YYYY/MM/DD')
    }

}


function getDateTimeStrFromUTC(dateTypeID, value) {

    if (!dateTypeID) {
        return "";
    }
    if (value === null) {
        return "";
    }
    var localTime = moment.utc(value).toDate();
    if (dateTypeID == 1) {
       
        return moment(localTime).local().format('jYYYY/jMM/jDD HH:mm')
    } else {
        return moment(localTime).local().format('YYYY/MM/DD HH:mm')
    }

}

function getDateTypeCulture(dateTypeID) {

    if (!dateTypeID) {
        return "";
    }
    if (dateTypeID === null) {
        return "";
    }

    if (dateTypeID == 1) {
        return "fa-IR";
    } else {
        return "";
    }

}