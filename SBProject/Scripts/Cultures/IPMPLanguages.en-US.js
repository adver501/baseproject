﻿generalResource = {
    culture: "en-US",
    code: "Code",
    amount: "Amount",
    values: "Values",
    selectProject: "Select Project",
    field: "Field",
    link: "link",
    organizationUnit: "Organization Unit",
    previousStep: "Previous Step",
    nextStep: "Next Step",
    show: "Show",
    schedule: "Schedule",
    projectWBS: "Project WBS",
    dateSequences: "Date Sequences",
    jalali: "Jalali",
    georgian: "Georgian",
    daily: "Daily",
    weekly: "Weekly",
    monthly: "Monthly",
    files: "Files",
    downloadFile: "Download File",
    editFile: "Edit File",
    newFile: "New File",
    deleteFile: "Delete File",
    file: "File",
    pleaseWaiting: "please waiting",
    map: "Map",
    text: "Text",
    value: "Value",
    volumn: "Volumn",
    continent: "Continent",
    country: "Country",
    province: "Province",
    county: "County",
    township: "Ttownship",
    villageship: "Villageship",
    village: "Village",
    city: "City",
    direction: "ltr",
    defaultAligh: "left",
    status: "Status",
    number: "Number",
    name: "Name",
    description: "Description",
    description2: "Description",
    descriptions: "Descriptions",
    minute: "Minute",
    hour: "Hour",
    day: "Day",
    date: "Date",
    financial: "Financial",
    total: "Total",
    title: "Title",
    type: "Type",
    addWindowTitle: "Add New",
    editWindowTitle: "Edit",
    completeData: "Please complete data",
    add_succeed: "Item(s) successfully added",
    do_succeed: "Item(s) successfully",
    edit_succeed: "Changes successfully commited",
    delete_succeed: "Item(s) successfully deleted",
    errorOccured: "There is a problem with the operation",
    Save: "Save",
    SaveAndClose: "Save and Close",
    SaveAndStartWorkFlow: "Save and Start WorkFlow",
    Cancel: "Cancel",
    ok: "Ok",
    operationSucceed: "Operation successfully done",
    operationDone: "Operation Done!",
    acction_failed: "Action failed. Please contact the support team",
    operation_unsuccessful: "The Operation Was Unsuccessfull",

    projectName: "Project Name",
    projectCode: "Project Code",
    projectManagementMethod: "Project Management Method",
    NoRecordsFound: "No Records Found",

    search_showing: "Showing",
    search_of: "Of",
    search_showMore: "More Results",

    ItemHasRelatedItems: "Item has a related Item(s)",

    remove_confirmation: "Are you sure you want to delete this item?",
    removeMulti_confirmation: "Are you sure you want to delete selected items?",
    removeLink_confirmation: "Are you sure you want to delete the current item's link",
    removeMultiLink_confirmation: "Are you sure you want to delete current items' link",
    remove_succeed: "Item deleted successfully",
    removeMulti_succeed: "Items deleted successfully",
    removeLink_succeed: "The link to the item successfully removed",

    toolbar_edit: "Edit",
    toolbar_remove: "Delete",
    toolbar_acctionable: "Accrionable Items",
    toolbar_viewable: "Viewable Items",
    toolbar_userCreated: "Items Created By Me",

    isNotInRange: "The value is not in the allowed range",
    invalidValue: "The value is invalid",
    noChanges: "No changes has been made",

    select_emptyValue: "empty value",
    select_noChange: "no change",
    select_none: "None",
    selection_noItemSelected: "No item selected",
    selection_optionLabel: "Please Select...",
    loading: "Loading...",

    project: "Project",

    dynamicDetailsTab: "Other Details",
    workflowTab: "workflow",
    PublicDocumentsAttached: "Public Documents Attached",

    export: "Export",
    exportToExcel: "Export to Excel",
    exportToPDF: "Export to PDF",

    generalDataFormItemTitle: "General",
    BadRequest: "The values entered are not correct. Please try again",

    unKnown: "Not Specified",

    newSave: "Registr New Information",
    editInfo: "Editing Information",
    alertDelete: "Are You Sure You Want To Delete This Row?",
    hasDependencyItem: "Delete is not possible Because the row has dependent Items.", //حذف این ردیف ، به علت وابستگی امکان پذیر نمی باشد,

    alertDeleteHasDependent: "All of dependent Items will be deleted\nAre You Sure You Want To Delete This Row?",

    generalSettings: "General Settings",

    gridSettingsSaved: "Grid settings saved successfully",
    gridSettingsReset: "Grid settings reseted successfully",

    grid_filter_contains: "Contains",
    grid_filter_startsWith: "Starts With",
    grid_filter_endsWith: "Ends With",
    grid_filter_equalsTo: "Equals To",
    grid_row: "Row",

    grid: "grid",
    chart: "chart",

    duration_min: "Minut",
    duration_hor: "Hour",
    duration_day: "Day",
    duration_mon: "Month",

    fieldDataEntryIsRequired: "DataEntry For This Field Is Required",

    do: "Perform",
    result: "Results",
    default: "Default",
    error: "Error",
    Notice: "Notice",
    Items: "Items",
    Pixel: "Pixel",

    planProgress: "Plan Progress",
    actualProgress: "Actual Progress",
    weight: "Weight",
    disciplineWeight: "Discipline Weight",

    //Tree operations
    addRootTitle: "Add New Root",
    addBranchTitle: "Add New Branch",
    editBranchTitle: "Edit Branch",
    selectNodeToAddBranch: "Select item to add branch",

    gridSettings: "Grid Settings",
    saveGridSettings: "Save Grid Settings",
    resetGridSettings: "Reset Grid Settings",

    logic_and: "and",
    logic_or: "or",

    stakeholder: "Stakeholder",
    drophere: "Drop here"

};

organizationUnitStrs = {
    OrganizationUnitType: "Organization Unit Type",
};

progressDetailsStrs = {
    grid_Title: "Title",
    grid_ActivityCode: "WBS Path",
    grid_StartProgressActual: "Period Start actual progress",
    grid_StartProgressPlan: "Period Start plan progress ",
    grid_EndProgressActual: "Period End actual progress",
    grid_EndProgressPlan: "Period End plan progress",
    grid_WeightOfWhole: "Weight Of Whole",
    grid_Percent: "Effeciently Percent",
    lable_StartDate: "Period Start Date",
    lable_EndDate: "Period End Date",
    lable_TotalItemCount: "Items Count",
};


genderList = [
    { ID: 1, Title: "Male" },
    { ID: 2, Title: "Female" }
];

operators_string = {
    eq: "Is equal to",
    neq: "Is not equal to",
    startswith: "Starts with",
    contains: "Contains",
    doesnotcontain: "Does not contain",
    endswith: "Ends with",
    e: "Is Empty",
    ne: "Is Not Empty"
};
operators_number = {
    eq: "Is equal to",
    neq: "Is not equal to",
    gte: "Is greater than or equal to",
    gt: "Is greater than",
    lte: "Is less than or equal to",
    lt: "Is less than",
    b: "Between two numbers",
    nb: "Not between two numbers",
    n: "Is null",
    nn: "Is not null"

};
operators_date = {
    eq: "Is equal to",
    neq: "Is not equal to",
    gte: "Is after or equal to",
    gt: "Is after",
    lte: "Is before or equal to",
    lt: "Is before"
};
operators_enums = {
    eq: "Is equal to",
    neq: "Is not equal to"
};

operators_boolean = {
    istrue: "Is true",
    isfalse: "Is false"
};

dynamicField = {
    dynamicDetailsEmpty: "Field is not defined",
    EntityType: "Entity Type",
    Order: "Sequence",
    ErrorMessage: "Error Message",

    ListTopValue: "No Item Selected Expression",
    DefaultValue: "Default Value",

    InputFieldSize: "Input Field Size",
    MaximumCharacters: "Maximum Characters",
    IsNumeric: "Is Numeric",
    IsRequired: "Is Required",
    IsReadOnly: "Is Read Only",
    DependentFieldFieldNo: "Dependent Field Number",
    Active: "Is Active",
    DependentField1Name: "Main Dependent Field Name",
    DependentField2Name: "Sub Dependent Field Name",

    NameFieldNo1: "Main Field Name",
    ErrorMessageFieldNo1: "Main Field Error Message",
    ListTopValueFieldNo1: "Main Field No Item Selected Expression",
    RequiredFieldNo1: "Main Field",

    NameFieldNo2: "Sub Field",
    ErrorMessageFieldNo2: "Sub Field",
    ListTopValueFieldNo2: "Sub Field",
    RequiredFieldNo2: "Sub Field",

    DependentFieldItemName: "Item Name",
    entityTypeNotSet: "Entity Type Not Set",
    dependentInfo: "Dependent Information",
    dependentInfoEdit: "Edit Dependent Item",
    dependentItems: "Dependent Items",
    mainDependentItems: "Main Items",
    insertItemRequired: "please insert atleast one item "
};
customFieldType =
    [
        { Title: "text box", ID: 1 },
        { Title: "MultiLine Text box", ID: 2 },
        { Title: "drop down list", ID: 4 },
        { Title: "Radio botton", ID: 6 },
        { Title: "Check box", ID: 8 },
        { Title: "dependent Items", ID: 10 },
        { Title: "Date", ID: 13 },
    ];
depFieldNumbers =
    [
        { Title: "two levels", ID: "2" }
    ];

checkBoxDefaultValue =
    [
        { Title: "selected", ID: "1" },
        { Title: "not selected", ID: "2" }
    ];
pleaseSelect = {
    selectWbsItem: "Please Select WBS Item",
    selectProjectPlan: "Please Select Project Plan",
    selectProjectWBS: "Please Select Project WBS",
    select: "Please Select",
    selectOrSearch: "Please Select or Serach",
    pleaseCompleteRequiredFields: "please Complete Required Fields",
    pleaseSelectMethod: "Please Select one Method"
};

fiscalYearListStrs = {
    window_add: "Add Fiscal Year",
};

ProjectTypes = {
    DeletionProjectPossible: "Deletion Due To Project Dependency Is Not Possible.",
    DeletionDependencePatternPossible: "Deletion Is Not Possible Due To The Dependence On The Failure Pattern.",
};

ContractType = {
    RemovalPossibleDependencyContracts: "Removal Is Not Possible Due To Dependency On Contracts.",
};

projectGroup = {
    GreaterThanZero: "The Amount Of Time Weight Or Cost Should Be Greater Than Zero",
    ShouldBe100: "Total Weight Of Time And Cost Should Be 100",
    NoRecordsFound: "No Records Found",
    Creation: "Creation",
    CreateSubcategoriesIn: "Create Subcategories In",
    NameOfGroup: "Name Of Group",
    Icon: "Icon",
    MethodWeightingSubProjects: "The Method Of Weighting Sub-Projects",
    Handy: "Handy",
    BasedOnTime: "Based On Time",
    BasedOnCost: "Based On Cost",
    BasedOnTimeAndCost: "Based On Time And Cost",
    TimeWeight: "Time Weight",
    CostWeight: "Cost Weight",
    Equally: "Equally",
    ProjectsHaveBeenSuccessfullyAdded: "Projects Have Been Successfully Added.",
    WeighingWasDone: "Weighing Was Done.",
    projectName: "project Name",
    ProjectWeight: "Project Weight",
    ProjectAutoInsert: "Project Auto Insert",
    InsertProject: "Insert Project",
    TheOperationWasUnsuccessful: "The Operation Was Unsuccessful.",
    Code: "Code",
    ContractNumber: "Contract Number",
    ContractDate: "Contract Date",
    Plan: "Plan",
    Location: "Location",
    ProjectStatus: "Project Status",
    TypeOfProject: "Type Of Project",
    TypeOfProjectCurrency: "Type Of Project Currency",
    TypeOfProjectDate: "Type Of Project Date",
    ProjectManager: "Project Admin",
    ProjectManagerUsername: "Project Admin Username",
    Project: "Project",
    Select: "Select ...",
    SelectFilde: "... Select ...",
    EnteringThisFieldIsRequired: "Entering This Field Is Required",
    Weight: "Weight",
    RebalancingBalancingIsDone: "Rebalancing Balancing Is Done",
};

ReportHierarchy = {
    NameOfHierarchy: "Name Of Hierarchy",
    DescriptionOfHierarchy: "Description Of Hierarchy",
    Up: "Up",
    UpInactive: "Up",
    Down: "Down",
    DownInactive: "Down",
    CreateTitle: "Create",
    CreateSubTitle: "Create Sub",
    EditTitle: "Edit",
};

projectSettings = {
    showLastNotApproveProgress: "Show last unconfirmed progress",
    registerFirstProgress: "First progress date",
    workFlowStarted: "Workflow start date",
    registerHundredProgress: "Hundred progress date",
    workFlowFinished: "Workflow finished date",
    dataFormItemActualStartDate: "Actual start date for data form item",
    dataFormItemActualEndDate: "Actual end date for data form item",
    canEditActualDate: "Can edit actual start and end date",
    projectSettingsTitle: "Project settings",
    showDatePart: "Show date part in project details ",
    projectDFIAccessIsDisciplineBased: "Check user discipline for accessing Dataform Items",

};

ProgressSettings = {
    DelayReasonInputEnabled: "Delay Reason Input Enabled",
    DelayReasonInputMandatoryMarginPercent: "Delay Reason Input Mandatory Margin Percent",
    DelayReasonInputEnabledMarginPercent: "Delay Reason Input Enabled Margin Percent",
    AllowedActualMoreThanPlanPercent: "Allowed Actual More Than Plan Percent",
};


gdmSettings = {
    MaxFileSize: "Max File Size",
    AllowedFileExtensions: "Acceptable File Formats (Comma Separated, * Blank Means To Accept All Formats)",
    CreateMD5Checksum: "Create MD5 Checksum",
    DenyDownloadIfChecksumNull: "Deny Down load If Checksum Null",
    DenyDownloadIfChecksumDiffer: "Deny Down load If Checksum Differ",
};

dashboardStrs = {
    totalView: "Overview",
    project: "Project",
    projectGroup: "Project Group",
    widget_add_succeed: "Widget added successfully",
    widget_settingsSaved_succeed: "Widget general settings saved successfully",
    widget_configSaved_succeed: "Widget specific config saved successfully",
    widget_onlySupports_project: "This widget currently supports project view only",
    widget_onlySupports_projectGroup: "This widget currently supports project group view only",
    widget_onlySupports_projectOrProjectGroup: "This widget currently supports project and project group view only",
    widget_onlySupports_total: "This widget currently supports total view only",
    widget_onlySupports_totalOrProjectGroup: "This widget currently supports total view and projectGroup view only",
    widget_onlySupports_ProjectOrProjectGroup: "This widget currently supports project view and projectGroup view only",
    layout_save_succeed: "Dashboard layout saved successfully",
    youDoNotHaveViewPermission: "You do not have View permission",

    widgetTotalSettings: "Widget's general settings",
    widgetConfigs: "Widget's specific configs",
    widgetHasNoConfig: "The widget has no specific configs",

    layout_replaceWithDefaultConfirm: "Do you want the layout to be replaced by default layout?",
    layout_replacedWithDefaultInfo: "The layout of this section has been replaced by the default layout. If you prefer this layout to yours you han keep it by clicking 'Save Current Layout'  ",
    selective_windowTitle: "Select tabs",
    selective_addWindowTitle: "Add Item to Dashboard Tabs",
    selective_aeSelectedItem: "Selected Item",
    selective_itemAlreadyAdded: "",
    projectsFilterWin_title: "Filter Projects",

    onlineUsersCount: " Online Users Count",
    filtered: "filtered",

    noDefaultProjectPlan: "Default project plan is not selected for the project",
    noDefaultProjectWBS: "Default project WBS is not selected for the project",
    noDefaultProjectWBSOrProjectPlan: "Default plan Or WBS is not selected for the project",

};

Menu = {
    AccountType: "Account Type",
    Select: "Select ...",
    NewBranch: "Create a New Branch",
    NewBranchInactive: "Create New Branch(Inactive)",
    NewSubfolder: "Create a New Subfolder",
    NewSubfolderInactive: "Create a New Subfolder(Inactive)",
    Edit: "Edit",
    EditDisabled: "Edit(Disabled)",
    Delete: "Delete",
    DeleteDisabled: "Delete(Disabled)",
    Up: "Up(Row Layout)",
    UpInactive: "Up(Inactive)",
    Down: "Down(Row Layout)",
    DownInactive: "Down(Inactive)",
    TreeManageMenus: "Tree Manage Menus",
    DefaultTitle: "Default Title",
    FarsiTitle: "Farsi Title",
    EnglishTitle: "English Title",
    Address: "Address",
    Example: "Example",
    Plugin: "Plugin",
    None: "None",
    ShowForEveryone: "Show For Everyone",
    Active: "Active",
    ProjectIDRequired: "Project ID Required",
    Visible: "Visible",
    Enabled: "Enabled",
    Selectable: "Selectable",
    OpenInNewPage: "Open In New Page",
    OrganizationalRoles: "Organizational Roles",
    CreateMenuAccess: "Create Menu Access",
    CreateMenuAccessInactive: "Create Menu Access (Inactive)",
    RemoveMenuAccessFromRole: "Remove Menu Access From Role",
    RemoveMenuAccessFromRoleInactive: "Remove Menu Access From Role (Inactive)",
    AllocatedOrganizationalRoles: "Allocated Organizational Roles",
    SystemManager: "System Manager",
    projectManager: "Project Admin",
    NormalUser: "Normal User",
    TheGivenDataRowWasNotFound: "The Given Data Row Was Not Found.",
    PleaseFillInTheRequiredValues: "Please Fill In The Required Values.",
    ProblemConnectingToServerPleaseTryAgain: "Problem Connecting To Server, Please Try Again.",
    CreateNewMenu: "Create New Menu",
    CreateSubDirectoryMenu: "Create Sub-Directory Menu",
    EditTheMenuInformation: "Edit The Menu Information",
    DeleteMenuBecauseSubcategory: "There Is No Way To Delete This Menu Because Of The Subcategory",
    RecordNewInformation: "Record New Information",
    Title: "Title"
};

workFlowManagerStrs = {
    commentIsMandatory: "You should enter comment for this step",

    stateTypes: "State Type",
    rolesToAcceptOrReject: "Pathes (roles) wich should still decide in this step",
    pathAccepted: "Pathes (roles) accepted",
    pathRejected: "Pathes (roles) rejected",
    acceptsNeededToGoToNextStep: "Number of accepts needed to go to next step",
    rejectsNeededToGoToPreviousStep: "Number of rejects needed to go to next step",

    message_noModelSelected: "Workflow model is not choosed yet. Please choose one model", //'مدل چرخه هنوز مشخص نشده است. لطفا مدلی را انتخاب نمایید',
    message_noAccessToWorkflowHistory: "You do not have proper access for visiting the history of the workflow", //'شما دسترسی لازم برای مشاهده تاریخچه چرخه را ندارید',
    message_noAccessToWorkflowComments: "You do not have proper access for visiting the comments of the workflow", //'شما دسترسی لازم برای مشاهده نظرات وارد شده در چرخه را ندارید',
    message_noAccessToStateDetails: "You do not have proper access for visiting the detail status of the workflow", //'شما دسترسی لازم برای مشاهده جزئیات وضعیت چرخه را ندارید',

    startWorkflow: "Start Workflow",
    restartWorkflow: "Reset Workflow",

    tabs_workflowHistory: "Workflow History",
    tabs_comments: "Comments",
    tabs_currentStateDetails: "Current Step Details",

    steps_followingStepLabel: "Accept and send to next step (Please select one)",
    steps_nextStepLabel: "Accept/Next Step",
    steps_previousStepLabel: "Reject/Previous Step",
    steps_commentStepLabel: "Comment",
    dynamicParallel_saveConditions: "Save Acceptance Paths",

    eventComments: "Comment Accept/Reject",

    grid_history_time: "Time",
    grid_history_logType: "Event Type",
    grid_history_desc: "Description",
    grid_history_fromState: "From",
    grid_history_toState: "To",
    grid_history_user: "Actor",

    grid_comments_time: "Time",
    grid_comments_user: "Actor User",
    grid_comments_acceptorTitle: "Actor Title",
    grid_comments_commentBody: "Comment",
    grid_comments_logtype: "Event Type",

    steps_turnBackStepLabel: "Return",
    lastApprovedProgress: "last approved progress",
    lastTempProgress: "last unapproved progress",
    nameIsDuplicate: "Workflow name is duplicate"
};

generalAsyncFileResource = {
    select: "select file ...",
    remove: "remove",
    cancel: "cancel",
    previouslyRemoved: "Item has already been removed",
    dropFilesHere: "Drop files here to upload",
    statusUploading: "Uploading...",
    statusUploaded: "Done",
};

generalGridResource = {
    permissions: "Permissions",
    creationTime: "Creation Time",
    creator: "Creator",
    lastModificationTime: "Last Modification Time",
    lastModifier: "Last Modifier",
    total: "Total",
};

generalWindowResource = {
    edit: "Edit item",
    add: "Add new item"
};

contractAmendmentResource = {
    window_add: "Add Contract Amendment",
    window_edit: "Edit Contract Amendment",
};

relatedGeneralDocumentManagementResource = {
    grid_title: "Title",
    grid_fileName: "File Name",
    grid_downloadLatestVersion: "Get the latest version",
    grid_sendDate: "Send Date",
    grid_receiveDate: "Receive Date",
    grid_senderGeneralDocumentNumber: "Sender Document Number",
    grid_generalDocumentNumber: "Receiver Document Number",
    grid_generatorTypeTitle: "Generator",
    grid_generalDocumentVersionTitle: "Version",
    grid_generalDocumentTypeTitle: "Type",
    grid_seen: "Seen",
    getLatestVersion: "Get Latest Version",

    window_add: "Add General Document",
    window_edit: "Edit General Document",
    toolbar_add: "Attach new general document",
    toolbar_add_dis: "Add general document is disabled or you don't have permission to add",
    toolbar_edit: "Edit general document",
    toolbar_remove: "Remove general document",
    toolbar_security: "General Document Security Settings",
    toolbar_security_dis: "",
    toolbar_edit_dis: "",
    toolbar_remove_dis: "",


    grid_receiver: "Receiver",
    grid_sender: "Sender",
    grid_comments: "Comments",
    window_versionFiles: "Files of Versions",
    window_removeTitle: "Delete Selected File",
    window_uploadNewFile: "Upload new file",
    noAccess: " You do not have permission to edit or view the related documents.",

    error_nofileSelected: "",
    error_fileSizeIsBigger: "File size exceeds maximum alowed file size. Maximum is :",
    error_fileExtensionForbidden: "This file extension is not allowed. File extension is:",
    error_fileIsInfected: "The file is infected",
    error_generalUploadError: "General upload error",
    error_pleaseEnterDocumentTitle: "Please enter document title",
    error_maximumAttachementCountHasBeenPassed: "Number of attachements are more than allowed",
    error_gettingDocumentGenerators: "Error in getting document generators",
    error_gettingDocumentTypes: "Error in getting document types",
    error_gettingDocumentVersions: "Error in getting document versions",

    messages_totalAttachmentSize: "Maximum size of attached files should not be more than {0} Kilobytes",
    messages_totalAttachmentCount: "You can attach {0} files in maximum",

    agg_dfi_HasNoAccess: "You do not have access to this section. You shoud either have managament or 'DFI Aggregated Docs' permission in project to use this section",
};

gdmManagementStrs = {
    folderWin_addBranchTitle: "Add new sunfolder",
    folderWin_addRootTitle: "Add new root",
    folderWin_editBranchTitle: "Edit branch",
    folderHasSubfolders: "Folder has subfolders. Please delete them first",
    search_noParameter: "No search parameter specified",
    noFolderSelected: "Please select a folder to add sub folders",
};

invoiceList = {
    grid_title: "Title",
    grid_invoiceNumber: "invoice Number",
    grid_projectName: "project Name",
    grid_contractTitle: "contract Title",
    grid_contractNumber: "contract Number",
    grid_status: "status",
    grid_subContractor: "subContractor",
    grid_dateIssued: "date Issued",
    grid_dateReceived: "date Received",
    grid_isTemp: "isTemp",
    grid_Temp: "Temporary",
    grid_Permanently: "Permanentl",
    grid_previousAccepted: "Previously Accepted",
    grid_previousAccepted_Currency: "Accepted Amount to day (Multi-Currency)",
    grid_amount: "amount",
    grid_dateAccepted: "Date Accepted",
    grid_finalAcceptedAmount: "Final Accepted Amount",
    grid_TotalAmount_Currency: "Total Amount (Multi-Currency)",
    grid_finalAcceptedAmount_Currency: "Current Period Amount (Multi-Currency)",
    grid_contractAmount: "Contract Amount",
    grid_contractTotalAmount: "Contract Total Amount",
    grid_valueAddedTax: "Value Added Tax",
    grid_TotalAmount: "Total Amount",
    grid_AcceptedAmount: "Accepted Amount",
    aewindows_InsertCurrency: "Insert From other Currencies",

    window_add: "Create New Invoice",
    window_edit: "Edit Invoice",
    toolbar_add: "Create new invoice",
    toolbar_add_dis: "Create new invoice is disabled or you don't have permission to it",
    toolbar_edit: "Edit invoice",
    toolbar_remove: "Remove invoice",
    toolbar_edit_dis: "Edit invoice is disabled or you don't have permission to it",
    toolbar_remove_dis: "Remove invoice is disabled or you don't have permission to it",

    generalTab: "General",
    selectContractRequired: "Please select conract."
};

cbsStrs = {
    newCBSSubBranchWinTitle: 'Add New CBS Sub Brnch',
    newCBSRootWinTitle: 'Add New CBS Root',
    editCBSWinTitle: 'Edit CBS',
    removeError_hasSubItems: "You can not remove this CBS" + "\n" + "This CBS has sub branch, please remove them first",
    removeError_hasPayments: "You can not remove this CBS" + "\n" + "This CBS has related payments",
    removeError_hasContracts: "You can not remove this CBS" + "\n" + "This CBS has related contracts",
    removeError_hasInvoices: "You can not remove this CBS" + "\n" + "This CBS has related invoices",
    removeError_hasCashflow: "You can not remove this CBS" + "\n" + "This CBS has related cashflows",
    title: "Title",
    description: "Description",
    weight: "Weight",
    budget: "Budget",
    chart_projectPayementsChart: "Time phased payments of the project",
    chart_selectedCBSPayementsChart: "Time phased payments related to the selected CBS",
    chart_totalPayments: "Total Payments In the Period",
    chart_totalComulativePayments: "Total Cumulative Payments In the Period ",
};

multiCurrencyCostStrs = {
    multiCurrencyCostDataEntry: "Multi Currency Data Entry",
    enteredItemsCount: "Entered Items: ",
    currencyValueNotSet: 'Currency Volumn Not Specified',
    currencyTypeNotSet: 'Currency Type Not Set',
    totalBaseCurrencyValue: "Base Currency Value",
}

issueList = {
    grid_title: "Title",
    grid_Description: "Description",
    grid_CategoriesTitle: "Category Title",
    grid_StatusName: "Status",
    grid_AssignedUserDetailsFistLastName: "Assigned user detail name",
    grid_DueDate: "Due Date",
    grid_IdentificationDate: "Identification Date",
    grid_CreatedBy: "Created By",
    grid_LastModifiedBy: "Last Modified By",
    grid_CreationTime: "Creation Time",
    grid_LastModificationTime: "Last Modification Time",
    grid_Severity: "Severity",
    FieldName: "Field Name",
    ActionUser: "Action User",
    FromValue: "From Value",
    ToValue: "To Value",
    ActionDate: "Action Date",
    ActionDescription: "Action Description",
    IssueHistoryWindowTitle: "History",
    NewIssueWindowTitle: "New Issue",
    EditIssueWindowTitle: "Edit Issue ",
    IssueStatus_title: "Titel",
    IssueStatus_description: "Description",
};

riskList = {
    grid_title: "Title",
    grid_Description: "Description",
    grid_CategoriesTitle: "Category Title",
    grid_StatusName: "Status",
    grid_AssignedUserDetailsFistLastName: "Assigned user detail name",
    grid_DueDate: "Due Date",
    grid_IdentificationDate: "Identification Date",
    grid_CreatedBy: "Created By",
    grid_LastModifiedBy: "Last Modified By",
    grid_CreationTime: "Creation Time",
    grid_LastModificationTime: "Last Modification Time",
    grid_Severity: "Severity",
    grid_type: "Risk Type",
    grid_probability: "Probability",
    FieldName: "Field Name",
    ActionUser: "Action User",
    FromValue: "From Value",
    ToValue: "To Value",
    ActionDate: "Action Date",
    ActionDescription: "Action Description",
    positive: "Positive",
    negative: "Negative",
    history: "History",
    addOrEditRisk: "Add or Edit Risk",
};

numberingFormats = {

    part_toolbar_add: "New Part",
    part_window_add: "Part Details",
    window_add: "Format Details",
    valueIsNotValid: "Value is not valid",
    formatParts: "Format Parts",
    editSeperator: "Edit Separator",
    parts: "Parts",
    formatExistsForField: "numbering format for the field of entity has already created "

};

ProjectStatusStrs = {
    ProjectIsInThisStatus: "Project In This Situation",
    Deactivated: "Is Deactivated",
    Activated: "Is Activated",
};

workSpace = {
    noAccess: "You do not have permission to access project workspace",
    noDefaultProjectPlan: "Default project plan is not selected for the project",
    noDefaultProjectWBS: "Default project WBS is not selected for the project",
    assginDFIToWbsConfirm: "Assign the selected dataform item to the selected WBS? Selected WBS:",
    assginMultiDFIToWbsConfirm: "Assign free dataformitems to the selected wbs?",

    wbsToolbar_add: "Add Branch",
    wbsToolbar_addRoot: "Add Root",
    wbsToolbar_edit: "Edit",
    wbsToolbar_remove: "Remove",
    wbsToolbar_resourceUsage: "Resource Usage",
    wbsToolbar_resourceAssign: "Resource Assign",

    wbsToolbar_add_dis: "Add Branch",
    wbsToolbar_addRoot_dis: "Add Root",
    wbsToolbar_edit_dis: "Edit",
    wbsToolbar_remove_dis: "Remove",
    wbsToolbar_resourceUsage_dis: "Resource Usage",
    wbsToolbar_resourceAssign_dis: "Resource Assign",

    dfiToolbar_add: "",
    dfiToolbar_addMenu: "",
    dfiToolbar_edit: "",
    dfiToolbar_remove: "",
    dfiToolbar_add_dis: "",
    dfiToolbar_addMenu_dis: "",
    dfiToolbar_edit_dis: "",
    dfiToolbar_remove_dis: "",

    dfiToolbar_removeLink_dis: "",
    dfiToolbar_removeLink: "",

    dfi_noProgressEditPermission: "You do not have permission to edit the progress of this item.",

    dfi_activityCode: "Activity Code",
    dfi_actualEndDate: "Actual End Date", //'تاریخ واقعی پایان',
    dfi_actualStartDate: "Actual Start Date", //'تاریخ واقعی شروع',
    dfi_detailsTab: "Item Specific Details", //'مشخصات تخصصی آیتم',
    dfi_discipline: "Discipline", //'دیسیپلین',
    dfi_duration: "Item Duration", //'مدت زمان آیتم',
    dfi_financialProgress: "Financial Progress", //'پیشرفت مالی',
    dfi_forecastEndDate: "Forecast End Date", //'تاریخ پیش بینی پایان	',
    dfi_generalNoEditPermission: "You do not have proper access to edit these kind of informations", //'شما دسترسی لازم را برای ویرایش این قلم کاری ندارید',
    dfi_generalTab: "General", //'تنظیمات عمومی',
    dfi_generalTitleRow: "Item Specifications", //'مشخصات آیتم',
    dfi_invoiceProgress: "Invoice Progress", //'پیشرفت صورت وضعیت',
    dfi_otherDetailsTab: "Other Details", //'جزئیات دیگر',
    dfi_physicalProgress: "Physical Progress", //'پیشرفت فیزیكی',
    dfi_planEndDate: "Plan End Date", //'تاریخ پایان برنامه ریزی شده	',
    dfi_planManHour: "Plan Man-Hour", //'نفرساعت برنامه ریزی شده',
    dfi_planningDetailsTab: "Planning Details", //'برنامه ریزی کلی',
    dfi_planningStepsTab: "Planning Steps", //'زمانبندی مراحل',
    dfi_planStartDate: "Plan Start Date", //'تاریخ شروع برنامه ریزی شده',
    dfi_planTitleRow: "Item Plan Timing", //'برنامه زمانبندی آیتم',//////
    dfi_progressChart: "Progress Chart", //'نمودار پیشرفت',
    dfi_progressCheckDate: "Progress Check Date", //'تاریخ پیشرفت',
    dfi_progressPercent: "Progress Percent", //'درصد',
    dfi_progressPlan: "DataFormItem Progress Plan Procedure", //'روش محاسبه پیشرفت برنامه ای',
    dfi_progressPlanProcedure: "DataFormItem Progress Plan Procedure", //'روش محاسبه پیشرفت برنامه ای',
    dfi_progressPlanProcedureWarn: "When not Defined, the Progress Would be Calculated Linearly ", //'در صورت عدم انتخاب روش محاسبه، پیشرفت به صورت خطی زمانی محاسبه میشود',
    dfi_progressTab: "Progress", //'پیشرفت	',
    dfi_progressTable: "Progress Table", //'جدول پیشرفت	',
    dfi_relatedDocsTab: "Related Documents", //'مدارک الصاق شده',
    dfi_title: "Item Title", //'عنوان آیتم',
    dfi_updateProgress: "Update Progress", //'به روز رسانی',
    dfi_volumnOfWork: "Total Volumn Of Work", //'کل حجم پیش بینی شده',
    dfi_volumnOfWorkUnit: "Volumn Of Work Unit", //'واحد حجم کار آیتم',
    dfi_weightOfActivity: "Weight To Activity Percent",
    dfi_weightOfWhole: "Weight To Project Percent",
    dfi_wFModel: "Item Workflow", //'چرخه آیتم',
    dfi_workflow: "Item Workflow", //'چرخه آیتم',
    dfi_workflowTab: "Workflow", //'چرخه',
    dfi_workflowTitle: "Workflow", //'چرخه',
    dfi_delayReason: "Delay Reason",
    dfi_delayReasonComments: "Delay Reason Comments",
    dfi_progressInputTab: "Progress Input",

    autoAdd_winTitle_all_SelectedWBS: "Create DFI For All Sub Activities of Selected WBS",
    autoAdd_winTitle_emptyActivities_SelectedWBS: "Create DFI For Sub Activities of Selected WBS With No DFI",
    autoAdd_winTitle_all_Project: "Create DFI For All Activities of Project",
    autoAdd_winTitle_emptyActivities_Project: "Create DFI For All Activities of Project WBS With No DFI",

    autoAdd_PleaseSelectExcel: "Please select an excel file",
    autoAdd_selectFile: "Select File",
    autoAdd_create: "Create",
    autoAdd_createOneDFIForEachActivity: "Create an item for each activity",
    autoAdd_createDFIByExcel: "Create items due to the excel file",

    wbs_ewinTitle: "Edit WBS",
    wbs_awinTitle: "Create New WBS",
    dfi_ewinTitle: "Edit Data Form Item",
    dfi_awinTitle: "Create Data Form Item",
    multiDfi_winTitle: "Edit Multi Data Form Item",

    wbsToolbar_search: "Search",
    wbsToolbar_settings: "Settings",
    wbsToolbar_gantt: "Project Gantt Chart",
    totalSettingWin_title: "Total workspace settings",
    search_pleaseEnterAtLeastThreeWords: "Please enter at least 3 letters",
    showType_wbs: "By WBS",
    showType_free: "Free Items",
    showType_project: "All Project Items",
    showType_mine: "My Items",

    setAllSubDFIParamsWin_title: "Change Attroibutes of All Sub DFIs"

};

articleResources = {
    image: "Image",
    CanView: "Show On The Site",
    active: "Active",
    Chosen: "View On First Page",
    date: "Date",
    title: "Title",
    language: "Language",
    persian: "Persian",
    abstract: "Abstract",
    body: "Body",
    save: "Save",
    cancel: "Cancel",
    preview: "Preview",
    select: "Select",
    newSave: "Registr New Information",
    editInfo: "Editing Information",
    alertDelete: "Are You Sure You Want To Delete This Row?",
    alertSuccessDelete: "Deletion Successfully Completed.",
    alertImageType: "Please Upload a Photo In The Correct Format",
};

ManageProjectProgressCalculationResources = {
    newSave: "Registr New Information",
    alertDelete: "Are You Sure You Want To Delete This Row?",
    noItemSelected: "No item selected",
    notValid: "The Entered Value Is Not Valid",
    title: "Title",
    amountTimeProgress: "The Amount Of Time Progress",
    amountInformationProgress: "The Amount Of Information Progress",
    stageTitle: "Stage Title",
    timeProgress: "Time Progress",
    value: "Value From 0 To 100",
    intelligenceProgress: "Intelligence Progress",
    save: "Save",
    cancel: "Cancel",
    alertPlannedProgress: "Please Register The Planned Progress",
    function: "The Type Of Distribution Function",
    creator: "Creator",
    creationTime: "Creation Time",
    lastEditedBy: "Last Edited By",
    lastEditedTime: "Last Edited Time",
    editInfo: "Editing Information",
    alertSuccessDelete: "Deletion Successfully Completed.",
    titleNotValid: "The Title Entered Is Not Valid",
    plannedNotValid: "The Stages Of Planned Progress Are Not Valid",
    problemsStorageProcess: "Problems The Storage Process, Please Try Again",
    linear: "Linear",
    stepping: "Stairs",
    description: "Description",
    duration: "Allowed duration of activity"
};

wbsManagementResources = {
    grid_Name: "Name",
    grid_Duration: "Duration",
    grid_Code: "Code",
    grid_Weight: "Weight",
    grid_PlanStartDate: "Start P",
    grid_PlanEndDate: "End P",
    grid_ProgressPlan: "Progress P",
    grid_ProgressActual: "Progress A",
    grid_ActualStartDate: "Start A",
    grid_ActualEndDate: "End A",
    grid_ActivityCode: "Activity Code",
    grid_noRecord: "No records to display (Possibility of Project Plan and Project WBS mismatch)",
    grid_RootTitlePath: "Root Path",
    grid_WeightPlanProgress: "Progress P based on weight",
    grid_WeightActualProgress: "Progress A based on weight",
    cannotConvertToActivity: "Cannot convert to activity",
    cannotConvertToNonActivity: "Cannot convert to non activity"
};

dfiStrs = {
    title: "Title",
    wbsPath: "WBS Path",
    ActivityCode: "Activity Code",
    DisciplineName: "Discipline",
    CurrentState: "Current State",
    WorkflowModelName: "Workflow Model Name",
    ProjectName: "ProjectName",
    PlanStartDate: "Plan Start Date",
    PlanEndDate: "Plan End Date",
    ActivityName: "Parent Activity",
    autoRelate: "Work items auto assignment"
};

dfiListResources = {
    grid_Title: "Title",
    grid_DisciplineName: "Discipline",
    grid_ActualProgress: "Progress A",
    grid_PlanProgress: "Progress P",
    grid_ActivityCode: "Activity Code",
    grid_WBSCode: "WBSCode",
    grid_ExtensionName: "Extension",
    grid_WorkflowModelName: "Workflow",
    grid_CurrentState: "Current Status",
    grid_ProjectDataFormItemProgressPlanName: "Plan Progress Calculation Method Name",
    grid_PlanStartDate: "Start p",
    grid_PlanEndDate: "End p",
    grid_ForecastEndDate: "End F",
    grid_ActualStartDate: "Start A",
    grid_ActualEndDate: "End A",
    grid_WeightOfWhole: "Weight in Project",
    grid_WeightOfActivity: "Weight in Activity",
    grid_PlanManHour: "Man Hour P",
    grid_ProjectName: "Project",
};

aeSingleDfi = {
    progressStepsGrid_stepName: "step name",
    progressStepsGrid_planDate: "plan date",
    progressStepsGrid_actualDate: "actual date",
    progressStepsGrid_isAutomatic: "is automatic",
    progressStepsGrid_isTemp: "is temp",
    progressStepsGrid_isAccepted: "Accepted",

    progressActualGrid_checkDate: "Progress Date",
    progressActualGrid_creationTime: "Creation Time",
    progressActualGrid_PhysicalProgress: "Physical Progress",
    progressActualGrid_FinancialProgress: "Financial Progress",
    progressActualGrid_InvoiceProgress: "Invoice Progress",
    progressActualGrid_Creator: "Creator",

    progressActual_From: "Minimum",
    progressActual_To: "Maximum",

    progressActual_ProgressSuccessFullySaved: "Actual progress successfully saved",
    progressActual_ProgressTemporarySavedAndWaitForAccept: "Actual progress temporary saved and will be permanent after acceptance",
    progressActual_ProgressIsMoreThanTheAllowedMarginFromPlan: "Progress is more than the allowed margin from plan",
    progressActual_DelayReasonSpecificationIsMandatoryInThisVariance: "Delay reason specification is mandatory in this variance",
};

eMultiDfi = {
    notSameProject: "Selected dataform items should be in a same project",
    notSameType: "Selected data form items should be same type",
    progressNotInSameState: "Selected items do not have same progress state",
    someHasStartedWorkflows: "You can not change workflow model because some items has started workflows",
};

StakeholdersStrs = {
    title: "Stakeholder",
    grid_name: "Name",
    grid_country: "Country",
    window_add: "Create Stakeholder",
    window_edit: "Edit Stakeholder",
    StakeholderProfile: "Contractor Profile",
    itemsSelected: "Vendors Selected",
    nationalNo: "National No",
    nationalID: "National Id",
    nationalCode: "National Code",
    registerNo: "Register No",
    registerDate: "Register Date",
    registerType: "Register Type",
    registerLocation: "Register Location",
    Address: "Address",
    PostalCode: "Postal Code",
    CEOName: "CEO Name",
    Email: "Email",
    CallNo: "Call Number",
    Fax: "Fax Number",
    fieldofactivity: "field of activity",
    FieldParentCaption: "Affiliated Stakeholder",
    IDNo: "Identity No",
    gender: "Gender",
    Birthdate: "Birth date",
    grid_LegalEntityTypeName: "Legal Type",
    grid_ParentName: "Affiliated Stakeholder",
    juridicallegal: "juridical",
    naturallegal: "natural",
    legalEntityTypeIsrequired: "Legal entity type is required",
    DetailHeaderTitle: "{0} Information",
    UneditableOrganizationUnit: "It's not possible to edit Organisation Unit in this form. Please use Organisation Unit section."

};
LegalEntityType = [
    { ID: 0, Name: "Not Set", LegalType: "" },
    { ID: 1, Name: "Juridical", LegalType: "Company" },
    { ID: 2, Name: "Natural", LegalType: "Person" },
    { ID: 3, Name: "Organizational", LegalType: "OrganizationUnit" },
];

users = {
    grid_firstName: "First Name",
    grid_lastName: "Last Name",
    grid_userName: "User Name",
    itemsSelected: "Users are selected",
    selectImage: "Select Image",
    inputPassNotEqual: "input passwords not equal",
    passIsEmpty: "passwords is empty",
    selectdUserDigSignPass: "selected user digital sign password",
    areYouSureToSync: "Are you sure to sync users with active directory?"
};

Extension = {
    grid_ExtendedControl_Name: "Name",
    grid_Extension_Name: "Module Name",
    grid_Description: "Description",
    grid_IsReport: "Is Report"
};

contractList = {
    grid_ContractNumber: "Contract Number",
    grid_Title: "Title",
    grid_SubContractorName: "SubContractor Name",
    grid_ProjectName: "Project Name",
    grid_ContractDate: "Contract Date",
    grid_ContractAmount: "Contract Amount",
    grid_ContractCurrencyAmount: "Contract Currency Amounts",
    grid_ContractTotalCurrencyAmount: "Contract Total Currency Amounts",
    grid_ContractPredictedFinalAmount: "Predicted Final Amount",
    grid_ContractPayedAmount: "Payed Amount",
    grid_Name: "Name",
    grid_CBSPath: "CBSPath",
    grid_ContractTypeName: "Type",
    grid_AmendedContractDate: "Amended Date",
    grid_AmendmentCount: "Amendment Count",
    grid_ContractTotalAmount: "Total Amount",
    grid_ContractFinishDate: "Finish Date",
    grid_ContractFinalAmount: "Contract Final Amount",
    grid_ContractedStartDate: "Start Date",
    grid_ContractAwardDate: "Award Date",
    grid_ForecastedFinishDate: "Forecasted Finish Date",
    grid_ContractStatusName: "Status",
    grid_AwardWorkflowStatus: "Award Workflow Status",
    grid_InvoiceCount: "Invoice Count",
    grid_ContractFinancialTypeName: "Financial Type",
    grid_awardWorkflow: "Contract Award Workflow",

    error_hasInvoices:
        "This Contract Has Invoices, You Must Delete Them Before Deleting This Contract.", //'این قرارداد دارای صورت وضعیت ثبت شده است. قبل از حذف میباست صورت وضعیتها حذف گردند',
    error_isDefaultContractForAProject:
        "This Item Is Project's Default Contract. Choose Another One As The Default Contract In Project's Settings To Delete This Item.", //'این آیتم، قرارداد پیشفرض پروژه است. برای حذف آن قرارداد دیگری را در بخش تنظیمات پیشفرض پروژه قرار دهید',

    removeCBSTitle: "Remove Connection to CBS",

    grid_Taskmaster: "Taskmaster",
    grid_Supervisor: "Supervisor",
    grid_Consultant: "Consultant",

    addWindowTitle: "Add New Contract",
    editWindowTitle: "Edit Contract",
    errorInGettingSettings: "Error in getting settings",
    pleaseCheckTheOrderOfSteps: "Please Check The Order Of Steps",

    contractPeriodicReportSettings: "Contract Acceptance Settings",
    contractProjectDFIRelation: 'Contract DFI Relation',
    awardWorkflowStarted: "Contract Award Workflow Started",
    awardWorkflowRestarted: "Contract Award Workflow Restarted.",

    aeContract_contactInfoTab: "Contract Details",
    aeContract_relatedProjectsTab: "Related Projects",
    aeContract_dynamicFieldsTab: "Other Settings",
    aeContract_awardWorkflowTab: "Award Workflow",
    aeContract_relatedGeneralDocumentsTab: "Related Documents",
    aeContract_awardWorkflow: "Award Workflow",
    aeContract_Description: "Description",
    aeContract_Duration: "Duration",
    aeContract_Day: "DAY",
    aeContract_FinanceSources: "Finance Sources",
    aeContract_weigthSumShouldBe100: "Weigth Sum Should Be Equal To 100",
    aeContract_requiredDynamicFieldsAreNotFilled: "Required Dynamic Fields Are Not Filled",
    aeContract_StartDateCanNotBeAfterEndDate: "Start Date Can Not Be After End Date",
    aeContract_acceptInvoiceByWF: "Accept invoice by work flow",
    aeContract_confirmRemoveAcceptInvoiceByWF: "Are you sure to disable? In case of disabling acceptance by workflow, all of the workflows of invoices will be deleted.",
    agg_contract_HasNoAccess: 'You should be project manager or have contract management permission in project to view this section',

};

amendmentListStrs = {
    addWindowTite: "Add New Amendment",
    editWindowTitle: "Edit Amendment",
    aeAmendment_requiredDynamicFieldsAreNotFilled: "Required Dynamic Fields Are Not Filled",
}

paymentListStrs = {
    grid_DocumentNumber: "Document Number",
    grid_PaiementDate: "Payment Date",
    grid_Amount: "Amount",
    grid_CostCenter: "Cost Center",
    grid_PaymentBehalf: "Payment Bahalf",
    grid_CreatedBy: "Created By",
    grid_LastModifiedBy: "Last Modified By",
    grid_CreationTime: "Creation Time",
    grid_LastModificationTime: "Last Modification Time",
    grid_Subcontractor: "Subcontractor",
    grid_ContractTitle: "Contract Name",
    grid_ProcurementName: "Procurement Name",
    grid_ContractNumber: "Contract Number",
    grid_InvoiceNumber: "Invoice Number",
    grid_CurrencyAbbriviation: "Currency",
    generalTab: "General Info",
    relatedDocsTab: "Related Docs",
    ae_project: "Project",
    ae_invoice: "Invoice",
    ae_documentNumber: "Document Number",
    ae_amount: "Amount",
    ae_paymentDate: "Payment Date",
    ae_cbs: "CBS",
    ae_costCenter: "Cost Center",
    ae_paymentBehalf: "Payment Behalf",
    ae_currencyType: "Currency",
    ae_paymentDescription: "Description",
    ae_paymentRemarks: "Remarks",
    ae_discipline: "Discipline"
};

amendmentManagment = {
    grid_AmendmentNumber: "AmendmentNumber",
    grid_TotalAmount: "TotalAmount",
    grid_AmendmentDate: "AmendmentDate",
};

ganttStrs = {
    grid_title: "Title",
    grid_startTime: "Start",
    grid_endTime: "End",
    grid_Resources: "Assigned Resources",

    //Messages

    save: "Update",
    start: "Start",
    end: "End",

    //Actions
    addChild: "Add new Child",
    append: "Add new Task",
    insertAfter: "Add Task Below",
    insertBefore: "Add Task Above",
    cancel: "Undo",
    deleteDependencyConfirmation: "Proceed with dependency deletion?",
    deleteDependencyWindowTitle: "Delete dependency?",
    deleteTaskConfirmation: "Proceed with task deletion?",
    deleteTaskWindowTitle: "Delete task?",
    destroy: "Destroy",
    pdf: "PDF Export",


    //Editor
    ed_assignButton: "Assign Resources",
    ed_editorTitle: "Edit Task",
    ed_end: "Task End",
    ed_percentComplete: "Task Progress",
    ed_resources: "Task Resources",
    ed_resourcesEditorTitle: "Assign Task Resources",
    ed_resourcesHeader: "Available Resources",
    ed_start: "Task Start",
    ed_title: "Task Title",
    ed_unitsHeader: "Resource Units",

    //Views
    vw_day: "Day view",
    vw_end: "Task End",
    vw_month: "Month view",
    vw_start: "Task Start",
    vw_week: "Week view",
    vw_year: "Year view",
};

timeSheets = {
    toolbar_add: "New Task",
    toolbar_edit: "Edit Task",
    toolbar_remove: "Delete Task",
    startTime: "Start Time",
    endTime: "End Time",
    totalTime: "Total Time",
    startTask: "Start Task",
    stopTask: "Stop Task",
    taskRowDeleteConfirm: "You sure you want to delete task?",
    todayWorkStartTime: "Today Work Start Time",
    todayWorkEndTime: "Today Work End Time",
    usefulTime: "Useful Time",
    timeIsIncorrect: "Time is incorrect",
    entranceTime: "Entrance Time",
    exitTime: "Exit Time",
    myTimeSheetReport: "My TimeSheet Report",
    pleaseSelectDate: "Please select date",
    timeSpent: "Time spent",
    workStartTime: "Work Start Time",
    workEndTime: "Work Snd Time",
    workDuration: "Work Duration",
    worksTime: "Works time",
    helpfulTime: "Effective time",
    withoutProject: "Without Project",
    year: 'Year',
    month: 'Month',
    yearly: 'Yearly',
    monthly: 'Monthly',
    monthPerformance: 'Month Performance',
    monthTotalTimeGroupByProject: 'Total Month Times Group By Project',
    monthTotalTimeGroupByProject_showBy: 'Show By',
    monthTotalTimeGroupByProject_showBy_profitable: 'Effective Times',
    monthTotalTimeGroupByProject_showBy_total: 'Total Times',
    date: 'Date',
    weekDay: 'Week Day',
    dayActivitiesDetails: 'Day Activities Details',
    messages_userNotRelatedToACompany: "User does not have a company. We can not base our calculations on any calendar",
    messages_userCompanyNotHaveCalendar: "The company of the user has not a default calendar",
    totalDayWorkingTime: "Total Day Working Time",
    arrivalDelay: "Arrival Delay",
    hurryInExit: "Hurry In Exit",
    extraWorkBeforeWorkStart: "Extra Work Before Work Start",
    extraWorkAfterWorkStart: "Extra Work After Work Start",
    userTotalWorkTime: "User Total Work Time",
    userTotalUsefulTime: "User Total Useful Time",
    userTotalWorkInWorkTime: "User Total Work in WorkTime",
    userName: "User Name",
    firstName: "FirstName",
    lastName: "Last Name",
    firstTimesheetResponsible: "First Timesheet Responsible",
    secondTimesheetResponsible: "Second Timesheet Responsible",
    thirdTimesheetResponsible: "Third Timesheet Responsible",
    fourthTimesheetResponsible: "Fourth Timesheet Responsible",
    fifthTimesheetResponsible: "Fifth Timesheet Responsible",
    firstVacationResponsible: "First Vacation Responsible",
    secondVacationResponsible: "Second Vacation Responsible",
    thirdVacationResponsible: "Third Vacation Responsible",
    fourthVacationResponsible: "Fourth Vacation Responsible",
    fifthVacationResponsible: "Fifth Vacation Responsible",
    userIsNotSelectedProperly: "User Is Not Selected Properly",
    firstResponsibleIsRequiredForNextUsers: "First Responsible Is Required For Next Users!",
    secondResponsibleIsRequiredForNextUsers: "Second Responsible Is Required For Next Users!",
    thirdResponsibleIsRequiredForNextUsers: "Third Responsible Is Required For Next Users!",
    fourthResponsibleIsRequiredForNextUsers: "Fourth Responsible Is Required For Next Users!",
    setTimesheetResponsibles: "Set Timesheet Responsibles",
    setVacationResponsibles: "Set Vacation Responsibles",
    vacationTotalDuration: "Vacation Total Duration",
    waitingForConfirmation: "Waiting for Confirmation",
    confirmed: "Confirmed",
    rejected: "Rejected",
    allFieldsAreRequired: "All Fields Are Required!",
    addNewVacation: "Add New Vacation",
    editVacation: "Edit Vacation",
    vacationAddedSuccessfully: 'Vacation Added Successfully.',
    vacationEdittedSuccessfully: 'Vacation Editted Successfully',
    vacationTime: "Vacation Time",
    confirmationStatus: "Confirmation Status",
    confirmationDate: "Confirmation Date",
    confirmRejectWindow: "Confirm or Reject Window",
    vacationWasUpdatedTryAgain: "This vacation was updated. Please try again.",
    totalWaitingForAcceptVacation: "Total Vacation Waiting For Accept",
    totalAcceptedVacation: "Total Accepted Vacation",
    totalRemainedVacation: "Total Remained Vacation",
    remainedVacationOnVacationCreation: "Remained Vacation On Vacation Creation",
    vacationDetail: "Vacation Detail",
    myVacations: "My Vacations",
    vacationRejectCause: "Vacation Reject Cause",
    otherInfo: "Other Info",
    detailsOfThisVacation: "Details Of This Vacation",
    acceptHistory: "Accept History",
    vacationsReport: "Vacations Report",
    vacationType: "Vacation Type",
    vacationTotalTime: "Vacation Total Time",
    vacationCreationTime: "Vacation Creation Time",
    vacationLastModificationTime: "Vacation Last Modification Time",
    responsible: "Responsible",
    inPreposition: "in",
    step: "step",
    by: "by",
    unableToDeleteCheckedLeave: "Unable To Delete Checked Leave",
    illegal: "Illegal",
    startDate: "Start Date",
    endDate: "End Date",
    startHour: "Start Hour",
    endHour: "End Hour",
    type: "Type",
    confirm: "confirm",
    reject: "reject",
    reportType: "Report Type",
};

ResourceStrs = {
    Name: "Name",
    ResourceTypeName: "Type",
    ResourceGroupName: "Group",
    Description: "Type Description",
    StandardRate: "StandardRate",
    NewResource: "New Resource",
    EditResource: "Edit Resource",
    FromDate: "FromDate",
    ToDate: "ToDate",
    OverTimeRate: "OverTimeRate",
    CostPerUse: "Cost Per Use",
    NotCompelete: "field not compelet",
    AddResource: "Add Resource",
    value: "value",
    Time: "Time",
    titleresourceassign: "Titleresourceassign",
    relatedResourcesCount: "Related Resources Count",
    resourceGroupManagement: "Resource Group Management"
};

cartableStrs = {
    Actionable: "Actionable",
    CreatedByMe: "Created By Me",
    Viewable: "Viewable"
};

unitStrs = {
    unitType: "Unit Type",
    unit: "Unit",
    baseCurrencyAmount: "Base Currency Amount",
    name: "Name",
    type: "Type",
    baseUnitMultiplier: "Base Unit Multiplier",
    baseUnitOffset: "Base Unit Offset",
};

generalUnitTypes = [
    { ID: 1, Name: "Length", LocalName: "Length" },
    { ID: 2, Name: "Time", LocalName: "Time" },
    { ID: 3, Name: "Area", LocalName: "Area" },
    { ID: 4, Name: "Volumn", LocalName: "Volumn" },
    { ID: 5, Name: "Currency", LocalName: "Currency" },
    { ID: 6, Name: "Mass", LocalName: "Mass" },
    { ID: 7, Name: "Temperature", LocalName: "Temperature" },
    { ID: 8, Name: "Electric Current", LocalName: "Electric Current" },
    { ID: 9, Name: "Amount of Substance", LocalName: "Amount of Substance" },
    { ID: 10, Name: "Luminous Intensity", LocalName: "Luminous Intensity" },
    { ID: 11, Name: "Count", LocalName: "Count" },
];

generalUnits = [];

generalUnits[1] =
    [
        { ID: 1, Abbriviation: "M", LocalName: "Meter(s)" },
        { ID: 17, Abbriviation: "km", LocalName: "Kilometer(s)" },
    ];
generalUnits[2] =
    [
        { ID: 2, Abbriviation: "sec", LocalName: "Second(s)" },
        { ID: 14, Abbriviation: "min", LocalName: "Minute(s)" },
        { ID: 16, Abbriviation: "hor", LocalName: "Hour(s)" },
        { ID: 18, Abbriviation: "day", LocalName: "Day(s)" },
        { ID: 19, Abbriviation: "week", LocalName: "Week(s)" },
        { ID: 20, Abbriviation: "mo", LocalName: "Month(es)" },
    ];

generalUnits[3] =
    [
        { ID: 4, Abbriviation: "sq m", LocalName: "Squre Meter" },

    ];

generalUnits[4] =
    [
        { ID: 5, Abbriviation: "cu m", LocalName: "Cube Meter" },
        { ID: 26, Abbriviation: "ltr", LocalName: "Liter" },

    ];

generalUnits[5] =
    [
        { ID: 6, Abbriviation: "USD", LocalName: "US Dollars" },
        { ID: 12, Abbriviation: "IRR", LocalName: "Rials" },
        { ID: 13, Abbriviation: "EUR", LocalName: "Euros" },

    ];

generalUnits[6] =
    [
        { ID: 7, Abbriviation: "kg", LocalName: "Kilogram(s)" },
        { ID: 21, Abbriviation: "ton", LocalName: "Ton(s)" },
    ];

generalUnits[7] =
    [
        { ID: 8, Abbriviation: "c", LocalName: "Centigrade(s)" },
    ];
generalUnits[8] =
    [
        { ID: 9, Abbriviation: "amp", LocalName: "Amper(s)" },
    ];
generalUnits[9] =
    [
        { ID: 10, Abbriviation: "mole", LocalName: "Mole(s)" },
    ];
generalUnits[10] =
    [
        { ID: 11, Abbriviation: "cd", LocalName: "Candel(s)" },
    ];
generalUnits[11] =
    [
        { ID: 22, Abbriviation: "num", LocalName: "Number(s)" },
        { ID: 23, Abbriviation: "u", LocalName: "Unit(s)" },
        { ID: 24, Abbriviation: "set", LocalName: "Set(s)" },
        { ID: 25, Abbriviation: "pcs", LocalName: "Pices" },
    ];

CalendarWeekDayNames = ["Sunday", "Monday", "Tuesday", "Wednsday", "Thursday", "Friday", "Saturday"];
CalendarMonthNames = [
    { ID: 1, Abbreviation: "Jan", LocalName: "January" },
    { ID: 2, Abbreviation: "Feb", LocalName: "February" },
    { ID: 3, Abbreviation: "Mar", LocalName: "March" },
    { ID: 4, Abbreviation: "Apr", LocalName: "April" },
    { ID: 5, Abbreviation: "May", LocalName: "May" },
    { ID: 6, Abbreviation: "Jun", LocalName: "June" },
    { ID: 7, Abbreviation: "Jul", LocalName: "July" },
    { ID: 8, Abbreviation: "Aug", LocalName: "August" },
    { ID: 9, Abbreviation: "Sep", LocalName: "September" },
    { ID: 10, Abbreviation: "Oct", LocalName: "October" },
    { ID: 11, Abbreviation: "Nov", LocalName: "November" },
    { ID: 12, Abbreviation: "Dec", LocalName: "December" }
];

generalActive = ["Deactive", "Active"];

calendarManagement = {
    grid_Name: "Name",
    grid_Description: "Description",
    grid_CalenderExceptionsCount: "Exception Days Count",
    ewin_title: "Edit Project Calendar",
    awin_title: "Create New Project Calendar",
    copywin_title: "Create Calendar Copy",
    timePeriodConjunction: "To",

    weekDayGrid_dayOfWeek: "Day of Week",
    weekDayGrid_workingTime: "Working Time",
    weekDayGrid_editWorkingTime: "Edit",

    exceptionsGrid_from: "From",
    exceptionsGrid_to: "To",
    exceptionsGrid_description: "Description",
    aexceptionwin_title: "Add Exception",
    eexceptionwin_title: "Edit Exception",

    outerTimePeriodConjunction: "AND",

    weekDayTimePeriodEditorWindowPrefix: "Edit working times for",
    error_exceptions_periodOverlap: "Exception Editing Error: Working time have overlaps ",
    error_workingtime_periodOverlap: "Exception Editing Error: Working time have overlaps",
    error_exceptions_startIsAfterEnd: "Start Date Can Not Be After End Date",
    error_fromIsNeeded: "Entering Start Date Is Required",
    error_toIsNeeded: "Entering End Date Is Required",

};

tenderManagement =
{
    grid_Title: "Tender Title",
    grid_Status: "Status",
    grid_ProjectName: "Project",
    grid_Permissions: "Permissions",
    ////////////////////////////
    grid_DurationDays: "Project Duration", //'طول مدت پروژه',
    grid_WaterAndPowerByTaskMaster: "Water And Power Cost Pay By Task Master", //'پرداخت هزینه آب و برق بر عهده کارفرما',
    grid_UseTaskMasterEquipment: "Is Possible To Use Client Equipment By Contractor", //'امکان استفاده از ماشین آلات کارفرما توسط پیمانکار',
    grid_MaterialByTaskMaster: "Materials By Task Master", //'تهیه مصالح عمده مصرفی',
    grid_MajorMaterialByTaskMaster: "Major Materials By Task Master", //'تهیه مصالح عمده غیر مصرفی',
    grid_ProjectPlace: "Project Place", //'محل پروژه',
    grid_AccommodationByTaskMaster: "Accommodation By Task Master", //'امکان اسکان پرسنل پیمانکار در پروژه',
    grid_EstimatedAmount: "Estimated Cost", //'مبلغ برآورد اولیه',
    grid_DateAccepted: "Date Accepted", //'تاریخ تایید',
    grid_PotensialVendorsCount: "Number Of Potential Vendors", //'تعداد افراد و شرکتهای واجد شرایط',
    grid_Duration: "Work Duration", //'مت انجام کار',
    grid_DailyDelayPenalty: "Daily Delay Penalty", //'جریمه روزانه تاخیرات غیر مجاز',

    awin_title: "Save Tender Draft", //'ثبت پیشنویس مناقصه',
    ewin_title: "Edit Tender Draft", //'ویرایش پیشنویس مناقصه',

    tenderOfferNo: "NO",
    tenderHoldingDate: "Holding Date",
    tenderExclusiveTerms: "Exclusive Terms",
    tenderRunType: "Run Type",
    tenderScope: "Scope",
    tenderType: "Type",
    envelopeDeliveryDate: "Envelope Delivery Date",
};

priceInqueryRequestManagementStrs =
{
    grid_Title: "Title Of Price Inquiry Request", //'عنوان درخواست استعلام قیمت',
    grid_Status: "Acceptance Worflow Status", //'وضعیت چرخه تایید ', //////
    grid_ProjectName: "Project", //'پروژه',
    grid_Permissions: "Permissions", //'دسترسی ها',

    grid_Description: "Description", //'شرح',
    grid_DateAccepted: "Date Accepted", //'تاریخ تایید',

    awin_title: "Save Price Inquiry Request", //'ثبت درخواست استعلام قیمت', ///////////
    ewin_title: "Edit Price Inquiry Request", //'ویرایش درخواست استعلام قیمت', /////////
};

contractDraftManagementStrs = {

    grid_Title: "Title Of Contract Draft", //'عنوان پیشنویس قرارداد', /////////////
    grid_Status: "Acceptance Workflow Status", //'وضعیت چرخه تایید ', /////////////
    grid_ProjectName: "Project Name", //'پروژه',
    grid_Permissions: "Permissions", //'دسترسی ها',

    grid_EstimatedAmount: "Estimated Cost Amount", //'مبلغ برآورد اولیه',
    grid_PerformanceGuarantee: "Performance Guarantee", //'رقم تضمین تعهدات',
    grid_GuaranteeDuration: "Guarantee Duration", //'دوره تضمین',
    grid_PrePayment: "Prepayment", //'پیش پرداخت',
    grid_SubContractorName: "Contractor Name", //'پیمانکار',
    grid_ContractDuration: "Contract Duration", //'مدت قرارداد',
    grid_DateAccepted: "Date Accepted", //'تاریخ تایید',
    grid_DailyDelayPenalty: "Daily Delay Penalty", //'جریمه روزانه تاخیر',

    awin_title: "Save Draft Of Contract", //'ثبت پیشنویس قرارداد',
    ewin_title: "Edit Draft Of Contract", //'ویرایش پیشنویس قرارداد',
};

w_dfiDelayReasonPieChartStrs = {
    notSet: "[No Delay]",
    settings_displayBy: "Display By",
    settings_displayByCount: "Count",
    settings_displayByPercent: "Percent"
};

workflowStatusStrs = {
    notStarted: "[Not started]"
};

w_invoicesStatusStrs = {
    settings_displayBy: "Display by",
    settings_displayByCount: "count",
    settings_displayByAmount: "amount",
    title_count: "Invoice count by current state",
    title_amount: "Total invoice amounts in states",
};

w_projectStatisticsStrs = {
    remainingDays: "Remaining Days",
    allDataFormItemsCount: "All Work Items",
    delayedNotStartedDataFormItemsCount: "Delayed, Not Started Work Items",
    criticalDataFormItemsCount: "Critical Work Items",
    nextWeekDataFormItemsCount: "Next Week Work Items",
    day: "day",
    activity: "activity",
};

w_scurveStrs = {
    plan: "Planned",
    actual: "Actual",
    commulative: "Commulative",
    planCommulative: "Planned Cumulative",
    actualCommulative: "Actual Cumulative",
    totalWeightedAverage: "Total Weighted Average",
    sCurveOnWBS: "S-Curve on WBS Levels",
    sCurve: "S-Curve",
    comparePlansScurve: "Compare Plans on S-Curve",
};

w_subWbsStatusStrs = {
    plan: "Plan",
    actual: "Actual",
    planEnd: "Plan End",
    planStart: "Plan Start",
    actualStart: "Actual Start",
    actualEnd: "Actual End",
    grid_phaseName: "Phase Name",
    gird_progress: "Progress",
    grid_planActual: "Plan/Actual",
    grid_manHour: "Man-hour",
    sumOfPlanHour: "Total Scheduled",
    showManHour: "Show Man/Hour Data",
};

w_weeklyPlanStrs = {
    thisWeekActivities: "This Week Activities",
    nextWeekActivities: "Next Week Activities",
    thisWeekTitle: "Activities that should be started or ended in this week according to peoject plan.",
    nextWeekTitle: "Activities that should be started or ended in the next week according to peoject plan.",
};

w_projectListStrs = {
    plan: "Plan",
    actual: "Actual",
    planEnd: "Plan End",
    planStart: "Plan Start",
    actualStart: "Actual Start",
    actualEnd: "Actual End",
    grid_title: "Title",
    gird_progress: "Progress",
    grid_planActual: "Plan/Actual",
    grid_cost: "Cost",
    grid_earnedValue: "Earned Value",

    setting_totalBudgetCalculationType: "Plan Value Calculation Type",
    setting_totalBudgetCalculationType_projectTotalPlannedCashOut: "Total Project's Planned Cashflow",
    setting_totalBudgetCalculationType_projectCBSRootBudjet: "Total CBS Zero Level Budgect",

    setting_currentPlanValueCalculationType: "Current Plan Value Calculation Type",
    setting_currentPlanValueCalculationType_plannedCashOut: "Planned Cashout",
    setting_currentPlanValueCalculationType_totalBudgetXActualProgress: "ActualProgress X TotalBudget",

    setting_currentActualCostCalculationType: "Current Actual Cost Calculation Type",
    setting_currentActualCostCalculationType_totalPayements: "Total Payements",
    setting_currentActualCostCalculationType_totalInvoiceFinalAcceptedAmounts: "Total Invoice Final Accepted Amounts",
    setting_currentActualCostCalculationType_totalActualResourceCosts: "Total Actualed Resource Costs"
};

w_projectLocationOnGoogleMapStrs = {
    alert_noLocation: "Project has no valid coordinations",
    projectMainLocation: "Project's main location"
};

w_myProjectsStatusStrs = {
    statusName: "Status Name",
    count: "Count",
    percent: "Percent"
};

w_dfiDelayReasonPieChartStrs = {
    count: "Count",
    percent: "Percent"
};

projectResource = {
    contractNO: "Contract NO",
    taskMaster: "TaskMaster",
    contractor: "Contractor",
    projectNo: "Project No",
    projectName: "Project Name",
    supervisor: "Supervisor",
    projectType: "Project Type",
    contractSubject: "Contract Subject",
    projectExecutionPlace: "Project Execution Place",
    servicesDescription: "Services Description",
    latitude: "Latitude",
    longitude: "Longitude",
    projectStatus: "Project Status",
    projectCity: "Project city",
    projectLocation: "Project Location",
    timeAndWbsInfo: "Time and WBS Informations",
    timeInfo: "Time Informations",
    newProjectPlan: "Add Project Plan",
    editProjectPlan: "Edit Project Plan",
    delProjectPlan: "Delete Project Plan",
    delProjectPlanConfirm: "Are you sure?",
    newProjectWBS: "Add Project WBS",
    editProjectWBS: "Edit Project WBS",
    delProjectWBS: "Delete Project WBS",
    delProjectWBSConfirm: "Are you sure?",
    cannotDeleteProjectWBS: "Because use as default project wbs, cannot delete",
    cannotDeleteProjectPlan: "Because use as default project plan, cannot delete",
    manageProjectPlans: "Manage Plans",
    manageProjectWBSs: "Manage WBSs",
    projectDateType: "Project Date Type",
    projectTimeUnit: "show type of activities time",
    hoursPerDay: "Hours Per Day",
    daysPerWeek: "Days Per Week",
    daysPerMonth: "Days Per Month",
    contractDate: "Contract Date",
    contractEffectDate: "Contract Effect Date",
    planStartDate: "Plan Start Date",
    planEndDate: "Plan End Date",
    actualStartDate: "Actual Start Date",
    actualEndDate: "Actual End Date",
    forecastEndDate: "Forecast End Date",
    defaultProjectWBS: "Default Project WBS",
    defaultProjectPlan: "Default Project Plan",
    allowSelectDateForActualProgress: "Allow Select Date For Actual Progress",
    projectManager: "Project Admin",
    responsibilities: "Responsibilities",
    projectDefaultCurrency: "Project Default Currency Type",
    projectPublicSettings: "Project Public Settings",
    geographicalLocation: "Geographical Location",
    projectContract: "Contract of Project",
    anotherSettings: "Another Settings",
    customFields: "Custom Fields",
    itemlink: "Project''s DataFormItem Progress Plan Procedures", //"تعیین روش های محاسبه پیشرفت برنامه آیتم های پروژه",
    Consultant: "Consultant", //"مشاور",
    DefaultProjectCalendar: "Default Project Calendar", //'تقویم پیش فرض پروژه'
};

workflowDesignerStrs = {
    Title: "Title",
    Description: "Description",
    NoStartState: "Workflow should have a start state.",

    nextStep: "Next Step",
    previousStep: "Previous Step",
    commentStep: "Comment Step",
    staticCommulativeProgressPercent: "Static Commulative Progress Percent",
    userCanEnterCommulativeProgressPercent: "User Can Enter Commulative Progress Percent",
    isInvoiceFinalAcceptanceState: "Is Invoice Final Acceptance State",
    isFinalAcceptanceState: "Is Final Formal Acceptance State",
    isFormalAcceptanceState: "Is Formal Acceptance State",
    invoiceStep_acceptText: "Accept Text",
    invoiceStep_stepText: "Step Text",
    stepText: "Step Text",
    from: "From",
    to: "To",
    commulativePhysicalProgressPercent: "Commulative Physical Progress Percent",
    commulativeFinancialProgressPercent: "Commulative Financial Progress Percent",
    commulativeInvoiceProgressPercent: "Commulative Invoice Progress Percent",
    acceptProgressAutomaticallyInThisStep: "Accept Progress Automatically In This Step",
    stepNameInProgress: "Step Name In Plan Progress Flow",
    allowTransmital: "Allow Transmital",
    allowIDC: "Allow IDC",
    autoReferByRefrenceMatrix: "Auto Refer by Refrence Matrix",
    physicalProgress: "Physical Progress",
    invoiceProgress: "Invoice Progress",
    financialProgress: "Financial Progress",
    physicalProgressPeriodIsNotValid: "Physical Progress Value is Not Valid",
    financialProgressPeriodIsNotValid: "Financial Progress Value is Not Valid",
    invoiceProgressPeriodIsNotValid: "Invoice Progress Value is Not Valid",
    fieldValue: "Field Value",
    fieldValueShouldBeInRange: "Should be betwean 0 to 100",
    fieldValueIsNotValid: "Is Not Valid",
    acceptancePathTitle: "Acceptance Path Title",

    stateSettings: "State Settings",
    statePermissions: "Permissions",
    stateEventSettings: "Events Settings",
    stateAcceptancePaths: "State Acceptance Paths",

    parameterDataType_String: "String",
    parameterDataType_Int: "Integer",
    parameterDataType_Int64: "Big Integer",
    parameterDataType_Double: "Float",
    parameterDataType_Boolean: "True/False",

    parameterManager_winTitle: "Decision Parameters",
    parameterDataType_Type: "Type",
    parameterManager_parameterName: "Parameter Name",
    parameterManager_parameterTitle: "Parameter Title",
    parameterManager_add: "Add New Parameter",
    parameterManager_edit: "Edit Parameter",

    parameterAlreadyAdded: "Parameter Already Added",

    permissionManager_roles: " Existing Project Roles",
    permissionManager_assignedAccesses: "Assigned Actions",
    permissionManager_notAssignedAccesses: "Not Assigned Actions",

    stateEventSetting_title: "Title",
    stateEventSetting_description: "Description",
    stateEventSetting_arguments: "Argument",
    stateEventSetting_groupType: "Group Type",
    stateEventSetting_commentIsRequired: "Comment Is Required",
    stateEventSetting_acceptProgress: "Accept Progress",

    stateTypes_start: "Start",
    stateTypes_branchState: "Branch State",
    stateTypes_multiUserChoiseState: "Multi User choice",
    stateTypes_parallelAcceptanceState: "Parallel Acceptance",
    stateTypes_dynamicParallelAcceptanceState: "Dynamic Parallel Acceptance",
    stateTypes_uTurnState: "UTurnState",
    stateTypes_finish: "Finish",
    stateTypes_comment: "Comment",
    stateTypes_generalDataFormItemState: "General DataFormItem",
    stateTypes_invoiceApprovalState: "Invoice Approval",
    stateTypes_simpleAcceptRejectState: "Simple Acceptance",
    stateTypes_documentApprovalState: "Document Approval",
    stateTypes_procurementItemState: "Procurement Item",

    parallel_minAccept: "Minimum Accepts",
    parallel_minReject: "Minimum Rejects",

    branch_value1NotValid: "According to selected data type, value entered for 'value1' is not valid",
    branch_value2NotValid: "According to selected data type, value entered for 'value2' is not valid",
    branch_parameter: "Parameter",
    branch_operator: "Operator",
    branch_value1: "Value 1",
    branch_value2: "Value 2",

    permissionsAreDisciplineWise: "Permissions Are Based in Discipline",
    acceptancePathDesign_editAcceptancePath: "Edit Acceptance Path",
    acceptancePathDesign_roles: "Roles",
    rolesSelector_notAssignedRoles: "نقشهای تخصیص نیافته",
    rolesSelector_assignedRoles: "نقشهای تخصیص یافته",
    branchStateSettings_condistionState: 'مرحله بر اساس شرط',
    branchStateSettings_exit: 'Exit',
    dynamicParallelAcceptanceStateSettings_accept: 'تایید/مرحله بعد',
    dynamicParallelAcceptanceStateSettings_reject: 'رد/ مرحله قبل',
    dynamicParallelAcceptanceStateSettings_comment: 'کامنت/ تغییرات',

    multiUserChoiseStateSetting_decisionTitle: "Decision Title",
    multiUserChoiseStateSetting_decisionDescription: "Decision Description",
    multiUserChoiseStateSetting_conditionState: "Condition State",

    relatedDataModels: 'Related Data Models',
    assignDataModel: 'Assign',
    rowOrder: 'Order',
    isPredefined: "Is Predefined",
    modelName: 'Model Name',

    noParam: 'No Parameter Added for Decision Making',
    dataModelManagement: 'Manage Data Models',
};

securityStrs = {
    permissionName: "Permission Name",
    roleNames: "Role Names",
    projectRoleDeleteConfirm: "Please ensure that this role is not used in workflows. Are you sure you want to delete?",
    projectRoleNameDuplicated: "Name is duplicated",
    noAccessToThisRab: "Access Denied",
    log_actionName: 'Action Name',
    log_time: 'Time',
    log_browser: 'Browser',
    log_user: 'User',
    log_os: 'OS',
    log_ip: 'IP',
    log_client: 'Client',
    log_userRole: 'User Role',
    log_details: 'Details',
    log_requestAddress: 'Request Address',
    log_responseStatusCode: 'Response Status Code',
    mngUser_username: 'Username',
    mngUser_firstName: 'First Name',
    mngUser_lastName: 'Last Name',
    mngUser_systemRole: 'System role',
    mngUser_isActive: 'Is Active',
    mngUser_companyName: 'Stakeholder',
    mngUser_post: 'Position',
    mngUser_email: 'Email',
    mngUser_emailConfirmed: 'Email Confirmed',
    mngUser_mobileNumber: 'Mobile',
    mngUser_mobileNumberConfirmed: 'Mobile Confirmed',
    mngUser_dateTypeName: 'Date Type',
    mngUser_accessFailedCount: 'Number of Failed Attempts',
    mngUser_previousAccessFailedCount: 'Number of Failed Attempts Before Last Successfull Login',
    mngUser_twoFactorEnabled: 'Two Step Authentication, Enabled',
    mngUser_lockoutEndDate: 'Lockout End Date',
    mngUser_lastLoginTime: 'Last Logged in Time',
    mngUser_lastFailedLoginTime: 'Last Login Failed',
    mngUser_lastLoginIP: 'Last Logged in IP',
    mngUser_sessionsCount: 'Sessions Count',
    mngUser_latestLoginFailed: 'Latest Login Failed',
    mngUser_hasClientCertificate: 'Has Digital Signature',
    mngUser_lockoutEnabled: 'Lockout Enabled',
    mngUser_signature: 'Signature',
    mngUser_newKeyPassCreated: 'Password for new key successfully generated',
    mngUser_newUser: 'New User',
    mngUser_editUser: 'Edit User',
    mngUser_passwordsIsNotSame: 'Selected passwords are not the same',
    mngUser_dateTime: 'Time',
    mngUser_browser: 'Browser',
    mngUser_os: 'OS',
    mngUser_IP: 'IP',
    mngUser_clientName: 'Client System',
    mngUser_userRole: 'User Role',
    mngUser_details: 'Details',
    mngUser_requestContentType: 'Request Content Type',
    mngUser_requestUri: 'Request Uri',
    mngUser_requestMethod: 'Request Method',
    mngUser_requestRouteTemplate: 'Request Route Template',
    mngUser_responseContentType: 'Response Content Type',
    mngUser_responseStatusCode: 'Response Status Code',
    mngUser_responseTimestamp: 'Response Time',
    mngUser_UserHasDigitalSignatureMessage: 'The selected user has already have a digital signature. In case of generating new digital signature, the previouse one would be invalid.',
    mngUser_usernameDuplicate: 'Username is Duplicate',
    mngUser_usernameDuplicateAndDeleted: 'Chosen Username is already used and deleted.You can use it again for integrity purpose',
    mngUser_usernameCharactedShouldNumAndEnChar: 'You can only use English standard characters and numbers for UserName',
    mngUser_passwordMustHasNum: 'Password must contain at least a number',
    mngUser_passwordMustHasNonNumChar: 'Password must contain at least one non-number and non-text character',
    mngUser_passwordMustHasLowerCaseChar: 'Password must contain at least on Lower-case character',
    mngUser_passwordMustHasUpperCaseChar: 'Password must contain at least on Upper-case character',
    mngUser_passwordAndUsernameShouldNotSame: 'Username and Password cannot be same',
    mngUser_passwordIsSameWithBefore: 'Password is same as previous one',
    mngUser_emailIsEmptyOrDuplicate: 'Email is not entered or is duplicate',
    mngUser_mobileIsEmptyOrDuplicate: 'Cell phone number is not entered or is duplicate',
    mngUser_passwordCharNumber: 'Password must have at least {0} character',
    mngUser_passwordIncorrect: 'Incorrect Password',



};


authenticationSettingsStrs = {
    cantSetUniqueEmail: "It’s not possible to set value ‘email cannot be duplicate’. There are Users without email or duplicated emails in System",
    loginDataValidTimeNotValid: "Value of 'Validation Time' is not valid. Must be greater than 0",
    minUserLockedMinutesNotValid: "Value of 'User Lockout Time' is not valid. Must be at least 1 minute",
    minFailedLogginBeforLogNotValid: "Value of 'Number of Unsuccessful Attempts to Lockout' is not valid. Must be at least 2",
    minLengthNotValid: "Value of ‘Minimum Length’ is not valid. Must be at least 3",
    passwordValidTimeNotValid: "Value of ‘Password Expiration Time’ is not valid. Minimum Value is 0",
    minFailedLoginBeforeEntringTwoFactorNotValid: "Value of ‘Number of Unsuccessful Attempts to Enter Two-Factor Authentication’ is not valid. Minimum Value is 2",
    lockoutAndTwoFactorDoesNotMeetWithEachOther: "If both User Lock-out and Two-Factor Authentication are active at the same time, the value of latter must less that former.",
    uniqueMobileNumberCanNotBeSet: "It’s not possible to set value ‘Cell Phone Number cannot be Duplicate’. There are Users without Cell Phone or duplicated Cell Phone Numbers in System",
    loggedInUsersDataCheckTimeFrame: "Value of ‘Logged-in Users Period Validation Check’ is not valid. Minimum Value is 0",
}

ProjectPlanning = {
    WBS: "WBS",
    defaultWBS: "Default WBS",
    defaultProjectPlan: "Default Project Plan",

    planprogresscalculationheadtab: "Plan Progress Calculation",
    weightingheadtab: "Weighting",
    schedulingheadtab: "Scheduling",

    planprogressrecalculationMessage: "Click to update plan progress for selected Schedule.",
    PlanProgressCalculationButtonCaption: "Recalculate Plan Progress",

    Timeconsumingprocess: "Note that the process will occupy server resources.",

    weightingBottomUp: "Bottom-Up Weighting",
    weightingBottomUpMessage: "Choosing this option will set weight of WBS tasks equal to sum of children items.",
    weightingBottomUpButtonCaption: "Bottom-Up Weight",
    weightingTopBottomButtonCaption: "Top-Bottom Weight",

    weightingTopDown: "Top-Down Weighting",
    weightingTopDownTitle: "Top-Down weighting should be performed manually.",

    weightingTopDownMessage: "Choosing this option will set weight of children items equal to weight of WBS, divided by children items count.",

    schedulingTopDown: "Top-Down Scheduling",
    schedulingTopDownMessage: "This option will set planned start and finish start dates from parent WBS tasks",
    schedulingTopDownButtonCaption: "Top-Down Schedule",

    actionResultpreMessage: "Calculation has Performed Successfully for {0} Item(s)",//محاسبه برنامه زمانی برای تعداد
    ProcessDuration: "Process Duration",
    TotalWeightMessage: "Total weight of work items of {0} project is {1}",
    TotalWeightAlertMessage: "If you don't correct weights, progress reports would be incorect."
};

ProjectContractCashflow = {
    ProjectTitle: "Project Name",
    Actual: "Actual",
    Program: "Program",
    Total: "Sum",
    ContractType: "Contract Types",
    ContractBudgetChart: "Contract Budget Chart",
    FillCacheContractCashflowCaption: "Fill Cache Contract Cashflow",
    ShowResultCaption: "Show Report Result",
    Year: "Year"
};

ContractCashflow = {
    ContractTitle: "Contract Title",
    Actual: "Actual",
    Program: "Program",
    Total: "Sum",
    ContractType: "Contract Types",
    ContractBudgetChart: "Contract Budget Chart",
    ShowResultCaption: "Show Report Result",
    Year: "Year"
};

galleryStrs = {
    manageAlbums: "Manage Albumes",

    aewin_title: "Title",
    aewin_album: "Album",
    aewin_description: "Description",
    aewin_showInDashboard: "Show in dashboard",
    aewin_photo: "Photo",
    aewin_photoFile: "Photo File",

    albumManagerWinTitle: "Manage Albums",
    addNewPhotoWindowTitle: "Add New Photo",
    editPhotoWindowTitle: "Edit Photo",
    pleaseUploadAPhotoFile: "Please Upload A Photo File.",
    pictureSelect: "Select A Picture",
    grid_originalFileName: "Original File Name",
    grid_imageContentType: "Image Type",
    preview: "Preview",

    albume_title: "Title",
    album_description: "Description",
    album_addWinTitle: "Add new album",
    album_editWinTitle: "Edit album",
};
userSettingStrs = {
    pleaseSelectOneFile: "Please Select One File",
    editWinTitle: 'Edit Profile'
};

///////////////////////////////////////////////////////    Engineering Module Starts /////////////////////////////////////////
engineeringStrs = {
    taskMasterDocumentNumber: "TaskMaster Document Number",
    contractorDocumentNumber: "Contractor DocumentNumber",
    reservedDocumentNumber: "Reserved DocumentNumber",
    supervisorDocumentNumber: "Supervisor DocumentNumber",
    documentClass: "Document Class",
    documentType: "Document Type",
    isReferableInCurrentState: "Is Referable In Current State",
    isTransmitableInCurrentState: "Is Transmitable In Current State",
    someAreNonTransmitableMessage: "From {0}; selected documents, {1}; document(s) is not transmitable in current state",
    documentNo: "Document No",
    totalPlannedManHour: "Planned Man Hour",
    //aeIDC
    ReferenceReturn: "Return",
    ReferenceReceiver: "Receiver",
    ReferenceEdit: "Edit",
    ReferenceText: "Comments",
    ReferenceNew: "New",
    documentReferType: "Refer Type",
    TheDeadlineForReplies: "Deadline",
    AbilityToDereference: "Can Refer Again",

    RefererDiscipline: "From Discipline",
    ReferedToDiscipline: "To Discipline",


    //aeTransmital
    DocumentNoEmployer: "Taskmaster Doc No",
    DocumentNoContractor: "Contractor Doc No",
    ViewItem: "Transmital Details",
    EngineeringDocumentsAttached: "Attached Eng Docs",
    PublicDocumentsAttached: "Attached General Docs",
    TransmitalNumber: "Transmital Number",
    TransmitalDate: "Transmital Date",
    TransmitalFrom: "From",
    TransmitalTo: "To",
    ReferenceCode: "Refrence Code",
    Transmital_Subject: "Title",
    Transmital_Remarks: "Remarks",
    DocumentsAttachedAutomaticallyGoNextStep: "Documents Attached Automatically Go Next Step", //'مدارک الصاق شده به صورت خودکار به مرحله بعد بروند',
    DocumentAttach: "Attached Documents", //'مدارک الصاق شده به مرسوله ',
    DocumentAttachable: "Attachable Documents", //'مدارک قابل الصاق به مرسوله',
    DocumentAttachCount: "Number Of Attached Documents", //'تعداد مدارک الصاق شده',
    RecieveDate: "Recieved Date", //'تاریخ دریافت',
    DocumentCount: "Number Of Documents", //'تعداد مدرک', //////////
    AttachedDocumentStatus: "Attached Document Status", //'وضعیت مدرک هنگام الصاق', ///////
    DocumentVersionAttached: "Attached Document Version", //'نسخه مدرک هنگام الصاق',
    ClientDocumentNumber: "Task Master Document Number", //'شماره سند کارفرما',
    DocumentNumberContractor: "Contractor Document Number", //'شماره سند پیمانکار',
    DocumentLevel: "Document Level",
    EditNumber: "Revision Number",
    TransmitalDocs_Grid_Position: "Row Number",
    TransmitalDocs_Grid_From: "From",
    TransmitalDocs_Grid_To: "To",
    TransmitalDocs_Grid_TimeRevNo: "Revision Number",
    TransmitalDocs_Grid_TimeState: "Status",
    TransmitalDocs_Grid_PageCount: "Page Count",
    TransmitalDocs_Grid_DisciplineName: "Discipline",
    TransmitalDocs_Grid_ActivityCode: "Activity Code",
    TransmitalDocs_Grid_WBSCode: "WBS Code",
    TransmitalDocs_Grid_WorkflowModelName: "Workflow",
    TransmitalDocs_Grid_CurrentState: "Current Status",
    TransmitalDocs_Grid_ExtensionName: "DFI Type",
    TransmitalDocs_Grid_ProjectDataFormItemProgressPlanName: "Plan Progress",
    TransmitalDocs_Grid_PlanStartDate: "Plan Start",
    TransmitalDocs_Grid_PlanEndDate: "Plan Finish",
    TransmitalDocs_Grid_ForecastEndDate: "Forecast Finish",
    TransmitalDocs_Grid_ActualStartDate: "Actual Strat",
    TransmitalDocs_Grid_ActualEndDate: "Actual Finish",
    TransmitalDocs_Grid_WeightOfWhole: " Weight to Whole",
    TransmitalDocs_Grid_WeightOfActivity: "Weight to Activity",
    TransmitalDocs_Grid_PlanManHour: "Plan ManHour",
    transmiltal_TransmitalDateIsMandatory: "Transmital Date Is Required",
    TransmitalDocs_Grid_DateType: 'Date Type',

    SheetCount: "Sheet Count",
    DocumentSize: "Document Size",
    SupervisorDocNum: "Supervisor Doc No",
    DocumentType: "Document Type",
    ReservedDocNumber: "Reserved Doc No",
    DocumentClass: "Document Class",
    DocumentTitle: "Document Title",

    newRefer: "New Refer",
    editRefer: "Edit Refer",
    removeRefer: "Remove Refer",
    reRefer: "Rerefer",
    postbackRefer: "Postback",
    referedDocumentDetails: "Document Details When It Was Refered",
    ViewReferenceItem: "Details",
    Reference: "IDCs",
    Items: "Transmitals",
    Comments: "Comments",
    DueDate: "Due Date",
    ReRefered: "Re Refer",
    RefererLastName: "Referrer",
    ReferedToLastName: "Refered to",
    DescriptionReferenceTime: "Refer Time Comments",
    PlanStartDate: "Refer Date",
    DeadlineFoReturn: "Due Date",
    TimeBack: "Return Time",
    DescriptionTimeBack: "Reject Time Comments", //'توضیحات زمان بازگشت',
    TextCommentReferral: "Referral Comment", //'متن  کامنت ارجاع دهنده',
    PostBackComments: "Reply", //'جوابیه بازگشت',
    EditItem: "Edit Transmital",
    NewItem: "New Transmital",
    NewTransmital: "New Transmital",
    Project: "Project",


    //IDCs DocumentRefers
    waitingTime: "Wating Time",
    engiHumanResourceReport: "Engineering Human Resource Report",
    waitingTimeByDisciplinTitle: "Waiting time per discipline ",
    waitingTimeByUserTitle: "Waiting time per human resource",

    idcMatrix: "IDC Matrix",
};

w_engStepsDurationDenotativeStrs = {
    titlePrefix: "Average Step Time by:", //'آمار تفکیکی میانگین زمان صرف شده در هر استپ بر اساس',
    documentClass: "Document Class", //'کلاس مدرک',
    documentType: "Document Type", //'نوع مدرک',
    documentDiscipline: "Document Disipline", //'دیسیپلین مدرک',
    settings_displayBy: "Display BY", //'نمایش بر اساس',
};
w_engDataformItemsStateCountDenotativeStrs = {
    titlePrefix: "Average Step Time by:", //'آمار تفکیکی تعداد مدارک مهندسی در وضعیتهای مختلف بر اساس',
    documentClass: "Document Class", //'کلاس مدرک',
    documentType: "Document Type", //'نوع مدرک',
    documentDiscipline: "Document Discipline", //'دیسیپلین مدرک',
    settings_displayBy: "Display By", //'نمایش بر اساس',
};
w_engDataformItemsStateCount = {
    title_count: "Count", //'تعداد',
};
///////////////////////////////////////////////////////    Engineering Module Ends /////////////////////////////////////////
procurementStrs =
{
    YouHaveNoPermissionToViewThisSection: "You Have No Permission To View This Section",
    ProcurementType: "Procurement Type",
    MRNumber: "MR Number",
    MaterialType: "Material Type",
    MRMTOIssueDate: "MR/MTO Issue Date",
    MRMTOPublishDate: "MR/MTO Publish Date",
    TotalQuantity: "Total Quantity",
    QuantityUnit: "Quantity Unit",
    Materials: "Materials",
    VendorSelection: "Vendor Selection",
    Milestons: "Milestons",
    Vendors: "Vendors",
    VendorSelectionFinalization: "Vendor Selection Finalization",
    VendorSelectionMethod: "Vendor Selection Method",
    InqueryRFQDate: "Inquery/RFQ Date",
    TechnicalClarificationDate: "Technical Clarification Date",
    InqueryBIDCloseDate: "Inquery/BID Close Date",
    TBEIssueDate: "TBE Issue Date",
    TBEApproveDate: "TBE Approve Date",
    CBEIssueDate: "CBE Issue Date",
    CBEApproveDate: "CBE Approve Date",
    FinalVendorSelectionMR1Date: "Final Vendor Selection (MR1) Date",
    KOMDate: "KOM Date",
    MRRevisionsPO: "MR Revisions & PO",
    POPlacementDate: "PO Placement Date",
    POEffectDate: "PO Effect Date",
    PONumber: "PO Number",
    ManufacturingStartDate: "Manufacturing Start Date",
    ManufacturingFinishDate: "Manufacturing Finish Date",
    FirstInspectionDate: "First Inspection Date",
    InspectorThirdParty: "Inspector(Third Party)",
    FinalInspectionDate: "Final Inspection Date",
    Manufacturing: "Manufacturing",
    Inspection: "Inspection",
    Insurance: "Insurance",
    TransporFinalization: "Transport & Finalization",
    Financial: "Financial",
    ShipmentTerm: "Shipment Term",
    BLNumber: "BL Number",
    BillofLoadningDate: "Bill of Loadning Date",
    DepartureDate: "Departure Date",
    CustomsArrivalDate: "Customs Arrival Date",
    CustomsClearanceDate: "Customs Clearance Date",
    SiteTransportatinDate: "Site Transportatin Date",
    ROSDate: "ROS Date",
    FinalDocumentationDate: "Final Documentation Date",
    TotalAmountPlan: "Total Amount Plan",
    TotalAmountPlanCurrencyID: "Total Amount Plan Currency ID",
    TotalAmountActual: "Total Amount Actual",
    TotalAmountPlanActualID: "Total Amount Plan Actual ID",
    Guaranties: "Guaranties",
    Payment: "Payment",
    LCs: "LCs",
    //Material Specification Plan
    MaterialSpecification: "Material Specifications",
    MaterialSpecificationPlan: "Planned Material Specifications",
    MaterialSpecificationActual: "Actual Material Specifications",
    MaterialTypeCode: "MaterialTypeCode",
    Description: "Description",
    Quantity: "Quantity",
    QuantityUnitAbbriviation: "Quantity Unit Abbriviation",
    UnitPrice: "Unit Price",
    PriceUnitAbbriviation: "Price Unit Abbriviation",
    UnitWeight: "Unit Weight",
    DimWidth: "DimWidth",
    DimensionUnitAbbriviation: "Dimension Unit Abbriviation",
    DimLength: "DimLength",
    ID: "ID",
    Width: "Width",
    Length: "Length",
    Height: "Height",
    RefrencePlanItem: "Refrence PlanItem",
    ////Vendor
    FinalSelectedVendor: "Final Selected Vendor (MR (1))",
    VendorName: "Vendor Name",
    Origin: "Origin",
    RFQActualDate: "RFQ Actual Date",
    RFQResponseDate: "RFQ Response Date",
    TechnicalStatus: "Technical Status",
    DeliveryDuration: "Delivery Duration",
    TotalQuotedPrice: "Total Quoted Price",
    RFQResponseStatus: "RFQ Response Status",
    AddNewVendor: "Add New Potensial Vendor",
    EditVendor: "Edit Potensial Vendor",
    ///Revision
    MRRevisions: "MR Revisions",
    RevisionNumber: "RevisionNumber",
    RevisionDate: "Revision Date",
    ///Manufacturing        
    ManufacturingDuration: "Manufacturing Duration Estimation",
    ManufacturingProgressAssessmentPeriods: "Manufacturing Progress Assessment Periods",
    ManufacturingAddWindow: "Add new progress assessment",
    ManufacturingEditWindow: "Edit progress assessment",
    Progress: "Progress",
    ProgressDate: "Progress Assessment Date",
    ProgressPercent: "Progress Percent",
    //Inspection 
    InspectionDetails: "Inspection Details",

    InspectionStakeholderName: "Stakeholder",
    InspectionTitle: "Title",
    InspectionDate: "Date",
    InspectionDateA: "InspectionDateA",
    InspectionDateP: "InspectionDateP",
    InspectionDateF: "InspectionDateF",
    InspectionStatus: "Status",
    EditInspection: "Edit Inspection",
    AddInspection: "Add Inspection",
    //Insurance
    InsuranceStakeholder: "Stakeholder",
    InsuranceName: "Name",
    InsuranceDate: "Date",
    InsuranceAmount: "Amount",
    InsuranceCurrencyUnit: "CurrencyUnit",
    InsuranceRate: "Rate",
    InsuranceValidAfterArrival: "ValidAfterArrival",
    InsuranceValidAfterArrivalDuration: "ValidAfterArrivalDuration",
    AddNewInsurance: "Add New Insurance",
    EditInsurance: "Add New Insurance",
    //Financial&Guranty
    FinancialGurantyCurrencyName: "CurrencyName",
    FinancialGurantyAmount: "Amount",
    FinancialGurantyDate: "GurantyDate",
    FinancialGurantyExpiretionDate: "ExpiretionDate",
    FinancialGurantyTypeID: "FinancialGurantyTypeID",
    FinancialGurantyCurrencyUnitID: "CurrencyUnitID",
    FinancialGurantyRefrenceNumber: "RefrenceNumber",
    FinancialGurantyNotes: "Notes",
    FinancialLCNumber: "FinancialLCNumber",
    FinancialLCIssueDate: "IssueDate",
    FinancialLCCurrencyName: "CurrencyName",
    FinancialLCAmount: "Amount",
    FinancialLCExpirationDate: "ExpirationDate",
    FinancialLCConditions: "Conditions",
    FinancialLCNotes: "Notes",
    AddNewLC: "Add New LC",
    EditLC: "Edit LC",
    AddNewFinancialGuranty: "Add New Financial Guranty",
    EditFinancialGuranty: "Edit Financial Guranty",

    ///window caption
    MaterialSpecificationPlanNewItem: "Material Specification Plan NewItem",
    MaterialSpecificationPlanEditItem: "Material Specification Plan EditItem",
    AddNewRevision: "Add New Revision",
    RevisionDetails: "Revision Details",

    InsuranceDetails: "Insurance Details",

    totalPlannedManHour: "Planned Man Hour",
    PlanAbbr: " P",
    ActualAbbr: " A",
    ForecastAbbr: " F",
    DocumentTitle: "Procurement item title",
};
////////////////////////////////////////////////////// Periodic Report Starts ///////////////////////////////////////////////////////
periodicReportSettings = {
    SelfDeclareReasonMandatoryMarginPercent: "Self Declare Reason Mandatory Margin Percent",
    MaximumAttachedFilesCount: "Maximum Attached Files Count",
    MaxFileSize: "Max File Size",
    kilobyte: "Kilobyte",
    AllowedFileExtensions: "Acceptable File Formats (Comma Separated, * Blank Means To Accept All Formats)",
    ContractPeriodicReportSettingEnable: "Contract Periodic Report Setting Enable",
    DocumentAttachmentIsMandatoryForSelfDeclare: "Document Attachment Is Mandatory For Self Declare",
    CanSelfDeclareMoreThanPlan: "Can Self Declare More Than Plan",
    AssessVarianceDropDownVisible: "Assess Variance DropDown Visible",
    AssessVarianceCommentBoxVisible: "Assess Variance Comment Box Visible",
    CanFinalizeSelfDeclareBeforeSelfDeclarePeriodFinished: "Can Finalize Self Declare Before Self Declare Period Finished"
};

acceptReportStrs = {
    contractProgressSaveIDIsRequired: "پارامتر contractProgressSaveID الزامیست",
    notDoneYet: 'هنوز صورت نگرفته است',
    pleaseEnterAssessedProgress: 'لطفا پیشرفت ارزیابی را وارد کنید',
    canNotAssessMoreThanSelfDeclare: 'امکان ثبت ارزیابی بیش از خوداظهاری وجود ندارد',
    canNotSelfDeclareLessThanBefore: 'برای این قلم کاری در در مراحل قبلی ارزیابی پیشرفت {0} ثبت شده است. امکان ثبت پیشرفت بیشتر از این مقدار وجود ندارد ',
    shouldAddReason: 'ورود علت انحراف ارزیابی برای ارزیابی با میزان انحراف بیش از {0} درصد نسبت به خود اظهاری الزامی است',
    shouldReadAllDocumentsBeforeSaving: 'قبل از ثبت ارزیابی مشاهده کلیه مستندات ارسال شده الزامی است',
    errorInSavingProgress: 'مشکلی در ثبت اطلاعات پیشرفت ',
    progressSuccessfullySaved: "پیشرفت با موفقیت ثبت شد",
    toBeInformedAboutNotCheckedItems: "تعداد {0} قلم کاری را هنوز بررسی نکرده اید. در صورت تایید نهایی مقدار پیشرفت این گونه ها برابر با میزان ثبت شده در مرحله {1} در نظر گرفته میشوند  ",
    canNotAcceptAllBeforeAcceptingAll: " تعداد {0} قلم کاری را هنوز بررسی نکرده اید. امکان تایید در این مرحله تنها پس از بررسی کامل وجود خواهد داشت  ",
    acceptReportWPTabs_notAssessedSelfDeclares: "اقلام کاری خود اظهاری شده ارزیابی نشده",
    acceptReportWPTabs_allSelfDeclaredItems: " تمامی اقلام کاری خود اظهاری شده",
    acceptReportWPTabs_activitiesTree: "شکست کار فعالیتها",
    acceptReportWPTabs_notAssessedSelfDeclaresDesc: "لیست تمامی اقلام کاری ثبت خود اظهاری شده که هنوز بوسیله شما ارزیابی نشده اند",
    acceptReportWPTabs_allSelfDeclaredItemsDesc: "لیست تمامی اقلام کاری ثبت خود اظهاری شده",
    acceptReportWPTabs_activitiesTreeDesc: "شکست کار کلیه اقلام کاری خود اظهاری شده",
    nextStepDefaultTitle: 'تایید نهایی و ثبت پیشرفت واقعی',
    nextStepChangedTitle: 'تایید و ارسال برای {0} ',
    level0Title: "خوداظهاری",
};

contractPeriodicReportAcceptanceCartableStrs = {
    grid_ProjectName: "پروژه",
    grid_ContractNumber: "شماره قرارداد",
    grid_Title: "عنوان قرارداد",
    grid_ContractTypeName: "نوع قرارداد",
    grid_ProgressReportsPeriodsTypeTitle: "دوره ورود اطلاعات",
    grid_LatestProgressPeriodCheckDate: "دوره جاری (منتهی به)",
    grid_LatestSavedProgressPeriodCheckDate: "آخرین دوره ذخیره شده (منتهی به)",
    grid_WaitingForAcceptance: "تعداد گزارشات منتظر تایید",
    progressPeriodCheckDateTitle: "دوره (منتهی به)",
    acceptorUser: "کاربر تایید کننده",
    acceptTime: "تاریخ تایید",
};

contractPeriodicReportDFIListStrs = {
    pleaseEnterProgress: 'لطفا پیشرفت را وارد کنید',
    confirmSelfDeclareMoreThanPlan: 'پیشرفت ثبت شده از میزان برنامه بالاتر است. آیا مطمئن هستید که میخواهید این پیشرفت ثبت شود؟',
    canNotSelfDeclareMoreThanPlan: 'امکان ثبت پیشرفتی بیش از درصد پیشرفت برنامه وجود ندارد',
    canNotSelfDeclareMoreThan100: 'امکان ثبت پیشرفتی بیش از صد وجود ندارد',
    canNotSelfDeclareLessThanLatestActualAcceptedProgress: 'امکان ثبت پیشرفتی کمتر از میزان پیشرفت واقعی تایید شده وجود ندارد',
    canNotSelfDeclareLessThanLatestDeclaredProgress: 'امکان ثبت پیشرفتی کمتر از آخرین میزان پیشرفت وارد شده وجود ندارد',
    shouldEnterDelayReason: 'ورود علت تاخیر برای فعالیتهایی با میزان تاخیر بیش از {0} درصد نسبت به پیش بینی الزامی است',
    canNotSelfDeclareWithNoAttachedDocuments: 'هیچ مستندی الصاق نشده است. بدون الصاق مستندات امکان ثبت پیشرفت وجود ندارد.',
    progressSuccessfullySaved: "پیشرفت با موفقیت ثبت شد",
    errorInSavingProgress: 'مشکلی در ثبت اطلاعات پیشرفت ',
    grid_LatestSelfDeclaredProgressPeriodCheckDate: "آخرین دوره اظهار شده",
    grid_LatestDeclaredProgress: "آخرین پیشرفت اظهار شده",
    grid_SelfDeclare: 'ثبت اطلاعات پیشرفت',
    editProgress: "ویرایش پیشرفت",
    saveProgress: "ثبت پیشرفت",
    gettingRootPath: "دریافت مسیر ریشه",
    progressWinTitle: "ثبت پیشرفت",
    progressHistory: "تاریخچه پیشرفت",
    dfiProgressIsAlready100: 'پیشرفت این قلم کاری پیش از این کامل شده است',
    noActiveProgressPeriod: "دوره پایش فعالی وجود ندارد"
};

contractPeriodicReportDFIListForAcceptanceStrs = {
    pleaseEnterProgress: 'لطفا پیشرفت را وارد کنید',
    canNotSelfDeclareMoreThanPlan: 'امکان ثبت پیشرفتی بیش از درصد پیشرفت برنامه وجود ندارد',
    canNotSelfDeclareMoreThan100: 'امکان ثبت پیشرفتی بیش از صد وجود ندارد',
    canNotSelfDeclareLessThanLatestActualAcceptedProgress: 'امکان ثبت پیشرفتی کمتر از میزان پیشرفت واقعی تایید شده وجود ندارد',
    shouldEnterDelayReason: 'ورود علت تاخیر برای فعالیتهایی با میزان تاخیر بیش از {0} درصد نسبت به پیش بینی الزامی است',
    canNotSelfDeclareWithNoAttachedDocuments: 'هیچ مستندی الصاق نشده است. بدون الصاق مستندات امکان ثبت پیشرفت وجود ندارد.',
    progressSuccessfullySaved: "پیشرفت با موفقیت ثبت شد",
    errorInSavingProgress: 'مشکلی در ثبت اطلاعات پیشرفت ',
    grid_SelfDeclare: 'خود اظهاری',
    progressWinTitle: "ثبت پیشرفت",
    notAssessed: 'بررسی نشده',
    assess: "بررسی",
};

dataFormItemHistoryStrs = {
    gettingContractPeriodicReportSettingError: "اشکال در دریافت تنظیمات ",
    grid_ProgressPeriodCheckDate: 'برای دوره منتهی به',
};

progressReportsPeriodsManagementStrs = {
    grid_Title: 'Title',
    grid_ProgressPeriodCheckDate: 'For Period Endig to',
    grid_DataGatheringStartDate: "Data Gathering Start Date",
    grid_DataGatheringEndDate: "Data Gathering End Date",
    noRows: "No Record Found",
    editWindowTite: "Edit",
    dataEntryPeriod: "Data Gathering Type",
    datePeriod: "Date Period",
    datePeriodType_active: "Active",
    datePeriodType_all: "All",
};

userContractListStrs = {
    grid_LatestProgressPeriodCheckDate: "دوره جاری (منتهی به)",
    grid_LatestSavedProgressPeriodCheckDate: "آخرین دوره ذخیره شده (منتهی به)",
    grid_SaveProgress: "ثبت اطلاعات پیشرفت",
};


reportCartableStrs = {
    hotDFIs: "Hot DFIs",
    hotDFIs_description: "DFIs in reporting time but not self declared",
    allDFIs: "All DFIs",
    allDFIs_description: "All DFIs related to you",
    wbs: "WBS",
    wbs_description: "All DFIs related to you by WBS",
    finalizeAlert: "You can not edit self declare after finalizing. Are you sure?",
    selfDeclareFilalized: 'Self declare finalized.'
};

////////////////////////////////////////////////////// Peridic Report Ends ///////////////////////////////////////////////////////

projectPlanCompareStrs = {
    planTitle: "Planned Title",
    chartPlanColor: "Planned Chart Color",
    chartActualColor: "Actual Chart Color",
};

resourceUsageReportsStrs = {
    usageDate: "Date of Usage",
    relatedDFIRootPath: "DataformItem Parent Path",
    usageAmount: "Usage Amount",
    usageComments: 'Usage Comments',
    creationTime: 'Date of Register',
};

resourceUsageHistogramReportsStrs = {
    chartTitle: 'Resource Usage',
    weekly: 'Weekly',
    monthly: 'Monthly',
    pleaseSelectAResource: 'Please Select a Resource',
    fromCheckDate: 'Start From',
    toCheckDate: 'End To',
    usageAmount: "Usage Amount",
    startDateShouldBeAfterEndDate: "Start Date Should Be After End Date",
};

workResourceAssignStrs = {
    chartTitle: 'Work Resource Assignment Chart',
    startDateShouldBeAfterEndDate: "Start Date Should Be After End Date",
};

resourceAssignStrs = {
    chartTitle: 'Resource Assignment Chart',
};

projectsStr = {
    filters: "filters",
    gridColumns: {
        ProjectNO: "Project Code",
        Name: "Title",
        ContractNO: "Contract Number",
        ContractDate: "Contract Date",
        ProgramTitle: "Program Title",
        DefaultLocationTitle: "Default Location",
        ProjectStatusName: "ProjectS tatus",
        ProjectTypeName: "ProjectType",
        Currency: "Default Currency",
        ProjectDateType: "Calendar Type",
        PMFullName: "Project Admin",
        PMUserName: "Project Admin's UserName",
    }
};

wbsPatternsStr = {
    Name: "Name",
    Code: "Code",
    DefaultDuration: "DefaultDuration",
    Weight: "Weight",
    Unit: "Unit",
};


projectVolumesStrs = {
    titleCaptionGrid: "Title",
    volumn: "Volumn",
    unit: "Unit"
};
advancedToolsStrs = {
    transferDataFromProject: "Transfer Data From Project",
    roles: "Roles",
    projects: "Projects",
    project: "Project",
    rolesAccesses: "Roles Accesses",
    addedAccess: " {0} Access(es) Added",
    noAccessAdded: "No Access Added",
    notFoundRoles: "Not Found Roles:",
    done: "Done",
    copyFinished: "Copy Finished",
    addedWorkflows: "{0} Workflow(s) Copied",
    addedWBS: "{0} WBS Containing {1} Rows Copied",
    defaultProjectWBSSet: "Project Default WBS Set",
    defaultProjectPlanSet: "Project Default Plan Set",
    addedIssueCategory: "{0} Issue Categories Added",
    addedPlan: "{0} Plan Containing {1} Rows Copied",

    issueCategoryIsDuplicated: "Category: '{0}' Is Duplicated ",
    noIssueCategoryAdded: "No Issue Category Added",
    dataFormItemPlanProgressCalculationMethod: "DataFormItem Plan Progress Calculation Methods",

    workflows: "Workflows",
    wBSs: "WBSs",
    issueCategories: "Issue Categories",
    riskCategories: "Risk Categories",

    dfiWorkflowsResettedAlert: "DFI Workflows Reset Finished",
    dfiWorkflowsResetted: "{0} Workflows Resetted",
    noItemFoundToReset: "No Started Workflow Found To Resset",

    dfiWorkflowsStartedAlert: "DFI Workflow Start Finished",
    dfiWorkflowsStarted: "{0} Workflows Started",
    noItemFoundToStart: "No DFI Found To Start Workflow",

    userAddedToRole: "User '{0}' Added To Role '{1}'",
    noUserAddedToRoles: "All users have already been added ,or this role is not in the projects.",

    addedDFIs: "{0} DFIs Added",
    workflowIsNotDefinedInThisProject: "Workflow '{0}' Is Not Defined In This Project",
    dfippcmIsNotDefinedInThisProject: "DataFormItem Plan Progress Calculation Method '{0}' Is Not Defined In This Project",
    thisProjectNotHaveDefaultPlan: "This Project Does Not Have Default Plan.",
}
projectDisciplinesStrs = {
    newWindowTitle: 'New Project Discipline',
    editWindowTitle: 'Edit Project Discipline',
    canNotRemoveBecauseOFRelatedDFIs: "Because of related DataformItems, removing is not possible",
    canNotRemoveBecauseOFRelatedPayments: "Because of related payments, removing is not possible",
    grid_Name: "Name",
    grid_Description: "Description",
    grid_ManagerUserLastName: "Discipline Manager",
    grid_RelatedDataformItemsCount: "Related DataformItems Count",

};
////////////////////////////////////////////////////// Message Dispatch Module Starts /////////////////////////////////////////
sessionManagementStrs = {
    attachedDocuments: 'Attached Documents',
    proceeding: 'Proceeding',
    userIsAssignedToAnotherSession: "User: '{0}' is attending another session with title '{1}' at the same time ",
    roomIsAssignedToAnotherSession: "Session Room: '{0}' is assigned to the session '{1}' at the same time ",
    sessionRoom: "Session Room",
    sessionType: "Session Type",
    sessionTitle: "Title",
    sessionNo: "Session No",
    sessionAgendaNo: "Session Agenda No",
    sessionTime: "Session Time",
    sessionStart: "Start",
    sessionEnd: "Finish",
    project: "Project",
    projects: "Related Projects",
    userAttendees: "Invited Users",
    outerAttendees: "Invited People (Not System Users)",
    informedUsers: "Informed Users",
    organizerUser: "Session Organizer",
    sessionSubjectDefaultValue: "No Title",
    editProceedingRow: "Edit Proceeding Row",
    procTitle: "Title",
    procStatus: "Status",
    procResponsibles: "Responsible",
    procDueDate: "Due Date",
    procDescription: 'Description',
    procChangeHistory: 'Change History',
    procPriority: 'Priority',
    showTypes: {
        user: "All Related to Me",
        users: "Related Users",
        userCreated: "Created by Me",
        rooms: "Organization Rooms",
        allOrganization: "All Organization Sessions",
        allSystem: "All System Sessions",
    },
    invitedsStatus: {
        invitedUserTitle: 'Invited Title',
        notSystemUser: 'Not a System User',
        sessionPresenceStatus: 'Session Presence Status'
    },
    reports: {
        totalProceedingsCount: 'Total Proceedings Count',
        doneProceedingsCount: 'Done Proceedings Count',
        remainingProceedingsCount: 'Total Proceedings Count',
    },
    agg_session_HasNoAccess: "You do not have access to this section. You shoud either have managament or 'Session Aggregated Docs' permission in project to use this section",

}
////////////////////////////////////////////////////// Message Dispatch Module Ends /////////////////////////////////////////
//////////////////////////////////////////////////////     Message Dispatch Module Starts /////////////////////////////////////////

SmartAlert = {
    EventTypeName: "Event Type Name",
    EventsCount: "Event's Count",
    ExtensionName: "Extension Name",
    EventTitle: "Event Title",
    EventName: "Event's Name",
    EventRegistersCount: "Event Handler",
    EventRegistersTitle: "Event Handler' Title",
};

//////////////////////////////////////////////////////     Message Dispatch Module Ends /////////////////////////////////////////

/////////////////////////////////////////////////////      Chat Module Starts      /////////////////////////////////////////
chatStrs = {
    noFileSelected: "No File is selected",
    noOlderMessage: "There are no older messages",
    imageLoadingProblem: "There is a Problem in Image Loading",
    you: "You",
    loading: "Loading",
    userName: "Username",
    firstName: "First Name",
    lastName: "Last Name",
    enterAtLeastThreeWordsForSearch: "Enter at least 3 characters to initiate search ",
    newMessageFrom: "New Message From",
    otherMessages: "Other Messages",
    chatInfo:"Chat Information",
    clickSend: "Click to Send Message",
    chatwith: "Chat With ",
    videoCallto: "Video Call To ",
    voiceCallto: "Voice Call To "
};

chatGroupStrs = {
    GroupName: "Group Name",
    participants: "Participants",
    AddParticipant: "Add Participant",
    MemberSearch: "Search",
    ExitGroup: "Exit Group",
    DeleteGroup: "Delete Group",
};

/////////////////////////////////////////////////////      Chat Module Ends      /////////////////////////////////////////

/////////////////////////////////////////////////////      MDR Starts      /////////////////////////////////////////
mdrImportStrs = {
    pleaseSelectRequiredField: "Please select required items",
    fieldContent: "Field Content",
    fieldNameInExcel: "Field name in Excel file",
    selectColumns: "Select Columns",
    firstStepInfo1: "Notice that the items in the MDR list will be appended to the existing ones and they will not be updated",
    selectiveProjectPlan: "Project Plan",
    selectExcelFile: "Select Excel File",
    firstStepInfo2: "Please Note : <br />" +
        "<ul style='list-style-type: none'>" +
        "    <li>1- The first row of excel file may have these column names: (No,WBSNo,Weight,DocumentNo,DocumentTitle,ActivityCode,Descipline,DocumentType,DocumentClass,TotalPlannedManHour)" +
        "   <br />" +
        "      <img src='/images/MDRExcelSample.png' />" +
        "</li>" +

        "    <li>2- If the dates are base on Georgian clalendar, they should have this format: yyyy/mm/dd</li>" +
        //"    <li>3- If the dates are base on Jalali clalendar, they should have this format: 13yy/mm/dd .</li>" +
        "    <li>3- Please ensure that sheet name just contain letter and number characters</li>" +
        "    <li>4- Total weight of documents should be equal to weight of engineering phase</li>" +
        "</ul>" +
        "</li>" +
        "</ul>",
    uploadMDRFile: "Upload MDR Excel File",
    selectSheet: "Select Sheet",
    pleaseSelectSheet: "Please select sheet",
    finalCheck: "Final Check",
    lastStep: "Last Step",
    newDisciplineInfo: "List of disciplines that exist in the file but not in the project",
    createNewDiscipline: "Create new discipline for project",
    notCreateNewDiscipline: "Put empty value for disciplines that not exist in the project",
    newClassInfo: "List of document classes that exist in the file but not in the project",
    createNewClass: "Create new class for project",
    notCreateNewClass: "Put empty value for classes that not exist in the project",
    dataEntry: "Data Entry",
    allDisciplineExists: "All Disciplines Exists",
    allClassExists: "All Classes Exists",
    dataEntrySucceed: "Data entry successfully completed.",
    viewMDRPage: "View MDR Page",
    connectEDFIToWbs: "In order to assign documents to WBS use {0} page.",

};
/////////////////////////////////////////////////////      MDR Ends      /////////////////////////////////////////

/////////////////////////////////////////////////////      ProcurementItem Starts      /////////////////////////////////////////
piImportStrs = {
    pleaseSelectRequiredField: "Please select required items",
    fieldContent: "Field Content",
    fieldNameInExcel: "Field name in Excel file",
    selectColumns: "Select Columns",
    firstStepInfo1: "توجه داشته باشید كه فایل PI به لیست مدارك افزوده میشوند و در صورت تكراری بودن مدارك موجود به روز نخواهند شد",
    selectiveProjectPlan: "Project Plan",
    selectExcelFile: "Select Excel File",
    firstStepInfo2: "توجه : <br />" +
        "<ul style='list-style-type: none'>" +
        "    <li>1- در سطر اول فایل اکسل ستونها بهتر است با نامهای (No,WBSNo,Weight,Title,ActivityCode,Descipline,ProcurementType)" +
        "    نامگذاری شوند<br />" +
        "</li>" +
        "    <li>2- تاریخ های شروع و پایان برنامه ریزی باید بصورت میلادی و در قالب yyyy/mm/dd باشد.</li>" +
        //"    <li>&nbsp;3- در صورت میلادی بودن تاریخ های پروژه، تاریخها به همگی به صورت میلادی و با فرمت&nbsp;" +
        //"yyyy/mm/dd باشند</li>" +
        "    <li>3- اطیمنان حاصل كنید كه در نام شیت حاوی اطلاعات مدارك كاراكترهایی غیر از حرف و عدد" +
        "    وارد نشده باشد و نام شیت به صورت انگلیسی باشد.</li>" +
        "    <li>4- مجموع وزن مدارک می بایست برابر با وزن فاز خرید باشد</li>" +
        "</ul>" +
        "</li>" +
        "</ul>",
    uploadProcurementItemFile: "Upload PI Excel File",
    selectSheet: "Select Sheet",
    pleaseSelectSheet: "Please select sheet",
    finalCheck: "Final Check",
    lastStep: "Last Step",
    newDisciplineInfo: "لیست دیسلینهایی كه در فایل PI وارد شده وجود دارند اما در لیست كلاسهای پروژه وجود ندارد",
    createNewDiscipline: "Create new discipline for project",
    notCreateNewDiscipline: "دیسیپلین هایی كه در پروژه موجود نیستند را برای مدارك مقدار خالی قرار بده",
    newProcurementTypeInfo: "لیست نوع تدارک ها كه در فایل PI وارد شده وجود دارند اما در لیست نوع تدارک های پروژه وجود ندارد",
    createNewProcurementType: "Create new procurement type for project",
    notCreateNewProcurementType: "نوع تدارک هایی كه در پروژه موجود نیستند را برای مدارك مقدار خالی قرار بده",
    dataEntry: "Data Entry",
    allDisciplineExists: "All Disciplines Exists",
    allProcurementTypeExists: "All Procurement Types Exists",
    dataEntrySucceed: "Data entry successfully completed.",
    viewProcurementItemPage: "View PI Page",
    connectPDFIToWbs: "In order to assign PIs to WBS use {0} page.",


};
/////////////////////////////////////////////////////      ProcurementItem Ends      /////////////////////////////////////////


/////////////////////////////////////////////////////      Import WBS Start      /////////////////////////////////////////

wbsImportStrs =
{
    createWbs: "New WBS",
    updateWbs: "Update WBS",
    uploadWBSFile: "Upload",
    selectFile: "Select the project file",
    pleaseSelectFile: "Select the project file",
    firstStepInfo2: "<span style='font-weight:bold;color:red;'> Notice  : </span><br />" +
        "<ul style='list-style-type: none'>" +
        "   <li><span style='font-weight:bold;'> 1-</span>Acceptable Formats:</li>" +
        "   <li>" +
        "       <ul  style='list-style-type: none'>" +
        "           <li style='margin-left:10px;'>Microsoft Project fromats: MPP, MPT, MPX, MSPDI </li>" +
        "           <li style='margin-left:10px;'>Primavera P6 : XER format</li>" +
        "           <li style='margin-left:10px;'>Primavera P3 : PRX format</li>" +
        "           <li style='margin-left:10px;'>Planner : XML format</li>" +
        "           <li style='margin-left:10px;'>SureTrak : STX format</li>" +
        "           <li style='margin-left:10px;'>Asta Powerproject : PP, MDB formats</li>" +
        "           <li style='margin-left:10px;'>Phoenix Project Manager : PPX format</li>" +
        "           <li style='margin-left:10px;'>GanttProject : GAN format</li>" +
        "       </ul>" +
        "   </li>" +
        "   <li><span style='font-weight:bold;'> 2-</span>In order for future connection of Work Items to WBS, use “text1” custom field for Activity Code of Work Items." +
        "   </li>" +
        "   <li><span style='font-weight:bold;'> 3-</span>Use custom fields “Number1” in Microsoft Project for task’s weights." +
        "   </li>" +
        "   <li><span style='font-weight:bold;'> 4-</span>Use “Budgeted Labor” Units in Primavera P6 for task’s weights." +
        "   </li>" +
        "   <li><span style='font-weight:bold;'> 5-</span>Check your schedule file to avoid duplicate Activity and WBS codes." +
        "   </li>" +
        "   <li><span style='font-weight:bold;'> 6-</span>Maximum length of Task’s Name in schedule file is 500 characters." +
        "	</li>" +
        "</ul>",
    importedFileHasError: "Selected file has some issues",
    fileImportedSuccessfully: "Selected file imported successfully",
    newWBSName: "New WBS Name ",
    newPlanName: "New Plan Name",
    createRelateBetweenDFIandWBS: "Auto relate Work Items to WBS",
    basedOnRootPath: "Based on parent path",
    basedOnWBSCodeOfDFI: "Based on WBS code of Work Items",
    basedOnActivityCodeOfDFI: "Based on Activity Code of Work Items",
    basedOnWBSCode: "Based on WBS Code",
    basedOnActivityName: "Based on activity name",
    basedOnActivityCode: "Based on activity Code",
    basedOnGUID: "Based on Task GUID",
    updateResource: "Update Project's Resources",
    updateCalendar: "Update project's calendar and set as default",
    updateCBS: "Create and Update CBS according to Cost Fiels in Schedule File",
    pleaseFillInTheRequiredValues: "Fill out required Fields",
    thirdStepInfo1: " The following WBS has identified.",
    mspImportSettings: "Setting for Import from Microsoft Project",
    importSuccessInfo1: "Successfully Imported",
    wbsErrorList: "List of activities with error:",
    importSuccessInfo2: "Imported Schedule is set as default",
    workspacePage: "Click here to access Project's Work Space",
    howRelatePlanWithWbs: "Select Method to map selected WBS to new plan",
    numOfErrors: "Number of Errors",
    probabilityOfSuccess: "Probability of Success",
    numOfItemsCanCover: "Number of Items Can Cover"

};


/////////////////////////////////////////////////////      Import WBS End      /////////////////////////////////////////


/////////////////////////////////////////////////////      Currency Start      /////////////////////////////////////////

currencyStrs =
{
    conversionCurrency: "Conversion Currency",
    baseCurrency: "Base Currency",
    exchangeRate: "Exchange Rate",
    effectiveDate: "Effective Date",

};



/////////////////////////////////////////////////////      Currency End      /////////////////////////////////////////

/////////////////////////////////////////////////////      BI Starts      /////////////////////////////////////////

biStrs = {
    configur_messages: {
        measures: "[Drop Measures Here]",
        columns: "[Drop Columns Here]",
        rows: "[Drop Rows Here]",
        slices: '[Drop Slicers Here]',
        measuresLabel: 'MEASURES',
        columnsLabel: 'COLUMNS',
        slicersLabel: 'SLICERS',
        rowsLabel: 'ROWS',
        fieldsLabel: 'FIELDS'
    },
    filters: "Filters",
    dropFiltersHere: 'Drop Filters Here',
    hierarchy_Measures: "Measures",
    hierarchy_KPIs: "KPIs",
    reportShouldHaveAtLeastOneMeasure: "Report Should Have At Least One Measure",
    updateReoprtWindowTitle: "Update Reoprt Window Title",
    addReoprtWindowTitle: "Add Report",
    specifyANameForReport: "Specify a Name for Report",
    selectCube: "Select Cube",
    autoSliceSelect_assign: "Assign",
    autoSliceSelect_title: "Title",
    autoSliceSelect_selectAll: 'Select All',
};

/////////////////////////////////////////////////////      BI Ends      /////////////////////////////////////////

/////////////////////////////////////////////////////      Guarantee Starts      /////////////////////////////////////////

guaranteeStrs = {
    serialNO: "Serial NO",
    amount: 'Amount',
    issuanceDate: "Issuance Date",
    dueDate: "Due Date",
    duration: "Duration",
    toggleIsRealPerson: "شخص ضامن",
    toggleName: "Toggle Name",
    beneficiary: "Beneficiary",
    toggleResidence: "Toggle Residence",
    bank: "Bank",
    bankBranch: "Bank Branch",
    deliveryPlace: 'Delivery Place',
    transferee: 'Transferee',
    guaranteeType: 'Guarantee Type',
    guaranteeFormat: 'Guarantee Format',
    contractNo: 'Contract No',
    amountCurrency: 'Currency',
    toggleLegalEntityTypeName: 'Toggle Legal Entity Type Name',
    juridicallegal: "Juridicallegal",
    naturallegal: "Naturallegal",
    other: 'Other',
    specification: 'Specification',
    relatedDocs: 'Related Docs'
};



/////////////////////////////////////////////////////      Guarantee Ends      /////////////////////////////////////////



/////////////////////////////////////////////////////      Agile Starts      /////////////////////////////////////////
agileCommon =
{
};
scrumBoard = {
    items: 'items',
    point: 'points',
};
/////////////////////////////////////////////////////      Agile Ends      /////////////////////////////////////////

/////////////////////////////////////////////////////      Export Project Starts     /////////////////////////////////////////

exportProjectStrs = {
    pleaseSelectPlanandWBS: "Please select wbs and plan",
    pleaseSelectFileFormat: "Please select file format",
    useDefaultPlanandWBS: "Use default plan and wbs of project",
    selectPlanandWBS: "Select plan and wbs",
    wbsToUse: "WBS",
    planToUse: "Plan",
    fileFormat: "Fiel format",
    downloadFile: "Download File"


};



/////////////////////////////////////////////////////      Export Project Ends      /////////////////////////////////////////

/////////////////////////////////////////////////////      Cost Center Starts     /////////////////////////////////////////

costCenterStrs = {
    cannotDeleteHasPayment: "You cannot remove the cost center because of payment",
    cannotDeleteNotFound: "Cost center not found",
    cannotDeleteHasChild: "You cannot remove the cost center because of the subset"


};



/////////////////////////////////////////////////////      Cost Center Ends      /////////////////////////////////////////


/////////////////////////////////////////////////////      Import Calendar Starts     /////////////////////////////////////////

importCalendarStr =
{
    selectCalendarFile: "Select calendar file",
    uploadCalendarFile: "Upload calendar file",
    pleaseSelectCalendarFile: "Please select calendar file",
    createCalendar: "Create calendar",
    updateCalendar: "Update Calendar",
    name: "Name",
    description: "توشیحات",
    selectCalendar: "Select Calendar",
    updateWorkingTime: "Update working time",
    updateException: "Update exception",
    removePreException: "Remove prior exception",
    addToPreException: "Add to prior exceptions",
    dataEntrySucceed: "Importing the calendar was successful",
    dataEntryFailed: "There are error in the calendar import operation",
    finalStep: "Final step",
    importSettings: "Settings",
    generalInfo: "General Info",
    weekDays: "Week days",
    exceptions: "Exceptions",
    uploadFileFailed: "There are problems uploading and processing the file",
    pleaseInsertName: "Please insert calendar name",
    pleaseSelectCalendar: "Please select project calendar",
    selectOneItemForEdit: "Please select one option for edit",
    importCalendarFileTitle: "Import calendar from file",

}


/////////////////////////////////////////////////////      Import Calendar Ends     /////////////////////////////////////////
/////////////////////////////////////////////////////      Labelling Starts     ///////////////////////////////////////
labellingResources = {
    Title: "Title",
    ProjectName: "Project Name",
    Color: "Color",
    LabelTypeName: "Label Type Name",
    StakeholderName: "Stakeholder Name",
    newSave: "Create Label",
    editInfo: "Edit Label",
    alertDelete: "Are You Sure You Want To Delete This Row?",
    alertSuccessDelete: "Deletion Successfully Completed.",
    labelnotfound: "The Given Data Row Was Not Found.",
    labelHasRelatedItem: "Item has a related Item(s).Can not delete this Item.",
};
/////////////////////////////////////////////////////      Labelling Ends     /////////////////////////////////////////
