CREATE TABLE [Dr].[Doctor]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[NationalId] [int] NOT NULL,
[PhoneNumber] [int] NOT NULL,
[Name] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FName] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SectionId] [tinyint] NOT NULL,
[Address] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BirthDate] [date] NOT NULL,
[Expert] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Dr].[Doctor] ADD CONSTRAINT [PK_Doctor] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
