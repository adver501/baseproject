CREATE TABLE [student].[University]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [student].[University] ADD CONSTRAINT [PK_University] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
