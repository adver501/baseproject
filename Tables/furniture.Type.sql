CREATE TABLE [furniture].[Type]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [furniture].[Type] ADD CONSTRAINT [PK_Type] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
