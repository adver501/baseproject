CREATE TABLE [furniture].[Material]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TypeId] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [furniture].[Material] ADD CONSTRAINT [PK_Material] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [furniture].[Material] ADD CONSTRAINT [FK_Material_Type] FOREIGN KEY ([TypeId]) REFERENCES [furniture].[Type] ([Id])
GO
