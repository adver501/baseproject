CREATE TABLE [Library].[Author]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Library].[Author] ADD CONSTRAINT [PK_Author] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
