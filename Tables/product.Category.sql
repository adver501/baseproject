CREATE TABLE [product].[Category]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE Persian_100_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[Category] ADD CONSTRAINT [PK_Category_1] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
