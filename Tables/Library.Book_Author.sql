CREATE TABLE [Library].[Book_Author]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[BookId] [tinyint] NOT NULL,
[AuthorId] [tinyint] NOT NULL,
[SubGenreId] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Library].[Book_Author] ADD CONSTRAINT [PK_Book_Author] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Library].[Book_Author] ADD CONSTRAINT [FK_Book_Author_Author] FOREIGN KEY ([AuthorId]) REFERENCES [Library].[Author] ([Id])
GO
ALTER TABLE [Library].[Book_Author] ADD CONSTRAINT [FK_Book_Author_Book] FOREIGN KEY ([BookId]) REFERENCES [Library].[Book] ([Id])
GO
ALTER TABLE [Library].[Book_Author] ADD CONSTRAINT [FK_Book_Author_SubGenre] FOREIGN KEY ([SubGenreId]) REFERENCES [Library].[SubGenre] ([Id])
GO
