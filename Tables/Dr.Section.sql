CREATE TABLE [Dr].[Section]
(
[Id] [tinyint] NOT NULL,
[Population] [tinyint] NOT NULL,
[Name] [nchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Dr].[Section] ADD CONSTRAINT [PK_Section] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
