CREATE TABLE [furniture].[Sofa]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[Price] [float] NOT NULL,
[CategoryId] [tinyint] NOT NULL,
[ColorId] [tinyint] NOT NULL,
[MaterialId] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [furniture].[Sofa] ADD CONSTRAINT [PK_Sofa] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
