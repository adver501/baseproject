CREATE TABLE [Library].[SubGenre]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GenreId] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Library].[SubGenre] ADD CONSTRAINT [PK_SubGenre] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Library].[SubGenre] ADD CONSTRAINT [FK_SubGenre_Genre] FOREIGN KEY ([GenreId]) REFERENCES [Library].[Genre] ([Id])
GO
