CREATE TABLE [student].[Student]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Gender] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Grade] [float] NOT NULL,
[StudentId] [int] NOT NULL,
[MajorId] [int] NOT NULL,
[UniversityId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [student].[Student] ADD CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [student].[Student] ADD CONSTRAINT [FK_Student_Major] FOREIGN KEY ([MajorId]) REFERENCES [student].[Major] ([Id])
GO
ALTER TABLE [student].[Student] ADD CONSTRAINT [FK_Student_University] FOREIGN KEY ([UniversityId]) REFERENCES [student].[University] ([Id])
GO
