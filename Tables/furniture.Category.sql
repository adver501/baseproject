CREATE TABLE [furniture].[Category]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [furniture].[Category] ADD CONSTRAINT [PK_Category_2] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
