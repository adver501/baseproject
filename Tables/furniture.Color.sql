CREATE TABLE [furniture].[Color]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [furniture].[Color] ADD CONSTRAINT [PK_Color_2] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
