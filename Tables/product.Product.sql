CREATE TABLE [product].[Product]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE Persian_100_CI_AS NOT NULL,
[Price] [float] NOT NULL,
[Count] [tinyint] NOT NULL,
[Date] [datetime] NOT NULL,
[ColorId] [tinyint] NOT NULL,
[SubCategoryId] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[Product] ADD CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [product].[Product] ADD CONSTRAINT [FK_Product_Color] FOREIGN KEY ([ColorId]) REFERENCES [product].[Color] ([Id])
GO
ALTER TABLE [product].[Product] ADD CONSTRAINT [FK_Product_SubCategory] FOREIGN KEY ([SubCategoryId]) REFERENCES [product].[SubCategory] ([Id])
GO
