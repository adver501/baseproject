CREATE TABLE [product].[Color]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE Persian_100_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[Color] ADD CONSTRAINT [PK_Color_1] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
