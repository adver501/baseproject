CREATE TABLE [product].[SubCategory]
(
[Id] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE Persian_100_CI_AS NOT NULL,
[CategoryId] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[SubCategory] ADD CONSTRAINT [PK_SubCategory] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [product].[SubCategory] ADD CONSTRAINT [FK_SubCategory_Category] FOREIGN KEY ([CategoryId]) REFERENCES [product].[Category] ([Id])
GO
