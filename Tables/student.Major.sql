CREATE TABLE [student].[Major]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [student].[Major] ADD CONSTRAINT [PK_Major] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
