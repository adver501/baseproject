SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [Library].[VW_Book]
AS
SELECT        Library.SubGenre.Name AS SubGenreName, Library.Genre.Name AS GenreName, Library.SubGenre.Id AS SubGenreId, Library.Genre.Id AS GenreId, Library.Author.Id AS AuthorId, Library.Book.Id AS BookId, Library.Book.Name, Library.Book.ISBN, 
                         Library.Author.FirstName, Library.Author.LastName, Library.Book_Author.Id
FROM            Library.Author INNER JOIN
                         Library.Book_Author ON Library.Author.Id = Library.Book_Author.AuthorId INNER JOIN
                         Library.SubGenre ON Library.Book_Author.SubGenreId = Library.SubGenre.Id INNER JOIN
                         Library.Book ON Library.Book_Author.BookId = Library.Book.Id INNER JOIN
                         Library.Genre ON Library.SubGenre.GenreId = Library.Genre.Id
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'Library', 'VIEW', N'VW_Book', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[66] 4[8] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Author (Library)"
            Begin Extent = 
               Top = 155
               Left = 9
               Bottom = 268
               Right = 179
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Book (Library)"
            Begin Extent = 
               Top = 56
               Left = 620
               Bottom = 169
               Right = 790
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Book_Author (Library)"
            Begin Extent = 
               Top = 2
               Left = 301
               Bottom = 132
               Right = 471
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Genre (Library)"
            Begin Extent = 
               Top = 231
               Left = 279
               Bottom = 327
               Right = 449
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SubGenre (Library)"
            Begin Extent = 
               Top = 232
               Left = 564
               Bottom = 345
               Right = 734
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         ', 'SCHEMA', N'Library', 'VIEW', N'VW_Book', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1

GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'Library', 'VIEW', N'VW_Book', NULL, NULL
GO
