SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [furniture].[VW_Sofa]
AS
SELECT        furniture.Sofa.Id, furniture.Sofa.Price, furniture.Material.Name AS MaterialName, furniture.Color.Name AS ColorName, furniture.Category.Name AS CategoryName ,furniture.Type.Name AS TypeName,furniture.Category.Id AS CategoryId , furniture.Color.Id AS ColorId,furniture.Material.Id AS MaterialId
FROM            furniture.Sofa INNER JOIN
                         furniture.Color ON furniture.Sofa.ColorId = furniture.Color.Id INNER JOIN
                         furniture.Material ON furniture.Sofa.MaterialId = furniture.Material.Id INNER JOIN
                         furniture.Category ON furniture.Sofa.CategoryId = furniture.Category.Id INNER JOIN
						 furniture.Type ON Type.Id = Material.TypeId

GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[27] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Category (furniture)"
            Begin Extent = 
               Top = 130
               Left = 215
               Bottom = 226
               Right = 385
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Color (furniture)"
            Begin Extent = 
               Top = 23
               Left = 550
               Bottom = 119
               Right = 720
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Material (furniture)"
            Begin Extent = 
               Top = 198
               Left = 442
               Bottom = 310
               Right = 612
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Sofa (furniture)"
            Begin Extent = 
               Top = 0
               Left = 0
               Bottom = 130
               Right = 170
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Type (furniture)"
            Begin Extent = 
               Top = 239
               Left = 630
               Bottom = 335
               Right = 800
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
  ', 'SCHEMA', N'furniture', 'VIEW', N'VW_Sofa', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'       Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'furniture', 'VIEW', N'VW_Sofa', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'furniture', 'VIEW', N'VW_Sofa', NULL, NULL
GO
