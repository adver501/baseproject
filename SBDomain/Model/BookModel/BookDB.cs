namespace SBDomain.Model.BookModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BookDB : DbContext
    {
        public BookDB()
            : base("name=BookDB")
        {
        }

        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Book_Author> Book_Author { get; set; }
        public virtual DbSet<Genre> Genres { get; set; }
        public virtual DbSet<SubGenre> SubGenres { get; set; }
        public virtual DbSet<VW_Book> VW_Book { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Author>()
                .HasMany(e => e.Book_Author)
                .WithRequired(e => e.Author)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Book>()
                .Property(e => e.ISBN)
                .IsFixedLength();

            modelBuilder.Entity<Book>()
                .HasMany(e => e.Book_Author)
                .WithRequired(e => e.Book)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Genre>()
                .HasMany(e => e.SubGenres)
                .WithRequired(e => e.Genre)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubGenre>()
                .HasMany(e => e.Book_Author)
                .WithRequired(e => e.SubGenre)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VW_Book>()
                .Property(e => e.ISBN)
                .IsFixedLength();
        }
    }
}
