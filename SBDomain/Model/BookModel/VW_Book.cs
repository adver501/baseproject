namespace SBDomain.Model.BookModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Library.VW_Book")]
    public partial class VW_Book
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string SubGenreName { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string GenreName { get; set; }

        [Key]
        [Column(Order = 2)]
        public byte SubGenreId { get; set; }

        [Key]
        [Column(Order = 3)]
        public byte GenreId { get; set; }

        [Key]
        [Column(Order = 4)]
        public byte AuthorId { get; set; }

        [Key]
        [Column(Order = 5)]
        public byte BookId { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(50)]
        public string Name { get; set; }

        [Key]
        [Column(Order = 7)]
        [StringLength(13)]
        public string ISBN { get; set; }

        [Key]
        [Column(Order = 8)]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Key]
        [Column(Order = 9)]
        [StringLength(50)]
        public string LastName { get; set; }

        [Key]
        [Column(Order = 10)]
        public byte Id { get; set; }
    }
}
