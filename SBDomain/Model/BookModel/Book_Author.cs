namespace SBDomain.Model.BookModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Library.Book_Author")]
    public partial class Book_Author
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }

        public byte BookId { get; set; }

        public byte AuthorId { get; set; }

        public byte SubGenreId { get; set; }

        public virtual Author Author { get; set; }

        public virtual Book Book { get; set; }

        public virtual SubGenre SubGenre { get; set; }
    }
}
