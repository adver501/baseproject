namespace SBDomain.Model.ShirtModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VW_Shirt
    {
        [Key]
        [Column(Order = 0)]
        public byte Id { get; set; }

        [Key]
        [Column(Order = 1)]
        public double Price { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]

        public string ColorName { get; set; }
        [Key]
        [Column(Order = 3)]
        public byte ColorId { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(50)]
        public string CategoryName { get; set; }

        [Key]
        [Column(Order = 5)]
        public byte CategoryId { get; set; }
    }
}
