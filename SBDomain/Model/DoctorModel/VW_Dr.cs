namespace SBDomain.Model.DoctorModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Dr.VW_Dr")]
    public partial class VW_Dr
    {
        [Key]
        [Column(Order = 0)]
        public byte secId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(15)]
        public string secName { get; set; }

        [Key]
        [Column(Order = 2)]
        public byte Id { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int nId { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int phoneNum { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(10)]
        public string name { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(10)]
        public string fName { get; set; }

        [Key]
        [Column(Order = 7)]
        public byte sectionId { get; set; }

        [Key]
        [Column(Order = 8)]
        [StringLength(50)]
        public string address { get; set; }

        [Key]
        [Column(Order = 9, TypeName = "date")]
        public DateTime bDate { get; set; }

        [Key]
        [Column(Order = 10)]
        [StringLength(10)]
        public string expert { get; set; }
    }
}
