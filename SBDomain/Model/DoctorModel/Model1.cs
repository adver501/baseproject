namespace SBDomain.Model.DoctorModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Doctor> Doctors { get; set; }
        public virtual DbSet<Section> Sections { get; set; }
        public virtual DbSet<VW_Dr> VW_Dr { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Doctor>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Doctor>()
                .Property(e => e.FName)
                .IsFixedLength();

            modelBuilder.Entity<Doctor>()
                .Property(e => e.Address)
                .IsFixedLength();

            modelBuilder.Entity<Doctor>()
                .Property(e => e.Expert)
                .IsFixedLength();

            modelBuilder.Entity<Section>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<VW_Dr>()
                .Property(e => e.secName)
                .IsFixedLength();

            modelBuilder.Entity<VW_Dr>()
                .Property(e => e.name)
                .IsFixedLength();

            modelBuilder.Entity<VW_Dr>()
                .Property(e => e.fName)
                .IsFixedLength();

            modelBuilder.Entity<VW_Dr>()
                .Property(e => e.address)
                .IsFixedLength();

            modelBuilder.Entity<VW_Dr>()
                .Property(e => e.expert)
                .IsFixedLength();
        }
    }
}
