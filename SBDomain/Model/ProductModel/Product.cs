namespace SBDomain.Model.ProductModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("product.Product")]
    public partial class Product
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public double Price { get; set; }

        public byte Count { get; set; }

        public DateTime Date { get; set; }

        public byte ColorId { get; set; }

        public byte SubCategoryId { get; set; }

        public virtual Color Color { get; set; }

        public virtual SubCategory SubCategory { get; set; }
    }
}
