namespace SBDomain.Model.ProductModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("product.VW_Product")]
    public partial class VW_Product
    {
        [Key]
        [Column(Order = 0)]
        public byte Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string Name { get; set; }

        [Key]
        [Column(Order = 2)]
        public double Price { get; set; }

        [Key]
        [Column(Order = 3)]
        public byte ColorId { get; set; }

        [Key]
        [Column(Order = 4)]
        public byte SubCategoryId { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(50)]
        public string ColorName { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(50)]
        public string CategoryName { get; set; }

        [Key]
        [Column(Order = 7)]
        [StringLength(50)]
        public string SubCategoryName { get; set; }

        [Key]
        [Column(Order = 8)]
        public byte Count { get; set; }

        [Key]
        [Column(Order = 9)]
        public DateTime Date { get; set; }

        [Key]
        [Column(Order = 10)]
        public byte CategoryId { get; set; }
    }
}
