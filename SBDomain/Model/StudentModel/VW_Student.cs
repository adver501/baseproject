namespace SBDomain.Model.StudentModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("student.VW_Student")]
    public partial class VW_Student
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string LastName { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string Gender { get; set; }

        [Key]
        [Column(Order = 4)]
        public double Grade { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentId { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MajorId { get; set; }

        [Key]
        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UniversityId { get; set; }

        [Key]
        [Column(Order = 8)]
        public DateTime ModifiedDate { get; set; }

        [Key]
        [Column(Order = 9)]
        [StringLength(50)]
        public string Major { get; set; }

        [Key]
        [Column(Order = 10)]
        [StringLength(50)]
        public string University { get; set; }
    }
}
