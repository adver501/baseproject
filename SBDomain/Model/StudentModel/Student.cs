namespace SBDomain.Model.StudentModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("student.Student")]
    public partial class Student
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(50)]
        public string Gender { get; set; }

        public double Grade { get; set; }

        public int StudentId { get; set; }

        public int MajorId { get; set; }

        public int UniversityId { get; set; }

        public DateTime ModifiedDate { get; set; }

        public virtual Major Major { get; set; }

        public virtual University University { get; set; }
    }
}
