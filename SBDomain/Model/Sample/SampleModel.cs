namespace SBDomain.Model.Sample
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SampleModel : DbContext
    {
        public SampleModel()
            : base("name=SampleModel")
        {
        }

        public virtual DbSet<Main> Mains { get; set; }
        public virtual DbSet<Sub> Subs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Main>()
                .HasMany(e => e.Subs)
                .WithRequired(e => e.Main)
                .HasForeignKey(e => e.ParentId)
                .WillCascadeOnDelete(false);
        }
    }
}
