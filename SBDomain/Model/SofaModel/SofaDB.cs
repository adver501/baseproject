namespace SBDomain.Model.SofaModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SofaDB : DbContext
    {
        public SofaDB()
            : base("name=SofaDB")
        {
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Color> Colors { get; set; }
        public virtual DbSet<Material> Materials { get; set; }
        public virtual DbSet<Sofa> Sofas { get; set; }
        public virtual DbSet<Type> Types { get; set; }
        public virtual DbSet<VW_Sofa> VW_Sofa { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasMany(e => e.Sofas)
                .WithRequired(e => e.Category)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Color>()
                .HasMany(e => e.Sofas)
                .WithRequired(e => e.Color)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Material>()
                .HasMany(e => e.Sofas)
                .WithRequired(e => e.Material)
                .WillCascadeOnDelete(false);
        }
    }
}
