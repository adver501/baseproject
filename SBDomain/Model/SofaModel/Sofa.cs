namespace SBDomain.Model.SofaModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("furniture.Sofa")]
    public partial class Sofa
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }

        public double Price { get; set; }

        public byte CategoryId { get; set; }

        public byte ColorId { get; set; }

        public byte MaterialId { get; set; }

        public virtual Category Category { get; set; }

        public virtual Color Color { get; set; }

        public virtual Material Material { get; set; }
    }
}
